PREFIX project: <http://idi.fundacionctic.org/tabels/project/digital-tachograpgh-codes/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/digital-tachograph-codes/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?country,?TACHOALPHACode,?TACHONUMCode,?CONVCode,?ennote] IN horizontal 
    
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?TACHOALPHACodeResource = resource(concat("DIGITAL_TACHOGRAPH_NATION_ALPHA_CODE_",?TACHOALPHACode),emergelModules)
  LET ?TACHONUMCodeResource = resource(concat("DIGITAL_TACHOGRAPH_NATION_NUMERIC_CODE_",?TACHONUMCode),emergelModules)
  LET ?UEPublisher = resource("STANDARDIZATION_ORGANIZATION_UE",emergelModules)
 
CONSTRUCT {
   emergelModules:TACHOALPHACodeList a emergelModules:Country;
                                   rdfs:label "European Commission Digital Tachograph Nation alpha codes" ;
                                   skos:prefLabel "European Commission Digital Tachograph Nation alpha codes" ;
                                   rdfs:label "European Commission Digital Tachograph Nation alpha codes"@en ;
                                   skos:prefLabel "European Commission Digital Tachograph Nation alpha codes"@en ;
                                   rdfs:label "Códigos alfa para países no tacógrafo digital da Comissão Europeia"@pt ;
                                   skos:prefLabel "Códigos alfa para países no tacógrafo digital da Comissão Europeia"@pt ;
                                   rdfs:label "Codis alfa per a països del tacógrafo digital de la Comissió Europea"@ca ;
                                   skos:prefLabel "Codis alfa per a països del tacógrafo digital de la Comissió Europea"@ca ;
                                   rdfs:label "Codes alpha de pays du tachographe digital de la Comission Européenne"@fr ;
                                   skos:prefLabel "Codes alpha de pays du tachographe digital de la Comission Européenne"@fr ;
                                   rdfs:label "Códigos alfa para países del tacógrafo digital de la Comisión Europea"@es ;
                                   skos:prefLabel "Códigos alfa para países del tacógrafo digital de la Comisión Europea"@es ;
                                   dct:publisher ?UEPublisher .

   emergelModules:TACHONUMCodeList a emergelModules:Country;
                                   rdfs:label "European Commission Digital Tachograph Nation numeric codes" ;
                                   skos:prefLabel "European Commission Digital Tachograph Nation numeric codes" ;
                                   rdfs:label "European Commission Digital Tachograph Nation numeric codes"@en ;
                                   skos:prefLabel "European Commission Digital Tachograph Nation numeric codes"@en ;
                                   rdfs:label "Códigos numéricos para países no tacógrafo digital da Comissão Europeia"@pt ;
                                   skos:prefLabel "Códigos numéricos para países no tacógrafo digital da Comissão Europeia"@pt ;
                                   rdfs:label "Codis numérics per a països del tacógrafo digital de la Comissió Europea"@ca ;
                                   skos:prefLabel "Codis numérics per a països del tacógrafo digital de la Comissió Europea"@ca ;
                                   rdfs:label "Codes numériques de pays du tachographe digital de la Comission Européenne"@fr ;
                                   skos:prefLabel "Codes numériques de pays du tachographe digital de la Comission Européenne"@fr ;
                                   rdfs:label "Códigos numéricos para países del tacógrafo digital de la Comisión Europea"@es ;
                                   skos:prefLabel "Códigos numéricos para países del tacógrafo digital de la Comisión Europea"@es ;
                                   dct:publisher ?UEPublisher .
}

CONSTRUCT {
   ?TACHOALPHACodeResource a skos:Concept;
                  rdfs:label ?TACHOALPHACode ;
                  skos:prefLabel ?TACHOALPHACode ; 
                  skos:inScheme emergelModules:TACHOALPHACodeList;
                  emergel:codeOf ?ISOResource .

   ?ISOResource emergel:hasCode ?TACHOALPHACodeResource .
}

CONSTRUCT {
   ?TACHONUMCodeResource a skos:Concept;
                  rdfs:label ?TACHONUMCode ;
                  skos:prefLabel ?TACHONUMCode ; 
                  skos:inScheme emergelModules:TACHONUMCodeList;
                  emergel:codeOf ?ISOResource .

   ?ISOResource emergel:hasCode ?TACHONUMCodeResource .
}