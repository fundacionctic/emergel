PREFIX project: <http://idi.fundacionctic.org/tabels/project/nuts2-codes/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/nuts2-codes/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>

FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?NUTS2Code,?ISOCode] IN horizontal 
    
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?NUTS2CodeResource = resource(concat("NUTS2_COUNTRY_CODE_",?NUTS2Code),emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
 
CONSTRUCT {
  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   dct:publisher ?EurostatPublisher .
}

CONSTRUCT {
   ?NUTS2CodeResource a skos:Concept;
                  rdfs:label ?NUTS2Code ;
                  skos:prefLabel ?NUTS2Code ; 
                  skos:inScheme emergelModules:NUTS2CodeList;
                  emergel:codeOf ?ISOResource .

   ?ISOResource emergel:hasCode ?NUTS2CodeResource .
}