PREFIX project: <http://idi.fundacionctic.org/tabels/project/consudatle-federations/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/consudatle-federations/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>

FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?prefstyle,?ISOCode,?IAAFRegionCode,?IOCCode,?federation,?en,?enalt,?es,?esalt,?fr,?fralt,?nl,?pt,?ptalt] IN horizontal 

  LET ?IAAFPublisher = resource("STANDARDIZATION_ORGANIZATION_IAAF",emergelModules)
  LET ?nationalFederationResource = resource(concat("CONSUDATLE_NATIONAL_FEDERATION_",replace(?IOCCode,"-","_")),emergelModules)
  LET ?nationalFederationCodeResource = resource(concat("CONSUDATLE_NATIONAL_FEDERATION_CODE_",replace(?IOCCode,"-","_")),emergelModules)
  LET ?IAAFRegionResource = resource(concat("IAAF_CONTINENTAL_REGION_",replace(?IAAFRegionCode,"-","_")), emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?enLabel = setLangTag(?en, "en")
  LET ?enaltLabel = setLangTag(?enalt, "en")
  LET ?esLabel = setLangTag(?es, "es")
  LET ?esaltLabel = setLangTag(?esalt, "es")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?fraltLabel = setLangTag(?fralt, "fr")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?ptaltLabel = setLangTag(?ptalt, "pt")

WHEN not matches(?nl,"") DO
LET ?nlLabel = setLangTag(?nl, "nl")

CONSTRUCT {
   emergelModules:CONSUDATLEFederationsList a emergelModules:Country;
                                   rdfs:label "National federations of the South American Athletics Confederation (CONSUDATLE)" ;
                                   skos:prefLabel "National federations of the South American Athletics Confederation (CONSUDATLE)" ;
                                   rdfs:label "National federations of the South American Athletics Confederation (CONSUDATLE)"@en ;
                                   skos:prefLabel "National federations of the South American Athletics Confederation (CONSUDATLE)"@en ;
                                   skos:altLabel "National federations of the CONSUDATLE"@en ;
                                   rdfs:label "Federaciones nacionales de la Confederación Sudamericana de Atletismo (CONSUDATLE)"@es ;
                                   skos:prefLabel "Federaciones nacionales de la Confederación Sudamericana de Atletismo (CONSUDATLE)"@es ;
                                   rdfs:label "Federacions nacionals de la Confederació africana d'atletisme (CONSUDATLE)"@ca ;
                                   skos:prefLabel "Federacions nacionals de la Confederació africana d'atletisme (CONSUDATLE)"@ca ;
                                   rdfs:label "Federações nacionais da Confederação Sul-Americana de Atletismo (CONSUDATLE)"@pt ;
                                   skos:prefLabel "Federações nacionais da Confederação Sul-Americana de Atletismo (CONSUDATLE)"@pt ;
                                   rdfs:label "Fédérations nationales de la Confédération sud-américaine d'athlétisme (CONSUDATLE)"@fr ;
                                   skos:prefLabel "Fédérations nationales de la Confédération sud-américaine d'athlétisme (CONSUDATLE)"@fr ;
                                   skos:altLabel "Fédérations nationales de la CONSUDATLE"@fr .

  emergelModules:CONSUDATLEFederationCodeList a emergelModules:GeographicalArea ;
                                       skos:prefLabel "Codes for the federations of the CONSUDATLE" ;
                                       rdfs:label "Codes for the federations of the CONSUDATLE" ;
                                       dct:publisher ?IAAFPublisher.
}

CONSTRUCT {
  ?nationalFederationResource a skos:Concept;
                  rdfs:label ?federation ;
                  skos:prefLabel ?enLabel ; 
                  skos:prefLabel ?frLabel ;  
                  skos:prefLabel ?esLabel ; 
                  skos:prefLabel ?ptLabel ; 
                  skos:altLabel ?ptaltLabel ; 
                  skos:altLabel ?enaltLabel ; 
                  skos:altLabel ?esaltLabel ; 
                  skos:altLabel ?fraltLabel ; 
                  skos:inScheme emergelModules:CONSUDATLEFederationsList;
                  emergel:hasCode ?nationalFederationCodeResource ;
                  emergel:country ?ISOResource ;
                  skos:notation ?IOCCode ;
                  skos:broader ?IAAFRegionResource .

  ?IAAFRegionResource skos:narrower ?nationalFederationResource .
  
  ?nationalFederationCodeResource a skos:Concept;
                  rdfs:label ?IOCCode ;
                  skos:prefLabel ?IOCCode ; 
                  skos:inScheme emergelModules:CONSUDATLEFederationCodeList;
                  emergel:codeOf ?nationalFederationResource .
}

CONSTRUCT {
  ?nationalFederationResource skos:prefLabel ?nlLabel .
}