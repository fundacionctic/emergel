PREFIX project: <http://idi.fundacionctic.org/tabels/project/FIregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/FIregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?ISOCode,?svlabel,?enlabel,?rulabel,?nuts1,?nuts2,?nuts3,?FIPSCode,?HASCCode,?RCCode,?ISO2Code,?ISO3Code] IN horizontal 
    
  LET ?regionResource = resource(concat("FI_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("FI_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("FI_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionRCCodeResource = resource(concat("FI_DIVISION_LEVEL_1_REGIONAL_COUNCIL_CODE_",replace(?RCCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("FI_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?fiLabel = setLangTag(?region, "fi")
  LET ?svLangLabel = setLangTag(?svlabel, "sv")
  LET ?enLangLabel = setLangTag(?enlabel, "en")
  LET ?ruLangLabel = setLangTag(?rulabel, "ru")
  LET ?nut1Res = resource(concat("NUTS1_CODE_LIST_",?nuts1), emergelModules)
  LET ?nut2Res = resource(concat("NUTS2_CODE_LIST_",?nuts2), emergelModules)
  LET ?nut3Res = resource(concat("NUTS3_CODE_LIST_",?nuts3), emergelModules)

  
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

{
 WHEN not matches(?ISO2Code,"") DO
 LET ?regionISO2CodeResource = resource(concat("ISO_ALPHA_2_COUNTRY_CODE_",?ISO2Code),emergelModules)
 ;
 WHEN not matches(?ISO3Code,"") DO
 LET ?regionISO3CodeResource = resource(concat("ISO_ALPHA_3_COUNTRY_CODE_",?ISO3Code),emergelModules)
  }

CONSTRUCT {
  
  emergelModules:FIRegionList a emergelModules:Country;
                              rdfs:label "Regions of Finland" ;
                              skos:prefLabel "Regions of Finland" ;
                              rdfs:label "Regions of Finland"@en ;
                              skos:prefLabel "Regions of Finland"@en ;
                              rdfs:label "Suomen maakunnat"@fi ;
                              skos:prefLabel "Suomen maakunnat"@fi ;
                              rdfs:label "Landskapsförbund i Finland"@sv ;
                              skos:prefLabel "Landskapsförbund i Finland"@sv ;
                              rdfs:label "Административное деление Финляндии: Провинции/области Финляндии"@ru ;
                              skos:prefLabel "Административное деление Финляндии: Провинции/области Финляндии"@ru .
  
  emergelModules:FIRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of Finland"@en ;
                                   skos:prefLabel "ISO codes for the regions of Finland"@en ;
                                   rdfs:label "Koodit ISO 3166-2:FI"@fi ;
                                   skos:prefLabel "Koodit ISO 3166-2:FI"@fi ;
                                   rdfs:label "ISO 3166-2:FI-koder"@sv ;
                                   skos:prefLabel "ISO 3166-2:FI-koder"@sv ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:FIRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of Finland" ;
                                   skos:prefLabel "FIPS codes for the regions of Finland" ;
                                   rdfs:label "FIPS codes for the regions of Finland"@en ;
                                   skos:prefLabel "FIPS codes for the regions of Finland"@en ;
                                   rdfs:label "FIPS-koder - Landskapsförbund i Finland"@sv ;
                                   skos:prefLabel "FIPS-koder - Landskapsförbund i Finland"@sv ;
                                   rdfs:label "Koodit FIPS - Suomen maakunnat"@fi ;
                                   skos:prefLabel "Koodit FIPS - Suomen maakunnat"@fi ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:FIRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Finland"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Finland"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de las regiones de Finlandia"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de las regiones de Finlandia"@es ;
                                   rdfs:label "Codis HASC (Hierarchical administrative subdivision codes) de les regions de Finlàndia"@ca ;
                                   skos:prefLabel "Codis HASC (Hierarchical administrative subdivision codes) de les regions de Finlàndia"@ca ;
                                   rdfs:label "HASC (Hierarchical administrative subdivision codes) koder - Norges fylkeskommunen"@no ;
                                   skos:prefLabel "HASC (Hierarchical administrative subdivision codes) koder - Norges fylkeskommunen"@no ;
                                   rdfs:label "Koodit HASC (Hierarchical administrative subdivision codes) - Suomen maakunnat"@fi ;
                                   skos:prefLabel "Koodit HASC (Hierarchical administrative subdivision codes) - Suomen maakunnat"@fi ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:FIRegionRCCodeList a emergelModules:Country;
                                   rdfs:label "Ministry of the Interior's (Suomen sisäministeri) codes for the regions of Finland" ;
                                   skos:prefLabel "Ministry of the Interior's (Suomen sisäministeri) codes for the regions of Finland" ;
                                   rdfs:label "Ministry of the Interior's (Suomen sisäministeri) codes for the regions of Finland"@en ;
                                   skos:prefLabel "Ministry of the Interior's (Suomen sisäministeri) codes for the regions of Finland"@en ;
                                   rdfs:label "Koodit Suomen sisäministeri - Suomen maakunnat"@fi ;
                                   skos:prefLabel "Koodit Suomen sisäministeri - Suomen maakunnat"@fi .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .

  emergelModules:ISOALPHA2CountryClassification a emergelModules:Country ;
                                   skos:prefLabel "ISO-3166 alpha-2 designators for countries, dependent territories and special areas of geographical interest" ;
                                   rdfs:label "ISO-3166 alpha-2 designators for countries, dependent territories and special areas of geographical interest" ;
                                   skos:prefLabel "ISO-3166 alpha-2 designators for countries, dependent territories and special areas of geographical interest"@en ;
                                   rdfs:label "ISO-3166 alpha-2 designators for countries, dependent territories and special areas of geographical interest"@en ;
                                   skos:prefLabel "Codes pays ISO-3166 alpha-2"@fr ;
                                   rdfs:label "Codes pays ISO-3166 alpha-2"@fr ;
                                   skos:prefLabel "Codici di paesi ISO-3166 alpha-2"@it ;
                                   rdfs:label "Codici di paesi ISO-3166 alpha-2"@it ;
                                   skos:prefLabel "Codis de país ISO-3166 alpha-2"@ca ;
                                   rdfs:label "Codis de país ISO-3166 alpha-2"@ca ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:ISOALPHA3CountryClassification a emergelModules:Country ;
                                   skos:prefLabel "ISO-3166 alpha-3 designators for countries, dependent territories and special areas of geographical interest" ;
                                   rdfs:label "ISO-3166 alpha-3 designators for countries, dependent territories and special areas of geographical interest" ;
                                   skos:prefLabel "ISO-3166 alpha-3 designators for countries, dependent territories and special areas of geographical interest"@en ;
                                   rdfs:label "ISO-3166 alpha-3 designators for countries, dependent territories and special areas of geographical interest"@en ;
                                   skos:prefLabel "Codes pays ISO-3166 alpha-3"@fr ;
                                   rdfs:label "Codes pays ISO-3166 alpha-3"@fr ;
                                   skos:prefLabel "Codici di paesi ISO-3166 alpha-3"@it ;
                                   rdfs:label "Codici di paesi ISO-3166 alpha-3"@it ;
                                   skos:prefLabel "Codis de país ISO-3166 alpha-3"@ca ;
                                   rdfs:label "Codis de país ISO-3166 alpha-3"@ca ;
                                   dct:publisher ?ISOPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?fiLabel ;
                  skos:prefLabel ?svLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?ruLangLabel ;
                  skos:inScheme emergelModules:FIRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionRCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource ;
                  emergel:hasCode ?nut3Res.

  ?nut3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nut2Res ;
           skos:broader ?nut1Res .
  
  ?nut2Res a skos:Concept;
           skos:narrower ?nut3Res ;
           skos:inScheme emergelModules:NUTS2CodeList ;
           skos:broader ?nut1Res .

  ?nut1Res a skos:Concept;
           skos:narrower ?nut3Res ;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nut2Res .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:FIRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:FIRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:FIRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionRCCodeResource a skos:Concept;
                  rdfs:label ?RCCode ;
                  skos:prefLabel ?RCCode ; 
                  skos:inScheme emergelModules:FIRegionRCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionISO2CodeResource .

  ?regionISO2CodeResource a skos:Concept;
                  rdfs:label ?ISO2Code ;
                  skos:prefLabel ?ISO2Code ;
                  skos:inScheme emergelModules:ISOALPHA2CountryClassification;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionISO3CodeResource .

  ?regionISO3CodeResource a skos:Concept;
                  rdfs:label ?ISO3Code ;
                  skos:prefLabel ?ISO3Code ;
                  skos:inScheme emergelModules:ISOALPHA3CountryClassification;
                  emergel:codeOf ?regionResource .
}
