PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsAZ/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsAZ/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?region,?enlabel,?noteen,?rulabel,?noteru,?falabel,?notefa,?trlabel,?notetr,?hylabel,?notehy,?kalabel,?kalatlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("AZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("AZ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("AZ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "az")
  LET ?enLanglabel = setLangTag(?enlabel, "en")
  LET ?trLanglabel = setLangTag(?trlabel, "tr")
  LET ?hyLanglabel = setLangTag(?hylabel, "hy")
  LET ?ruLanglabel = setLangTag(?rulabel, "ru")
  LET ?faLanglabel = setLangTag(?falabel, "fa")

  LET ?ennoteLabel = setLangTag(?noteen,"en")
  LET ?runoteLabel = setLangTag(?noteru,"ru")
  LET ?fanoteLabel = setLangTag(?notefa,"fa")
  LET ?trnoteLabel = setLangTag(?notetr,"tr")
  LET ?hynoteLabel = setLangTag(?notehy,"hy")

{
  WHEN not matches(?kalabel,"") DO
  LET ?kaLanglabel = setLangTag(?kalabel, "ka")
  ;
  WHEN not matches(?kalatlabel,"") DO
  LET ?kalatLanglabel = setLangTag(?kalatlabel, "ka")
}

CONSTRUCT {
  
  emergelModules:AZDistrictList a emergelModules:Country;
                              rdfs:label "Administrative divisions of Azerbaijan: autonomous republic, rayons (districts) and cities" ;
                              skos:prefLabel "Administrative divisions of Azerbaijan: autonomous republic, rayons (districts) and cities" ;
                              rdfs:label "Administrative divisions of Azerbaijan: autonomous republic, rayons (districts) and cities"@en ;
                              skos:prefLabel "Administrative divisions of Azerbaijan: autonomous republic, rayons (districts) and cities"@en ;
                              rdfs:label "Azerbaycan'ın idari yapılanması"@tr ;
                              skos:prefLabel "Azerbaycan'ın idari yapılanması"@tr ;
                              rdfs:label "بخش‌های جمهوری آذربایجان"@fa ;
                              skos:prefLabel "بخش‌های جمهوری آذربایجان"@fa ;
                              rdfs:label "Azərbaycanın şəhər və rayonları"@az ;
                              skos:prefLabel "Azərbaycanın şəhər və rayonları"@az ;
                              rdfs:label "Սիրիայի մարզեր"@hy ;
                              skos:prefLabel "Սիրիայի մարզեր"@hy ;
                              rdfs:label "Административно-территориальное деление Азербайджана"@ru ;
                              skos:prefLabel "Административно-территориальное деление Азербайджана"@ru .
  
  emergelModules:AZDistrictISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts and cities of Azerbaijan" ;
                                   skos:prefLabel "ISO codes for the districts and cities of Azerbaijan" ;
                                   rdfs:label "ISO codes for the districts and cities of Azerbaijan"@en ;
                                   skos:prefLabel "ISO codes for the districts and cities of Azerbaijan"@en ;
                                   rdfs:label "ISO 3166-2:AZ - بخش‌های جمهوری آذربایجان"@fa ;
                                   skos:prefLabel "ISO 3166-2:AZ - بخش‌های جمهوری آذربایجان"@fa ;
                                   rdfs:label "ISO 3166-2:AZ - Административно-территориальное деление Азербайджана"@ru ;
                                   skos:prefLabel "ISO 3166-2:AZ - Административно-территориальное деление Азербайджана"@ru ;
                                   rdfs:label "ISO 3166-2:AZ - Azərbaycanın şəhər və rayonları"@az ;
                                   skos:prefLabel "ISO 3166-2:AZ - Azərbaycanın şəhər və rayonları"@az ;
                                   rdfs:label "ISO 3166-2:AZ - Azerbaycan'ın idari yapılanması"@tr ;
                                   skos:prefLabel "ISO 3166-2:AZ - Azerbaycan'ın idari yapılanması"@tr ;
                                   rdfs:label "ISO 3166-2:AZ - Սիրիայի մարզեր"@hy ;
                                   skos:prefLabel "ISO 3166-2:AZ - Սիրիայի մարզերה"@hy .

  emergelModules:AZDistrictFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts and cities of Azerbaijan" ;
                                   skos:prefLabel "FIPS codes for the districts and cities of Azerbaijan" ;
                                   rdfs:label "FIPS codes for the districts and cities of Azerbaijan"@en ;
                                   skos:prefLabel "FIPS codes for the districts and cities of Azerbaijan"@en ;
                                   rdfs:label "FIPS - بخش‌های جمهوری آذربایجان"@fa ;
                                   skos:prefLabel "FIPS - بخش‌های جمهوری آذربایجان"@fa ;
                                   rdfs:label "FIPS - Административно-территориальное деление Азербайджана"@ru ;
                                   skos:prefLabel "FIPS - Административно-территориальное деление Азербайджана"@ru ;
                                   rdfs:label "FIPS - Azərbaycanın şəhər və rayonları"@az ;
                                   skos:prefLabel "FIPS - Azərbaycanın şəhər və rayonları"@az ;
                                   rdfs:label "FIPS - Azerbaycan'ın idari yapılanması"@tr ;
                                   skos:prefLabel "FIPS - Azerbaycan'ın idari yapılanması"@tr ;
                                   rdfs:label "FIPS - Սիրիայի մարզեր"@hy ;
                                   skos:prefLabel "FIPS - Սիրիայի մարզեր"@hy .

}


CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?trLanglabel ;
                  skos:prefLabel ?ruLanglabel ;
                  skos:prefLabel ?hyLanglabel ;
                  skos:prefLabel ?faLanglabel ;
                  skos:note ?ennoteLabel ;
                  skos:note ?runoteLabel ;
                  skos:note ?fanoteLabel ;
                  skos:note ?trnoteLabel ;
                  skos:note ?hynoteLabel ;
                  skos:inScheme emergelModules:AZDistrictList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:AZDistrictISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:AZDistrictFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?kaLanglabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?kalatLanglabel .
}