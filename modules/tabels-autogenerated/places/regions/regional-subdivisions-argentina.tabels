PREFIX project: <http://idi.fundacionctic.org/tabels/project/ARProvinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/ARProvinces/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?IFAMCode,?region,?ipaesar,?de,?esalt,?en,?pt,?arn,?gn,?gnalt,?fr,?fralt,?Note,?ptnote,?esnote,?denote,?gnnote,?frnote,?INDECCode,?capital,?area,?pop2001,?pop2010,?pop2014] IN horizontal 
    
  LET ?regionResource = resource(concat("AR_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("AR_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?FIPSCodeResource = resource(concat("AR_DIVISION_LEVEL_1_FIPS_CODE_",?FIPSCode),emergelModules)
  LET ?IFAMCodeResource = resource(concat("AR_DIVISION_LEVEL_1_IFAM_CODE_",?IFAMCode),emergelModules)
  LET ?INDECCodeResource = resource(concat("AR_DIVISION_LEVEL_1_INDEC_CODE_",?INDECCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?esLabel = setLangTag(?region, "es")
  LET ?gnLabel = setLangTag(?gn, "gn")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?esaltLabel = setLangTag(?esalt, "es")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?fraltLabel = setLangTag(?fralt, "fr")
  LET ?deLabel = setLangTag(?de, "de")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?ipaesarLangLabel = setLangTag(?ipaesar, "es-AR-fonipa")
  LET ?ipaes419LangLabel = setLangTag(?ipaesar, "es-419-fonipa")

  LET ?NoteLabel = setLangTag(?Note,"en")
  LET ?esNoteLabel = setLangTag(?esnote,"es")
  LET ?gnNoteLabel = setLangTag(?gnnote,"gn")
  LET ?frNoteLabel = setLangTag(?frnote,"fr")
  LET ?deNoteLabel = setLangTag(?denote,"de")
  LET ?ptNoteLabel = setLangTag(?ptnote,"pt")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?IFAMPublisher = resource("STANDARDIZATION_ORGANIZATION_IFAM",emergelModules)
  LET ?INDECPublisher = resource("STANDARDIZATION_ORGANIZATION_INDEC",emergelModules)
    
{
    WHEN not matches(?gnalt,"") DO
    LET ?gnaltLangLabel = setLangTag(?gnalt, "gn")
    ;
    WHEN not matches(?arn,"") DO
    LET ?arnLangLabel = setLangTag(?arn, "arn")
}

CONSTRUCT {
  
  emergelModules:ARProvincesList a emergelModules:Country;
                              rdfs:label "Provinces of Argentina" ;
                              skos:prefLabel "Provinces of Argentina" ;
                              rdfs:label "Provinces of Argentina"@en ;
                              skos:prefLabel "Provinces of Argentina"@en ;
                              rdfs:label "Províncias de Argentina"@pt ;
                              skos:prefLabel "Províncias de Argentina"@pt ;                              
                              rdfs:label "Argentina Tetãmininguéra"@gn ;
                              skos:prefLabel "Argentina Tetãmininguéra"@gn ;                              
                              rdfs:label "Provincias de la Argentina"@es ;
                              skos:prefLabel "Provincias del Argentina"@es ;                              
                              rdfs:label "Provinces de l'Argentine"@fr ;
                              skos:prefLabel "Provinces de l'Argentine"@fr ;                              
                              rdfs:label "Provinzen Argentiniens"@de ;
                              skos:prefLabel "Provinzen Argentiniens"@de .
  
  emergelModules:ARProvincesISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Argentina" ;
                                   skos:prefLabel "ISO codes for the provinces of Argentina" ;
                                   rdfs:label "ISO codes for the provinces of Argentina"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Argentina"@en ;
                                   rdfs:label "Códigos ISO das províncias da Argentina"@pt ;
                                   skos:prefLabel "Códigos ISO das províncias da Argentina"@pt ;
                                   rdfs:label "Códigos ISO de las provincias de la Argentina"@es ;
                                   skos:prefLabel "Códigos ISO de las provincias de la Argentina"@es ;
                                   rdfs:label "ISO 3166-2:AR - Argentina Tetãmininguéra"@gn ;
                                   skos:prefLabel "ISO 3166-2:AR - Argentina Tetãmininguéra"@gn ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:ARProvincesFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Argentina" ;
                                   skos:prefLabel "FIPS codes for the provinces of Argentina" ;
                                   rdfs:label "FIPS codes for the provinces of Argentina"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Argentina"@en ;
                                   rdfs:label "Códigos FIPS das províncias da Argentina"@pt ;
                                   skos:prefLabel "Códigos FIPS das províncias da Argentina"@pt ;
                                   rdfs:label "Códigos FIPS de las provincias de la Argentina"@es ;
                                   skos:prefLabel "Códigos FIPS de las provincias de la Argentina"@es ;
                                   rdfs:label "FIPS - Argentina Tetãmininguéra"@gn ;
                                   skos:prefLabel "FIPS - Argentina Tetãmininguéra"@gn ;
                                   dct:publisher ?FIPSPublisher . 

  emergelModules:ARProvincesIFAMCodeList a emergelModules:Country;
                                   rdfs:label "IFAM (Instituto Federal de Asuntos Municipales) codes for the provinces of Argentina" ;
                                   skos:prefLabel "IFAM (Instituto Federal de Asuntos Municipales) codes for the provinces of Argentina" ;
                                   rdfs:label "IFAM (Instituto Federal de Asuntos Municipales) codes for the provinces of Argentina"@en ;
                                   skos:prefLabel "IFAM (Instituto Federal de Asuntos Municipales) codes for the provinces of Argentina"@en ;
                                   rdfs:label "Códigos IFAM (Instituto Federal de Asuntos Municipales) das províncias do Argentina"@pt ;
                                   skos:prefLabel "Códigos IFAM (Instituto Federal de Asuntos Municipales) das províncias do Argentina"@pt ;
                                   rdfs:label "Códigos IFAM (Instituto Federal de Asuntos Municipales) de las provincias de la Argentina"@es ;
                                   skos:prefLabel "Códigos IFAM (Instituto Federal de Asuntos Municipales) de las provincias de la Argentina"@es ;
                                   rdfs:label "IFAM - Argentina Tetãmininguéra"@gn ;
                                   skos:prefLabel "IFAM - Argentina Tetãmininguéra"@gn ;
                                   dct:publisher ?IFAMPublisher .     

  emergelModules:ARProvincesINDECCodeList a emergelModules:Country;
                                   rdfs:label "INDEC (Instituto Nacional de Estadística y Censos) codes for the provinces of Argentina" ;
                                   skos:prefLabel "INDEC (Instituto Nacional de Estadística y Censos) codes for the provinces of Argentina" ;
                                   rdfs:label "INDEC (Instituto Nacional de Estadística y Censos) codes for the provinces of Argentina"@en ;
                                   skos:prefLabel "INDEC (Instituto Nacional de Estadística y Censos) codes for the provinces of Argentina"@en ;
                                   rdfs:label "Códigos INDEC (Instituto Nacional de Estadística y Censos) das províncias do Argentina"@pt ;
                                   skos:prefLabel "Códigos INDEC (Instituto Nacional de Estadística y Censos) das províncias do Argentina"@pt ;
                                   rdfs:label "Códigos INDEC (Instituto Nacional de Estadística y Censos) de las provincias de la Argentina"@es ;
                                   skos:prefLabel "Códigos INDEC (Instituto Nacional de Estadística y Censos) de las provincias de la Argentina"@es ;
                                   rdfs:label "INDEC - Argentina Tetãmininguéra"@gn ;
                                   skos:prefLabel "INDEC - Argentina Tetãmininguéra"@gn ;
                                   dct:publisher ?INDECPublisher .                             
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?gnLabel ;
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?esaltLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:altLabel ?fraltLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:prefLabel ?ipaes419LangLabel ;
                  skos:prefLabel ?ipaesarLangLabel ;
                  ontolex:phoneticRep ?ipaesarLangLabel ;
                  ontolex:phoneticRep ?ipaes419LangLabel ;
                  skos:note ?NoteLabel ;
                  skos:note ?esNoteLabel ;
                  skos:note ?gnNoteLabel ;
                  skos:note ?frNoteLabel ;
                  skos:note ?ptNoteLabel ;
                  skos:note ?deNoteLabel ;
                  skos:inScheme emergelModules:ARProvincesList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?FIPSCodeResource ;
                  emergel:hasCode ?IFAMCodeResource ;
                  emergel:hasCode ?INDECCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:ARProvincesISOCodeList;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:ARProvincesFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?IFAMCodeResource a skos:Concept;
                  rdfs:label ?IFAMCode ;
                  skos:prefLabel ?IFAMCode ; 
                  skos:inScheme emergelModules:ARProvincesIFAMCodeList;
                  emergel:codeOf ?regionResource .

  ?INDECCodeResource a skos:Concept;
                  rdfs:label ?INDECCode ;
                  skos:prefLabel ?INDECCode ; 
                  skos:inScheme emergelModules:ARProvincesINDECCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource  skos:altLabel ?gnaltLangLabel .
}

CONSTRUCT {
  ?regionResource  skos:prefLabel ?arnLangLabel .
}
