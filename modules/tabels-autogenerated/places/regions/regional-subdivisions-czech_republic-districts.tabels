PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsCZE/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsCZE/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?provinceCode,?province,?regionCode,?ISOCode,?en,?sk,?de,?pl] IN horizontal 
    
  LET ?provinceResource = resource(concat("CZ_DIVISION_LEVEL_2_",replace(?province,"[^a-zA-Z0-9]","")),emergelModules)
  LET ?provinceCodeResource = resource(concat("CZ_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("CZ_DIVISION_LEVEL_1_CZ_",replace(?regionCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?csLabel = setLangTag(?province, "cs")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?skLabel = setLangTag(?sk, "sk")
  LET ?deLabel = setLangTag(?de, "de")
  LET ?plLabel = setLangTag(?pl, "pl")
    

CONSTRUCT {
  
  emergelModules:CZDistrictList a emergelModules:Country;
                              rdfs:label "Districts of the Czech Republic" ;
                              skos:prefLabel "Districts of the Czech Republic" ;
                              rdfs:label "Districts of the Czech Republic"@en ;
                              skos:prefLabel "Districts of the Czech Republic"@en ;
                              rdfs:label "Okresy České Republiky"@cs ;
                              skos:prefLabel "Okresy České Republiky"@cs ;
                              rdfs:label "Zoznam okresov v Česku"@sk ;
                              skos:prefLabel "Zoznam okresov v Česku"@sk ;
                              rdfs:label "Okresy (Gemeinden) in Tschechien"@de ;
                              skos:prefLabel "Okresy (Gemeinden) in Tschechien"@de ;
                              rdfs:label "Powiaty w Czechach"@pl ;
                              skos:prefLabel "Powiaty w Czechach"@pl .
  
  emergelModules:CZDistrictISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of the Czech Republic" ;
                                   skos:prefLabel "ISO codes for the districts of the Czech Republic" ;
                                   rdfs:label "ISO codes for the districts of the Czech Republic"@en ;
                                   skos:prefLabel "ISO codes for the districts of the Czech Republic"@en ;
                                   rdfs:label "Seznam kódů okresů České Republiky - ISO 3166-2:CZ"@cs ;
                                   skos:prefLabel "Seznam kódů okresů České Republiky - ISO 3166-2:CZ"@cs .
}
CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?csLabel ;
                  skos:prefLabel ?skLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:prefLabel ?plLabel ;
                  skos:inScheme emergelModules:CZDistrictList;
                  emergel:hasCode ?provinceCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
                  //skos:broader ?ISOResource .
  
  //?ISOResource skos:narrower ?provinceResource .
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:CZDistrictISOCodeList;
                  emergel:codeOf ?provinceResource .
}
