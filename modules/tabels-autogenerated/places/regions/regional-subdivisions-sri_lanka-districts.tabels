PREFIX project: <http://idi.fundacionctic.org/tabels/project/districtsLK/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/districtsLK/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?HASCCode,?FIPSCode,?province,?silabel,?silatlabel,?talabel,?talatlabel,?PABCode,?PCCode,?CCCode,?pop2012,?areakm2,?areami2,?hilabel,?zhlabel] IN horizontal 
    
  WHEN not matches (?provinceCode,"") do
  LET ?provinceResource = resource(concat("LK_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("LK_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("LK_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("LK_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?provinceCCCodeResource = resource(concat("LK_DIVISION_LEVEL_2_CENSUS_CODE_",replace(?CCCode,"-","_")),emergelModules)

    LET ?enLangLabel = setLangTag(?province, "en")
    LET ?silatLangLabel = setLangTag(?silatlabel, "si-Latn")
    LET ?talatLangLabel = setLangTag(?talatlabel, "ta-Latn")
    LET ?siLangLabel = setLangTag(?silabel, "si")
    LET ?taLangLabel = setLangTag(?talabel, "ta")
    LET ?hiLangLabel = setLangTag(?hilabel, "hi")
    LET ?zhLangLabel = setLangTag(?zhlabel, "zh")
    
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

{
WHEN NOT MATCHES (?FIPSCode,"") DO
LET ?provinceFIPSCodeResource = resource(concat("LK_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
;
WHEN NOT MATCHES (?PABCode,"") DO
LET ?provincePABCodeResource = resource(concat("LK_DIVISION_LEVEL_1_PAB_CODE_",replace(?PABCode,"-","_")),emergelModules)
;
WHEN NOT MATCHES (?PCCode,"") DO
LET ?provincePCCodeResource = resource(concat("LK_DIVISION_LEVEL_1_POSTAL_CODE_",replace(?PCCode,"-","_")),emergelModules)
}

CONSTRUCT {
  
  emergelModules:LKDistrictList a emergelModules:Country;
                              rdfs:label "Districts of Sri Lanka" ;
                              skos:prefLabel "Districts of Sri Lanka" ;
                              rdfs:label "श्रीलंका के जिले"@hi ;
                              skos:prefLabel "श्रीलंका के जिले"@hi ;
                              rdfs:label "Districts of Sri Lanka"@en ;
                              skos:prefLabel "Districts of Sri Lanka"@en ;
                              rdfs:label "ශ්‍රී ලංකාවේ පරිපාලන දිස්ත්‍රික්ක"@si ;
                              skos:prefLabel "ශ්‍රී ලංකාවේ පරිපාලන දිස්ත්‍රික්ක"@si ;
                              rdfs:label "斯里兰卡县级行政区划"@zh ;
                              skos:prefLabel "斯里兰卡县级行政区划"@zh ;
                              rdfs:label "இலங்கையின் மாவட்டங்கள்"@ta ;
                              skos:prefLabel "இலங்கையின் மாவட்டங்கள்"@ta .
  
  emergelModules:LKDistrictISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of Sri Lanka"@en ;
                                   skos:prefLabel "ISO codes for the districts of Sri Lanka"@en ;
                                   rdfs:label "ISO codes for the districts of Sri Lanka" ;
                                   skos:prefLabel "ISO codes for the districts of Sri Lanka" ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:LKDistrictFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts of Sri Lanka"@en ;
                                   skos:prefLabel "FIPS codes for the districts of Sri Lanka"@en ;
                                   rdfs:label "FIPS codes for the districts of Sri Lanka" ;
                                   skos:prefLabel "FIPS codes for the districts of Sri Lanka" ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:LKDistrictPABCodeList a emergelModules:Country;
                                   rdfs:label "Sri Lanka Post Office's abbreviations for the districts"@en ;
                                   skos:prefLabel "Sri Lanka Post Office's abbreviations for the districts"@en ;
                                   rdfs:label "Sri Lanka Post Office's abbreviations for the districts" ;
                                   skos:prefLabel "Sri Lanka Post Office's abbreviations for the districts" .

  emergelModules:LKDistrictHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Sri Lanka" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Sri Lanka" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Sri Lanka"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Sri Lanka"@en ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:LKDistrictPCCodeList a emergelModules:Country;
                                   rdfs:label "Postal code prefixes for the districts of Sri Lanka"@en ;
                                   skos:prefLabel "Postal code prefixes for the districts of Sri Lanka"@en ;
                                   rdfs:label "Postal code prefixes for the districts of Sri Lanka" ;
                                   skos:prefLabel "Postal code prefixes for the districts of Sri Lanka" .

  emergelModules:LKDistrictCCCodeList a emergelModules:Country;
                                   rdfs:label "Census codes for the districts of Sri Lanka"@en ;
                                   skos:prefLabel "Census codes for the districts of Sri Lanka"@en ;
                                   rdfs:label "Census codes for the districts of Sri Lanka" ;
                                   skos:prefLabel "Census codes for the districts of Sri Lanka" .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?siLangLabel ;
                  skos:prefLabel ?taLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?hiLangLabel ;
                  skos:prefLabel ?zhLangLabel ;
                  skos:prefLabel ?silatLangLabel ;
                  skos:prefLabel ?talatLangLabel ;
                  skos:inScheme emergelModules:LKDistrictList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceCCCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .

  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:LKDistrictISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceCCCodeResource a skos:Concept;
                  rdfs:label ?CCCode ;
                  skos:prefLabel ?CCCode ; 
                  skos:inScheme emergelModules:LKDistrictCCCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:LKDistrictHASCCodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {

  ?provinceResource emergel:hasCode ?provinceFIPSCodeResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:LKDistrictFIPSCodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {

  ?provinceResource emergel:hasCode ?provincePCCodeResource .

  ?provincePCCodeResource a skos:Concept;
                  rdfs:label ?PCCode ;
                  skos:prefLabel ?PCCode ; 
                  skos:inScheme emergelModules:LKDistrictPCCodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {

  ?provinceResource emergel:hasCode ?provincePABCodeResource .

  ?provincePABCodeResource a skos:Concept;
                  rdfs:label ?PABCode ;
                  skos:prefLabel ?PABCode ; 
                  skos:inScheme emergelModules:LKDistrictPABCodeList;
                  emergel:codeOf ?provinceResource .
}