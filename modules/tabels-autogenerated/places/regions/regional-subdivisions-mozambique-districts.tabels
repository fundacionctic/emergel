PREFIX project: <http://idi.fundacionctic.org/tabels/project/districtsMZ/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/districtsMZ/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?HASCCode,?province,?enlabel,?area,?pop1997,?pop2007,?pop2005,?ptaltlabel] IN horizontal 
    
  LET ?provinceResource = resource(concat("MZ_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("MZ_DIVISION_LEVEL_2_INEMZ_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("MZ_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("MZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ptLabel = setLangTag(?province, "pt")
  LET ?enLabel = setLangTag(?enlabel, "en")

  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?INEMZPublisher = resource("STANDARDIZATION_ORGANIZATION_INEMZ",emergelModules)

{
  WHEN not matches (?ptaltlabel, "") DO
  LET ?ptaltLabel = setLangTag(?ptaltlabel, "pt")
}

CONSTRUCT {
  
  emergelModules:MZDistrictList a emergelModules:Country;
                              rdfs:label "Districts of Mozambique" ;
                              skos:prefLabel "Districts of Mozambique" ;
                              rdfs:label "Districts of Mozambique"@en ;
                              skos:prefLabel "Districts of Mozambique"@en ;
                              rdfs:label "Distritos de Angola"@pt ;
                              skos:prefLabel "Distritos de Angola"@pt .
  
  emergelModules:MZDistrictINEMZCodeList a emergelModules:Country;
                                   rdfs:label "INE (Instituto Nacional de Estatística de Mozambique) codes for the districts of Mozambique" ;
                                   skos:prefLabel "INE (Instituto Nacional de Estatística de Mozambique) codes for the districts of Mozambique" ;
                                   rdfs:label "INE (Instituto Nacional de Estatística de Angola) codes for the districts of Mozambique"@en ;
                                   skos:prefLabel "INE (Instituto Nacional de Estatística de Angola) codes for the districts of Mozambique"@en ;
                                   rdfs:label "Códigos do INE (Instituto Nacional de Estatística de Moçambique) dos distritos de Mozambique"@pt ;
                                   skos:prefLabel "Códigos do INE (Instituto Nacional de Estatística de Moçambique) dos distritos de Mozambique"@pt ;
                                   dct:publisher ?INEMZPublisher .

  emergelModules:MZDistrictHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Mozambique" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Mozambique" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Mozambique"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Mozambique"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos de Moçambique"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos de Moçambique"@pt ;
                                   dct:publisher ?HASCPublisher .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:inScheme emergelModules:MZDistrictList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .

  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:MZDistrictINEMZCodeList;
                  emergel:codeOf ?provinceResource .
  
  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:MZDistrictHASCCodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?ptaltLabel .
}