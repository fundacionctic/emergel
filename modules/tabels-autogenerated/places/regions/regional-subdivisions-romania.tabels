PREFIX project: <http://idi.fundacionctic.org/tabels/project/ROregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/ROregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?PCCode,?HASCCode,?region,?note_en,?ISOCode,?bglabel,?delabel,?hulabel,?srlabel,?enlabel,?nuts1,?nuts2,?nuts3,?FIPSCode,?uklabel,?rmylabel,?kalabel,?roaltlabel,?iparolabel,?ipahulabel] IN horizontal 
    
  LET ?regionResource = resource(concat("RO_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("RO_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("RO_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("RO_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionPCCodeResource = resource(concat("RO_DIVISION_LEVEL_1_POSTAL_AREA_CODE_",replace(?PCCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  /*name of the country in local languages*/
  LET ?rueCountryLabel = setLangTag("Румуньско", "rue")
  LET ?moCountryLabel = setLangTag("Ромыния", "mo")
  LET ?gagCountryLabel = setLangTag("Romıniya", "gag")

  LET ?roLabel = setLangTag(?region, "ro")
  LET ?huLangLabel = setLangTag(?hulabel, "hu")
  LET ?srLangLabel = setLangTag(?srlabel, "sr")
  LET ?enLangLabel = setLangTag(?enlabel, "en")
  LET ?bgLangLabel = setLangTag(?bglabel, "bg")
  LET ?ukLangLabel = setLangTag(?uklabel, "uk")
  LET ?kaLangLabel = setLangTag(?kalabel, "ka")
  LET ?roaltLangLabel = setLangTag(?roaltlabel, "ro")
  LET ?iparoLangLabel = setLangTag(?iparolabel, "ro-RO-fonipa")
  LET ?note_enLang = setLangTag(?note_en, "en")
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

{
  WHEN not matches(?ipahulabel, "") DO
  LET ?ipahuLangLabel = setLangTag(?ipahulabel, "hu-RO-fonipa")
 ;
  WHEN not matches (?delabel, "") DO
  LET ?deLangLabel = setLangTag(?delabel, "de")
 ;   
  WHEN not matches (?rmylabel, "") DO
  LET ?rmyLangLabel = setLangTag(?rmylabel, "rmy")
}

CONSTRUCT {
  
  emergelModules:ROCountyList a emergelModules:Country;
                              rdfs:label "Counties of Romania"@en ;
                              skos:prefLabel "Counties of Romania"@en ;
                              rdfs:label "Counties of Romania" ;
                              skos:prefLabel "Counties of Romania" ;
                              rdfs:label "Județele României"@ro ;
                              skos:prefLabel "Județele României"@ro ;
                              rdfs:label "Románia megyéi"@hu ;
                              skos:prefLabel "Románia megyéi"@hu ;
                              rdfs:label "Kreise Rumäniens"@de ;
                              skos:prefLabel "Kreise Rumäniens"@de ;
                              rdfs:label "Окръзи в Румъния"@bg ;
                              skos:prefLabel "Окръзи в Румъния"@bg ;
                              rdfs:label "Адміністративний поділ Румунії: Жудеці Румунії"@uk ;
                              skos:prefLabel "Адміністративний поділ Румунії: Жудеці Румунії"@uk .
  
  emergelModules:ROCountyISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the counties of Romania" ;
                                   skos:prefLabel "ISO codes for the counties of Romania" ;
                                   rdfs:label "ISO codes for the counties of Romania"@en ;
                                   skos:prefLabel "ISO codes for the counties of Romania"@en ;
                                   rdfs:label "Listă de coduri - ISO 3166-2:RO (Județele României)"@ro ;
                                   skos:prefLabel "Listă de coduri - ISO 3166-2:RO (Județele României)"@ro ;
                                   rdfs:label "Földrajzi kódokat ISO 3166-2:RO (Megyék)"@hu ;
                                   skos:prefLabel "Földrajzi kódokat ISO 3166-2:RO (Megyék)"@hu ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:ROCountyHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Romania" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Romania" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Romania"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Romania"@en ;
                                   rdfs:label "Listă de coduri HASC (Hierarchical administrative subdivision codes) - Județele României"@ro ;
                                   skos:prefLabel "Listă de coduri HASC (Hierarchical administrative subdivision codes) - Județele României"@ro ;
                                   rdfs:label "Földrajzi kódokat HASC (Hierarchical administrative subdivision codes) - Megyék"@hu ;
                                   skos:prefLabel "Földrajzi kódokat HASC (Hierarchical administrative subdivision codes) - Megyék"@hu ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:ROCountyFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the departments of Romania" ;
                                   skos:prefLabel "FIPS codes for the departments of Romania" ;
                                   rdfs:label "FIPS codes for the departments of Romania"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Romania"@en ;
                                   rdfs:label "Listă de coduri - FIPS (Județele României)"@ro ;
                                   skos:prefLabel "Listă de coduri - FIPS (Județele României)"@ro ;
                                   rdfs:label "Földrajzi kódokat FIPS (Megyék)"@hu ;
                                   skos:prefLabel "Földrajzi kódokat FIPS (Megyék)"@hu ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:ROCountyPCCodeList a emergelModules:Country;
                                   rdfs:label "Postal area codes for the departments of Romania" ;
                                   skos:prefLabel "Postal area codes for the departments of Romania" ;
                                   rdfs:label "Postal area codes for the departments of Romania"@en ;
                                   skos:prefLabel "Postal area codes for the departments of Romania"@en ;
                                   rdfs:label "Listă de coduri poștali - Județele României"@ro ;
                                   skos:prefLabel "Listă de coduri poștali - Județele României"@ro .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?roLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?bgLangLabel ;
                  skos:prefLabel ?ukLangLabel ;
                  skos:prefLabel ?kaLangLabel ;
                  skos:altLabel ?roaltLangLabel ;
                  skos:prefLabel ?iparoLangLabel ;
                  ontolex:phoneticRep ?iparoLangLabel ;
                  skos:note ?note_enLang ;
                  skos:prefLabel ?huLangLabel ;
                  skos:prefLabel ?srLangLabel ;
                  skos:inScheme emergelModules:ROCountyList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionPCCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?nuts3Res ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?moCountryLabel ;
               skos:prefLabel ?gagCountryLabel ;
               skos:prefLabel ?rueCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:ROCountyISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:ROCountyFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:ROCountyHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionPCCodeResource a skos:Concept;
                  rdfs:label ?PCCode ;
                  skos:prefLabel ?PCCode ; 
                  skos:inScheme emergelModules:ROCountyPCCodeList;
                  emergel:codeOf ?regionResource .


  ?nuts3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts2Res ;
           skos:broader ?nuts1Res .
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList ;
           skos:broader ?nuts1Res .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nuts2Res .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?deLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?rmyLangLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?ipahuLangLabel ;
                  ontolex:phoneticRep ?ipahuLangLabel .
}