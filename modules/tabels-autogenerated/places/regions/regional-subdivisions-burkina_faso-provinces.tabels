PREFIX project: <http://idi.fundacionctic.org/tabels/project/provincesBF/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/provincesBF/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
WHEN not matches(?rowId,"") DO
    MATCH [?regionCode,?provinceCode,?HASCCode,?FIPSCode,?PlateCode,?province,?en,?pt,?bm,?pop2006,?pop1996,?areakm,?areami,?capital,?formerly] IN horizontal 
    
  LET ?provinceResource = resource(concat("BF_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("BF_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("BF_DIVISION_LEVEL_2_FIPS_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provincePLATECodeResource = resource(concat("BF_DIVISION_LEVEL_2_PROVINCEPLATE_CODE_",replace(?PlateCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("BF_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("BF_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)

  LET ?frLabel = setLangTag(?province, "fr")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?bmLabel = setLangTag(?bm, "bm")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)


CONSTRUCT {
  
  emergelModules:BFProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Burkina Faso" ;
                              skos:prefLabel "Provinces of Burkina Faso" ;
                              rdfs:label "Provinces of Burkina Faso"@en ;
                              skos:prefLabel "Provinces of Burkina Faso"@en ;
                              rdfs:label "Provinces de Burkina Faso"@fr ;
                              skos:prefLabel "Provinces de Burkina Faso"@fr ;
                              rdfs:label "Màra (Burukina Faso)"@bm ;
                              skos:prefLabel "Màra (Burukina Faso)"@bm ;
                              rdfs:label "Províncias de Burkina Faso"@pt ;
                              skos:prefLabel "Províncias de Burkina Faso"@pt .
  
  emergelModules:BFProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Burkina Faso" ;
                                   skos:prefLabel "ISO codes for the provinces of Burkina Faso" ;
                                   rdfs:label "ISO codes for the provinces of Burkina Faso"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Burkina Faso"@en ;
                                   rdfs:label "Codes ISO des provinces de Burkina Faso"@fr ;
                                   skos:prefLabel "Codes ISO des provinces de Burkina Faso"@fr ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:BFProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Burkina Faso" ;
                                   skos:prefLabel "FIPS codes for the provinces of Burkina Faso" ;
                                   rdfs:label "FIPS codes for the provinces of Burkina Faso"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Burkina Faso"@en ;
                                   rdfs:label "Codes FIPS des provinces de Burkina Faso"@fr ;
                                   skos:prefLabel "Codes FIPS des provinces de Burkina Faso"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:BFProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Burkina Faso" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Burkina Faso" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Burkina Faso"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Burkina Faso"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) pdes provinces de Burkina Faso"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des provinces de Burkina Faso"@fr ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:BFProvincePLATECodeList a emergelModules:Country;
                                   rdfs:label "Vehicle registration plate codes for the provinces of Burkina Faso" ;
                                   skos:prefLabel "Vehicle registration plate codes for the provinces of Burkina Faso" ;
                                   rdfs:label "Vehicle registration plate codes for the provinces of Burkina Faso"@en ;
                                   skos:prefLabel "Vehicle registration plate codes for the provinces of Burkina Faso"@en ;
                                   rdfs:label "Codes de province sur les plaques d'immatriculation de voitures"@fr ;
                                   skos:prefLabel "Codes de province sur les plaques d'immatriculation de voitures"@fr .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?bmLabel ;
                  skos:inScheme emergelModules:BFProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  emergel:hasCode ?provincePLATECodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
  
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:BFProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BFProvinceFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:BFProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .

  ?provincePLATECodeResource a skos:Concept;
                  rdfs:label ?PlateCode ;
                  skos:prefLabel ?PlateCode ; 
                  skos:inScheme emergelModules:BFProvincePLATECodeList;
                  emergel:codeOf ?provinceResource .
}