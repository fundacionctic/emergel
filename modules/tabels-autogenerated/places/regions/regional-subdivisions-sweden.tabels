PREFIX project: <http://idi.fundacionctic.org/tabels/project/SEregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/SEregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX symbol: <http://idi.fundacionctic.org/map-styles/symbols/countries/sweden/counties/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?prefstyle,?regionCode,?region,?ISOCode,?selabel,?altCode,?nuts1,?nuts2,?nuts3,?FIPSCode,?enlabel,?rulabel,?dalabel,?nolabel,?filabel,?lalabel,?STATCode] IN horizontal 
    
  LET ?regionResource = resource(concat("SE_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("SE_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("SE_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionSTATCodeResource = resource(concat("SE_DIVISION_LEVEL_1_STAT_CODE_",replace(?STATCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?svLabel = setLangTag(?region, "sv")
  LET ?seLabel = setLangTag(?selabel, "se")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?ruLabel = setLangTag(?rulabel, "ru")
  LET ?daLabel = setLangTag(?dalabel, "da")
  LET ?noLabel = setLangTag(?nolabel, "no")
  LET ?fiLabel = setLangTag(?filabel, "fi")
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)
  LET ?prefStyle = resource(?prefstyle,symbol)

  WHEN not matches(?lalabel,"") DO
  LET ?laLabel = setLangTag(?lalabel, "la")


CONSTRUCT {
  
  emergelModules:SERegionList a emergelModules:Country;
                              rdfs:label "Ruoŧa leanat"@se ;
                              skos:prefLabel "Ruoŧa leanat"@se ;
                              rdfs:label "Sveriges län"@sv ;
                              skos:prefLabel "Sveriges län"@sv ;
                              rdfs:label "Counties of Sweden"@en ;
                              skos:prefLabel "Counties of Sweden"@en ;
                              rdfs:label "Svenske län"@da ;
                              skos:prefLabel "Svenske län"@da ;
                              rdfs:label "Sveriges len"@no ;
                              skos:prefLabel "Sveriges len"@no ;
                              rdfs:label "Ruotsin läänit"@fi ;
                              skos:prefLabel "Ruotsin läänit"@fi ;
                              rdfs:label "Административное деление Швеции: Лены Швеции"@ru ;
                              skos:prefLabel "Административное деление Швеции: Лены Швеции"@ru .
  
  emergelModules:SERegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the counties of Sweden" ;
                                   skos:prefLabel "ISO codes for the counties of Sweden" ;
                                   rdfs:label "ISO codes for the counties of Sweden"@en ;
                                   skos:prefLabel "ISO codes for the counties of Sweden"@en ;
                                   rdfs:label "ISO 3166-2-koderna för Sverige"@sv ;
                                   skos:prefLabel "ISO 3166-2-koderna för Sverige"@sv .


   emergelModules:SERegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the counties of Sweden" ;
                                   skos:prefLabel "FIPS codes for the counties of Sweden" ;
                                   rdfs:label "FIPS codes for the counties of Sweden"@en ;
                                   skos:prefLabel "FIPS codes for the counties of Sweden"@en ;
                                   rdfs:label "FIPS-koderna för Sverige"@sv ;
                                   skos:prefLabel "FIPS-koderna för Sverige"@sv .

   emergelModules:SERegionSTATCodeList a emergelModules:Country;
                                   rdfs:label "Statistics Sweden (Statistiska centralbyrån) codes for the counties of Sweden" ;
                                   skos:prefLabel "Statistics Sweden (Statistiska centralbyrån) codes for the counties of Sweden" ;
                                   rdfs:label "Statistics Sweden (Statistiska centralbyrån) codes for the counties of Sweden"@en ;
                                   skos:prefLabel "Statistics Sweden (Statistiska centralbyrån) codes for the counties of Sweden"@en ;
                                   rdfs:label "Statistik-koderna för Sverige"@sv ;
                                   skos:prefLabel "Statistik-koderna för Sverige"@sv .                               

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .

   emergelModules:SECountyCoatOfArmsList a emergelModules:Country;
                                   rdfs:label "Coats of arms for the counties of Sweden"@en ;
                                   skos:prefLabel "Coats of arms for the counties of Sweden"@en ;
                                   rdfs:label "Sveriges län: vapnen"@sv ;
                                   skos:prefLabel "Sveriges län: vapnen"@sv .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?svLabel ;
                  skos:prefLabel ?seLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:prefLabel ?daLabel ;
                  skos:prefLabel ?noLabel ;
                  skos:prefLabel ?fiLabel ;
                  skos:inScheme emergelModules:SERegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionSTATCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource ;
                  emergel:prefStyle ?prefStyle ;
                  emergel:hasCode ?nuts3Res .

  ?nuts3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts2Res ;
           skos:broader ?nuts1Res .
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList ;
           skos:broader ?nuts1Res .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nuts2Res .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:altLabel ?altCode ;
                  skos:inScheme emergelModules:SERegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ;
                  skos:inScheme emergelModules:SERegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionSTATCodeResource a skos:Concept;
                  rdfs:label ?STATCode ;
                  skos:prefLabel ?STATCode ;
                  skos:inScheme emergelModules:SERegionSTATCodeList;
                  emergel:codeOf ?regionResource .

   ?prefStyle a emergel:Symbol ;
             rdfs:label ?region ;
             skos:inScheme emergelModules:SECountyCoatOfArmsList ;
             skos:prefLabel ?region .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?laLabel .
 
}