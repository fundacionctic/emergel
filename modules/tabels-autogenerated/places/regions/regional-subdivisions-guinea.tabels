PREFIX project: <http://idi.fundacionctic.org/tabels/project/GNregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/GNregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
WHEN not matches(?rowId,"") DO
    MATCH [?ISOCode,?regionCode,?FIPSCode,?region,?enlabel,?ptlabel,?suslabel,?manlabel,?mkulabel,?ennote,?frnote,?ptnote,?pop2014,?areakm2] IN horizontal 
    
  LET ?regionResource = resource(concat("GN_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("GN_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("GN_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
    LET ?swCountryLabel = setLangTag("Guinea", "sw")
    LET ?kgCountryLabel = setLangTag("Ginea", "kg")
    LET ?amCountryLabel = setLangTag("ጊኔ", "am")
    LET ?woCountryLabel = setLangTag("Gine", "wo")
    LET ?yoCountryLabel = setLangTag("Guinea", "yo")
    LET ?ffCountryLabel = setLangTag("Gine", "ff")

  LET ?frLabel = setLangTag(?region, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?ptLabel = setLangTag(?ptlabel, "pt")
  LET ?enNoteLangLabel = setLangTag(?ennote, "en")
  LET ?frNoteLangLabel = setLangTag(?frnote, "fr")
  LET ?ptNoteLangLabel = setLangTag(?ptnote, "pt")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  
{ WHEN not matches(?suslabel,"") DO
  LET ?susLangLabel = setLangTag(?suslabel, "sus")
  ;
  WHEN not matches(?manlabel,"") DO
  LET ?manLangLabel = setLangTag(?manlabel, "man")
  ;
  WHEN not matches(?mkulabel,"") DO
  LET ?mkuLangLabel = setLangTag(?mkulabel, "mku")
}

CONSTRUCT {
  
  emergelModules:GNRegionList a emergelModules:Country;
                              rdfs:label "Regions of Guinea" ;
                              skos:prefLabel "Regions of Guinea" ;
                              rdfs:label "Regions of Guinea"@en ;
                              skos:prefLabel "Regions of Guinea"@en ;
                              rdfs:label "Regiões da Guiné"@pt ;
                              skos:prefLabel "Regiões da Guiné"@pt ;
                              rdfs:label "Régions du Guinée"@fr ;
                              skos:prefLabel "Régions du Guinée"@fr .
  
  emergelModules:GNRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of Guinea"@en ;
                                   skos:prefLabel "ISO codes for the regions of Guinea"@en ;
                                   rdfs:label "ISO codes for the regions of Guinea" ;
                                   skos:prefLabel "ISO codes for the regions of Guinea" ;
                                   rdfs:label "Codes ISO des régions du Guinée"@fr ;
                                   skos:prefLabel "Codes ISO des régions du Guinée"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:GNRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of Guinea"@en ;
                                   skos:prefLabel "FIPS codes for the regions of Guinea"@en ;
                                   rdfs:label "FIPS codes for the regions of Guinea" ;
                                   skos:prefLabel "FIPS codes for the regions of Guinea" ;
                                   rdfs:label "Codes FIPS des régions du Guinée"@fr ;
                                   skos:prefLabel "Codes FIPS des régions du Guinée"@fr ;
                                   dct:publisher ?FIPSPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:note ?enNoteLangLabel ;
                  skos:note ?frNoteLangLabel ;
                  skos:note ?ptNoteLangLabel ;
                  skos:inScheme emergelModules:GNRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?swCountryLabel ;
               skos:prefLabel ?amCountryLabel ;
               skos:prefLabel ?yoCountryLabel ;
               skos:prefLabel ?woCountryLabel ;
               skos:prefLabel ?ffCountryLabel ;
               skos:prefLabel ?kgCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:GNRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GNRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?susLangLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?manLangLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?mkuLangLabel .
}