PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsMA/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsMA/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?region,?arlabel,?frlabel,?aralt1label,?berlabel,?beraltlabel,?FIPSCode,?eslabel,?enlabel,?ptlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("MA_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MA_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?ar1Label = setLangTag(?region, "ar-Latn")
  LET ?arLabel = setLangTag(?arlabel, "ar")
  LET ?esLabel = setLangTag(?eslabel, "es")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?frLabel = setLangTag(?frlabel, "fr")
  LET ?ptLabel = setLangTag(?ptlabel, "pt")

{
 WHEN NOT MATCHES (?FIPSCode,"") DO
  LET ?regionFIPSCodeResource = resource(concat("MA_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
;
  WHEN not matches (?berlabel, "") DO
  LET ?berLangLabel = setLangTag(?berlabel, "ber")
;
  WHEN not matches (?beraltlabel, "") DO
  LET ?beraltLangLabel = setLangTag(?beraltlabel, "ber-Tfng")
;
  WHEN not matches (?aralt1label, "") DO
  LET ?aralt1LangLabel = setLangTag(?aralt1label, "ar-Latn")
}    

CONSTRUCT {
  
  emergelModules:MARegionList a emergelModules:Country;
                              rdfs:label "Regions of Morocco" ;
                              skos:prefLabel "Regions of Morocco" ;
                              rdfs:label "Regions of Morocco"@en ;
                              skos:prefLabel "Regions of Morocco"@en ;
                              rdfs:label "Régions administratives du Maroc"@fr ;
                              skos:prefLabel "Régions administratives du Maroc"@fr ;
                              rdfs:label "Regiones de Marruecos"@es ;
                              skos:prefLabel "Regiones de Marruecos"@es ;
                              rdfs:label "Regiões de Marrocos"@pt ;
                              skos:prefLabel "Regiões de Marrocos"@pt ;
                              rdfs:label "التقسيم الإداري المغربي"@ar ;
                              skos:prefLabel "التقسيم الإداري المغربي"@ar .
  
  emergelModules:MARegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the administrative regions of Morocco" ;
                                   skos:prefLabel "ISO codes for the administrative regions of Morocco" ;
                                   rdfs:label "ISO codes for the administrative regions of Morocco"@en ;
                                   skos:prefLabel "ISO codes for the administrative regions of Morocco"@en ;
                                   rdfs:label "Codes ISO 3166-2 pour les régions administratives du Marroc"@fr ;
                                   skos:prefLabel "Codes ISO 3166-2 pour les régions administratives du Marroc"@fr ;
                                   rdfs:label "Códigos ISO 3166-2 de las regiones administrativas de Marruecos"@es ;
                                   skos:prefLabel "Códigos ISO 3166-2 de las regiones administrativas de Marruecos"@es ;
                                   rdfs:label "Códigos ISO 3166-2 das regiões administrativas de Marrocos"@pt ;
                                   skos:prefLabel "Códigos ISO 3166-2 das regiões administrativas de Marrocos"@pt ;
                                   rdfs:label "Codis ISO 3166-2 de les regions administratives del Marroc"@ca ;
                                   skos:prefLabel "Codis ISO 3166-2 de les regions administratives del Marroc"@ca ;
                                   rdfs:label "أيزو 3166-2:MA - التقسيم الإداري المغربي"@ar ;
                                   skos:prefLabel "أيزو 3166-2:MA - التقسيم الإداري المغربي"@ar .

  emergelModules:MARegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the administrative regions of Morocco"@en ;
                                   skos:prefLabel "FIPS codes for the administrative regions of Morocco"@en ;
                                   rdfs:label "Codes FIPS pour les régions administratives du Marroc"@fr ;
                                   skos:prefLabel "Codes FIPS pour les régions administratives du Marroc"@fr ;
                                   rdfs:label "Códigos FIPS de las regiones administrativas de Marruecos"@es ;
                                   skos:prefLabel "Códigos FIPS de las regiones administrativas de Marruecos"@es ;
                                   rdfs:label "Códigos FIPS das regiões administrativas de Marrocos"@pt ;
                                   skos:prefLabel "Códigos FIPS das regiões administrativas de Marrocos"@pt ;
                                   rdfs:label "Codis FIPS de les regions administratives del Marroc"@ca ;
                                   skos:prefLabel "Codis FIPS de les regions administratives del Marroc"@ca ;
                                   rdfs:label "FIPS - التقسيم الإداري المغربي"@ar ;
                                   skos:prefLabel "FIPS - التقسيم الإداري المغربي"@ar .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?ar1Label ;
                  skos:prefLabel ?arLabel ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?esLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:inScheme emergelModules:MARegionList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .  
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MARegionISOCodeList;
                  emergel:codeOf ?regionResource .

}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionFIPSCodeResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:MARegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?berLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?aralt1LangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?beraltLangLabel .
}
