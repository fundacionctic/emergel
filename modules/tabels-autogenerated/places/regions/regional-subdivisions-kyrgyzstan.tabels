PREFIX project: <http://idi.fundacionctic.org/tabels/project/KGregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/KGregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX symbol: <http://idi.fundacionctic.org/map-styles/symbols/countries/kazakhstan/provinces/symbols/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>



FOR ?rowId IN rows FILTER get-row(?rowId)
  WHEN not matches(?rowId,"") DO
    MATCH [?regionCode, ?FIPSCode, ?region, ?kyaltlabel, ?kylatlabel, ?rulabel, ?rulatlabel, ?rulatlabel1, ?ennote, ?runote, ?kynote, ?kanote, ?zhnote, ?ISOCode, ?enlabel, ?falabel, ?kalabel, ?kalatlabel, ?kaaralabel, ?uzlabel, ?uzlatlabel, ?zhlabel, ?capital, ?capitalCode] IN horizontal 
      
    LET ?regionResource = resource(concat("KG_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
    LET ?regionCodeResource = resource(concat("KG_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
    LET ?FIPSCodeResource = resource(concat("KG_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
    LET ?ISOResource = resource("KGZ",euCountry)
    /*name of the country in local languages*/
    LET ?kkCountryLabel = setLangTag("Қырғызстан", "kk")
    LET ?kyCountryLabel = setLangTag("Кыргызстан", "ky")
    LET ?uzCountryLabel = setLangTag("Қирғизистон", "uz")
    LET ?tgCountryLabel = setLangTag("Қирғизистон", "tg")
    LET ?ugCountryLabel = setLangTag("قىرغىزىستان", "ug")
    
    LET ?kyLabel = setLangTag(?region, "ky")
    LET ?kylatLangLabel = setLangTag(?kylatlabel, "ky-Latn")
    LET ?kyaltLangLabel = setLangTag(?kyaltlabel, "ky")
    LET ?ruLangLabel = setLangTag(?rulabel, "ru")
    LET ?rulatLangLabel = setLangTag(?rulatlabel, "ru-Latn")
    LET ?rualtLangLabel = setLangTag(?rulatlabel1, "ru-Latn")
    LET ?enLangLabel = setLangTag(?enlabel, "en")
    LET ?faLangLabel = setLangTag(?falabel, "fa")
    LET ?kkLangLabel = setLangTag(?kalabel, "kk")
    LET ?kkLatnLangLabel = setLangTag(?kalatlabel, "kk-Latn")
    LET ?kkArabLangLabel = setLangTag(?kaaralabel, "kk-Arab")
    LET ?uzLangLabel = setLangTag(?uzlabel, "uz")
    LET ?uzLatnLangLabel = setLangTag(?uzlatlabel, "uz-Latn")
    LET ?zhLangLabel = setLangTag(?zhlabel, "zh")
    LET ?notekkLang = setLangTag(?kanote, "kk")
    LET ?noteruLang = setLangTag(?runote, "ru")
    LET ?noteenLang = setLangTag(?ennote, "en")
    LET ?notekyLang = setLangTag(?kynote, "ky")
    LET ?notezhLang = setLangTag(?zhnote, "zh")
  
    // kk-Arab, kk-Latn, kk-Cyrl

  
  When not matches(?capitalCode,"") do
   LET ?capitalResource = resource(concat("KG_PLACE_",replace(?capitalCode," ","_")),emergelModules)
   LET ?capitalCodeResource = resource(concat("KG_PLACE_LOCODE_",replace(?capitalCode," ","_")),emergelModules)
    
  
    //The province type could be implemented as a scheme, concept type, subclass relation,...
     

CONSTRUCT {
  
  emergelModules:KGProvinceLevelDivisionList a emergelModules:Country;
                              rdfs:label "Provinces of Kyrgyzstan" ;
                              skos:prefLabel "Provinces of Kyrgyzstan" ;
                              rdfs:label "Provinces of Kyrgyzstan"@en ;
                              skos:prefLabel "Provinces of Kyrgyzstan"@en ;
                              rdfs:label "Қырғызстанның әкімшілік бөлінісі"@kk ;
                              skos:prefLabel "Қырғызстанның әкімшілік бөлінісі"@kk ;
                              rdfs:label "Qırğızstannıñ äkimşilik bölinisi"@kk-Latn ;
                              skos:prefLabel "Qırğızstannıñ äkimşilik bölinisi"@kk-Latn ;
                              rdfs:label "قىرعىزستاننىڭ اكىمشىلىك ٴبولىنىسى"@kk-Arab ;
                              skos:prefLabel "قىرعىزستاننىڭ اكىمشىلىك ٴبولىنىسى"@kk-Arab ;
                              rdfs:label "Административное деление Киргизии - Первый уровень"@ru ;
                              skos:prefLabel "Административное деление Киргизии - Первый уровень"@ru ;
                              rdfs:label "Administrativnoe delenie Kirgizii - Pervyj uroven’"@ru-Latn ;
                              skos:prefLabel "Administrativnoe delenie Kirgizii - Pervyj uroven’"@ru-Latn ;
                              rdfs:label "Кыргызстандын административдик бөлүнүшү"@ky ;
                              skos:prefLabel "Кыргызстандын административдик бөлүнүшү"@ky ;
                              rdfs:label "吉尔吉斯斯坦行政区划"@zh ;
                              skos:prefLabel "吉尔吉斯斯坦行政区划"@zh ;
                              rdfs:label "Қирғизистоннинг ҳудудий бўлиниши"@uz ;
                              skos:prefLabel "Қирғизистоннинг ҳудудий бўлиниши"@uz ;
                              rdfs:label "Qirgʻizistonning hududiy boʻlinishi"@uz-Latn ;
                              skos:prefLabel "Qirgʻizistonning hududiy boʻlinishi"@uz-Latn .
  
  emergelModules:KGProvinceLevelDivisionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Kyrgyzstan" ;
                                   skos:prefLabel "ISO codes for the provinces of Kyrgyzstan" ;
                                   rdfs:label "ISO codes for the provinces of Kyrgyzstan"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Kyrgyzstan"@en .
  
  emergelModules:KGProvinceLevelDivisionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Kyrgyzstan" ;
                                   skos:prefLabel "FIPS codes for the provinces of Kyrgyzstan" ;
                                   rdfs:label "FIPS codes for the provinces of Kyrgyzstan"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Kyrgyzstan"@en .
  
  emergelModules:KGPlacesList a emergelModules:Country;
                                   rdfs:label "Places in Kyrgyzstan"@en ;
                                   skos:prefLabel "Places in Kyrgyzstan"@en .
  
  emergelModules:KGPlacesLOCODEList a emergelModules:Country;
                                   rdfs:label "UN/LOCODEs for places in Kyrgyzstan"@en ;
                                   skos:prefLabel "UN/LOCODEs for places in Kyrgyzstan"@en .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?kyLabel ; 
                  skos:prefLabel ?kylatLangLabel ;
                  skos:prefLabel ?ruLangLabel ;
                  skos:prefLabel ?rulatLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?faLangLabel ;
                  skos:prefLabel ?zhLangLabel ;
                  skos:prefLabel ?kkLangLabel ;
                  skos:prefLabel ?kkLatnLangLabel ;
                  skos:prefLabel ?kkArabLangLabel ;
                  skos:prefLabel ?uzLangLabel ;
                  skos:prefLabel ?uzLatnLangLabel ;
                  skos:altLabel ?rualtLangLabel ;
                  skos:altLabel ?kyaltLangLabel ;
                  skos:note ?notekkLang ;
                  skos:note ?noteruLang ;
                  skos:note ?noteenLang ;
                  skos:note ?notezhLang ;
                  skos:note ?notekyLang ;
                  skos:inScheme emergelModules:KGProvinceLevelDivisionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?FIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .

    
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?kkCountryLabel ;
               skos:prefLabel ?kyCountryLabel ;
               skos:prefLabel ?uzCountryLabel ;
               skos:prefLabel ?tgCountryLabel ;
               skos:prefLabel ?ugCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:KGProvinceLevelDivisionISOCodeList ;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:KGProvinceLevelDivisionFIPSCodeList ;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{  
   ?regionResource emergel:capital ?capitalResource ;
                   skos:narrower ?capitalResource .
  
  ?capitalResource a skos:Concept ;
                   rdfs:label ?capital ;
                   skos:prefLabel ?capital ;
                   emergel:hasCode ?capitalCodeResource ;
                   skos:inScheme emergelModules:KGPlacesList ;
                   skos:broader ?regionResource .
  
  ?capitalCodeResource a skos:Concept ;
                   rdfs:label ?capitalCode ;
                   skos:prefLabel ?capitalCode ;
                   emergel:codeOf ?capitalResource ;
                   skos:inScheme emergelModules:KGPlacesLOCODEList .
                     
}
