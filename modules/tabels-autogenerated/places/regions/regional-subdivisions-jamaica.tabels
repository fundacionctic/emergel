PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsJM/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsJM/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?PCCode,?ISOCode,?region,?es,?esalt,?HASCCode,?STATINCode] IN horizontal 
    
  LET ?regionResource = resource(concat("JM_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("JM_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("JM_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("JM_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionPCCodeResource = resource(concat("JM_DIVISION_LEVEL_1_PC_CODE_",replace(?PCCode,"-","_")),emergelModules)
  LET ?regionSTATINCodeResource = resource(concat("JM_DIVISION_LEVEL_1_STATIN_CODE_",replace(?STATINCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
    LET ?jamCountryLabel = setLangTag("Jamaica", "jam")
    LET ?arwCountryLabel = setLangTag("Xaymaca", "arw")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?STATINPublisher = resource("STANDARDIZATION_ORGANIZATION_STATIN",emergelModules)

  LET ?enLabel = setLangTag(?region, "en")
  LET ?esLabel = setLangTag(?es, "es")
  LET ?esaltLabel = setLangTag(?esalt, "es")
    

CONSTRUCT {
  
  emergelModules:JMParishList a emergelModules:Country;
                              rdfs:label "Parishes of Jamaica" ;
                              skos:prefLabel "Parishes of Jamaica" ;
                              rdfs:label "Parishes of Jamaica"@en ;
                              skos:prefLabel "Parishes of Jamaica"@en ;
                              rdfs:label "Parroquias de Jamaica"@es ;
                              skos:prefLabel "Parroquias de Jamaica"@es .
  
  emergelModules:JMParishISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the parishes of Jamaica" ;
                                   skos:prefLabel "ISO codes for the parishes of Jamaica" ;
                                   rdfs:label "ISO codes for the parishes of Jamaica"@en ;
                                   skos:prefLabel "ISO codes for the parishes of Jamaica"@en ;
                                   rdfs:label "Códigos ISO de las parroquias de Jamaica"@es ;
                                   skos:prefLabel "Códigos ISO de las parroquias de Jamaica"@es ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:JMRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the parishes of Jamaica" ;
                                   skos:prefLabel "FIPS codes for the parishes of Jamaica" ;
                                   rdfs:label "FIPS codes for the parishes of Jamaica"@en ;
                                   skos:prefLabel "FIPS codes for the parishes of Jamaica"@en ;
                                   rdfs:label "Códigos FIPS de las parroquias de Jamaica"@es ;
                                   skos:prefLabel "Códigos FIPS de las parroquias de Jamaica"@es ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:JMRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Jamaica" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Jamaica" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Jamaica"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Jamaica"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de Jamaica"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de Jamaica"@es ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:JMRegionSTATINCodeList a emergelModules:Country;
                                   rdfs:label "STATIN codes for the parishes of Jamaica" ;
                                   skos:prefLabel "STATIN codes for the parishes of Jamaica" ;
                                   rdfs:label "STATIN codes for the parishes of Jamaica"@en ;
                                   skos:prefLabel "STATIN codes for the parishes of Jamaica"@en ;
                                   rdfs:label "Códigos STATIN de las parroquias de Jamaica"@es ;
                                   skos:prefLabel "Códigos STATIN de las parroquias de Jamaica"@es ;
                                   dct:publisher ?STATINPublisher .

  emergelModules:JMRegionPCCodeList a emergelModules:Country;
                                   rdfs:label "Postal code prefixes for the parishes of Jamaica" ;
                                   skos:prefLabel "Postal code prefixes for the parishes of Jamaica" ;
                                   rdfs:label "Postal code prefixes for the parishes of Jamaica"@en ;
                                   skos:prefLabel "Postal code prefixes for the parishes of Jamaica"@en ;
                                   rdfs:label "Prefijos de los códigos postales de las parroquias de Jamaica"@es ;
                                   skos:prefLabel "Prefijos de los códigos postales de las parroquias de Jamaica"@es .
}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?esaltLabel ;
                  skos:inScheme emergelModules:JMParishList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionSTATINCodeResource ;
                  emergel:hasCode ?regionPCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:JMRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:JMRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionSTATINCodeResource a skos:Concept;
                  rdfs:label ?STATINCode ;
                  skos:prefLabel ?STATINCode ; 
                  skos:inScheme emergelModules:JMRegionSTATINCodeList;
                  emergel:codeOf ?regionResource .

  ?regionPCCodeResource a skos:Concept;
                  rdfs:label ?PCCode ;
                  skos:prefLabel ?PCCode ; 
                  skos:inScheme emergelModules:JMRegionPCCodeList;
                  emergel:codeOf ?regionResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?arwCountryLabel ;
               skos:prefLabel ?jamCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:JMParishISOCodeList;
                  emergel:codeOf ?regionResource .
}
