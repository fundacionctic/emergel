PREFIX project: <http://idi.fundacionctic.org/tabels/project/MRregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/MRregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?ROMNUMCode,?fraltlabel,?region,?enlabel,?arlabel,?arlatlabel,?pop2013,?pop2000,?pop1988,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("MR_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MR_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("MR_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
    LET ?swCountryLabel = setLangTag("Mauritania", "sw")
    LET ?kgCountryLabel = setLangTag("Moritania", "kg")
    LET ?amCountryLabel = setLangTag("ሞሪታኒያ", "am")
    LET ?yoCountryLabel = setLangTag("Mauritáníà", "yo")

  LET ?frLabel = setLangTag(?region, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?arLabel = setLangTag(?arlabel, "ar")
  LET ?arlatLabel = setLangTag(?arlatlabel, "ar-Latn")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)

{
 WHEN NOT MATCHES (?FIPSCode,"") DO
  LET ?regionFIPSCodeResource = resource(concat("MR_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  ;
 WHEN NOT MATCHES (?ROMNUMCode,"") DO
  LET ?regionROMNUMCodeResource = resource(concat("MR_DIVISION_LEVEL_1_ROMNUM_CODE_",replace(?ROMNUMCode,"-","_")),emergelModules)
 ; 
  WHEN not matches(?fraltlabel,"") DO
  LET ?fraltLangLabel = setLangTag(?fraltlabel, "fr")
}

CONSTRUCT {
  
  emergelModules:MRRegionList a emergelModules:Country;
                              rdfs:label "Regions of Mauritania" ;
                              skos:prefLabel "Regions of Mauritania" ;
                              rdfs:label "Regions of Mauritania"@en ;
                              skos:prefLabel "Regions of Mauritania"@en ;
                              rdfs:label "ولايات موريتانيا"@ar ;
                              skos:prefLabel "ولايات موريتانيا"@ar ;
                              rdfs:label "wlạyạt mwrytạnyạ"@ar-Latn ;
                              skos:prefLabel "wlạyạt mwrytạnyạ"@ar-Latn ;
                              rdfs:label "Wilayas de Mauritanie"@fr ;
                              skos:prefLabel "Wilayas de Mauritanie"@fr .
  
  emergelModules:MRRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of Mauritania"@en ;
                                   skos:prefLabel "ISO codes for the regions of Mauritania"@en ;
                                   rdfs:label "ISO codes for the regions of Mauritania" ;
                                   skos:prefLabel "ISO codes for the regions of Mauritania" ;
                                   rdfs:label "Codes ISO des régions du Mauritanie"@fr ;
                                   skos:prefLabel "Codes ISO des régions du Mauritanie"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:MRRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of Mauritania"@en ;
                                   skos:prefLabel "FIPS codes for the regions of Mauritania"@en ;
                                   rdfs:label "FIPS codes for the regions of Mauritania" ;
                                   skos:prefLabel "FIPS codes for the regions of Mauritania" ;
                                   rdfs:label "Codes FIPS des régions du Mauritanie"@fr ;
                                   skos:prefLabel "Codes FIPS des régions du Mauritanie"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:MRRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Mauritania" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Mauritania" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Mauritania"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Mauritania"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des régions du Mauritanie"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des régions du Mauritanie"@fr ;
                                   dct:publisher ?HASCPublisher .

    emergelModules:MRRegionROMNUMCodeList a emergelModules:Country;
                                   rdfs:label "Roman numeral codes for the regions of Mauritania"@en ;
                                   skos:prefLabel "Roman numeral for the regions of Mauritania"@en ;
                                   rdfs:label "Roman numeral for the regions of Mauritania" ;
                                   skos:prefLabel "Roman numeral for the regions of Mauritania" ;
                                   rdfs:label "Codes numéraux romans des régions du Mauritanie"@fr ;
                                   skos:prefLabel "Codes numéraux romans des régions du Mauritanie"@fr .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?arLabel ;
                  skos:prefLabel ?arlatLabel ;
                  skos:inScheme emergelModules:MRRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?swCountryLabel ;
               skos:prefLabel ?amCountryLabel ;
               skos:prefLabel ?yoCountryLabel ;
               skos:prefLabel ?kgCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MRRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:MRRegionHASCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource emergel:hasCode ?regionFIPSCodeResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:MRRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource emergel:hasCode ?regionROMNUMCodeResource .

  ?regionROMNUMCodeResource a skos:Concept;
                  rdfs:label ?ROMNUMCode ;
                  skos:prefLabel ?ROMNUMCode ; 
                  skos:inScheme emergelModules:MRRegionROMNUMCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:altLabel ?fraltLangLabel  .
}