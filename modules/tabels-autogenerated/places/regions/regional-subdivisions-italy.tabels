PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsITA/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsITA/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?HASCCode,?region,?ipait,?ipaitalt,?ipalij,?ipalmo,?ipasc,?ISOCode,?ca,?de,?fr,?frp,?fur,?lij,?lmo,?nap,?oc,?pms,?sc,?scn,?sq,?vec,?FIPSCode,?nuts1,?nuts2,?en,?sl,?hr,?sh,?sr,?mt,?mtalt,?roatara,?ISTATCode] IN horizontal 
    
  LET ?regionResource = resource(concat("IT_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("IT_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("IT_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("IT_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionISTATCodeResource = resource(concat("IT_DIVISION_LEVEL_1_ISTAT_CODE_",replace(?ISTATCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
  LET ?ocCountryLabel = setLangTag("Itàlia", "oc")
  LET ?frpCountryLabel = setLangTag("Étalie", "frp")
  LET ?furCountryLabel = setLangTag("Italie", "fur")
  LET ?emlCountryLabel = setLangTag("Itâglia", "eml")
  LET ?lijCountryLabel = setLangTag("Italia", "lij")
  LET ?lmoCountryLabel = setLangTag("Itàlia", "lmo")
  LET ?laCountryLabel = setLangTag("Italia", "la")
  LET ?arCountryLabel = setLangTag("إيطاليا", "ar")
  LET ?rmCountryLabel = setLangTag("Italia", "rm")
  LET ?napCountryLabel = setLangTag("Italia", "nap")
  LET ?pmsCountryLabel = setLangTag("Italia", "pms")
  //LET ?roa-taraCountryLabel = setLangTag("Itaglie", "roa-tara")
  LET ?scCountryLabel = setLangTag("Itàlia", "sc")
  LET ?scnCountryLabel = setLangTag("Tàlia", "scn")

  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)  
   
  LET ?itLabel = setLangTag(?region, "it")

  LET ?caLangLabel = setLangTag(?ca, "ca")
  LET ?deLangLabel = setLangTag(?de, "de")
  LET ?frLangLabel = setLangTag(?fr, "fr")
  LET ?frpLangLabel = setLangTag(?frp, "frp")
  LET ?furLangLabel = setLangTag(?fur, "fur")
  LET ?lijLangLabel = setLangTag(?lij, "lij")
  LET ?lmoLangLabel = setLangTag(?lmo, "lmo")
  LET ?napLangLabel = setLangTag(?nap, "nap")
  LET ?ocLangLabel = setLangTag(?oc, "oc")
  LET ?pmsLangLabel = setLangTag(?pms, "pms")
  LET ?scLangLabel = setLangTag(?sc, "sc")
  LET ?slLangLabel = setLangTag(?sl, "sl")
  LET ?shLangLabel = setLangTag(?sh, "sh")
  LET ?srLangLabel = setLangTag(?sr, "sr")
  LET ?hrLangLabel = setLangTag(?hr, "hr")
  LET ?enLangLabel = setLangTag(?en, "en")
  LET ?mtLangLabel = setLangTag(?mt, "mt")
  LET ?mtaltLangLabel = setLangTag(?mtalt, "mt")
  LET ?scnLangLabel = setLangTag(?scn, "scn")
  LET ?sqLangLabel = setLangTag(?sq, "sq")
  LET ?vecLangLabel = setLangTag(?vec, "vec")
  LET ?ipaitLangLabel = setLangTag(?ipait, "it-IT-fonipa")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?ISTATPublisher = resource("STANDARDIZATION_ORGANIZATION_ISTAT",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)

  {
    WHEN not matches(?ipaitalt,"") DO
    LET ?ipaitaltLangLabel = setLangTag(?ipaitalt, "it-IT-fonipa")
    ;
    WHEN not matches(?ipalij,"") DO
    LET ?ipalijLangLabel = setLangTag(?ipalij, "lij-IT-fonipa")
    ;
    WHEN not matches(?ipalmo,"") DO
    LET ?ipalmoLangLabel = setLangTag(?ipalmo, "lmo-IT-fonipa")
    ;
    WHEN not matches(?ipasc,"") DO
    LET ?ipascLangLabel = setLangTag(?ipasc, "sc-IT-fonipa")
}

CONSTRUCT {
  
  emergelModules:ITRegionList a emergelModules:Country;
                              rdfs:label "Regions of Italy" ;
                              skos:prefLabel "Regions of Italy" ;
                              rdfs:label "Regions of Italy"@en ;
                              skos:prefLabel "Regions of Italy"@en ;
                              rdfs:label "Regioni d'Italia"@it ;
                              skos:prefLabel "Regioni d'Italia"@it ;
                              rdfs:label "Regions d'Itàlia"@ca ;
                              skos:prefLabel "Regions d'Itàlia"@ca ;
                              rdfs:label "Regjons de Italie"@fur ;
                              skos:prefLabel "Regjons de Italie"@fur ;
                              rdfs:label "Regioin d'Italia"@lij ;
                              skos:prefLabel "Regioin d'Italia"@lij ;
                              rdfs:label "Regiones de s'Itàlia"@sc ;
                              skos:prefLabel "Regiones de s'Itàlia"@sc ;
                              rdfs:label "Rajonet e Italisë"@sq ;
                              skos:prefLabel "Rajonet e Italisë"@sq ;
                              rdfs:label "Regions d'Itàlia"@oc ;
                              skos:prefLabel "Regions d'Itàlia"@oc ;
                              rdfs:label "Riggiuni d'Italia"@scn ;
                              skos:prefLabel "Riggiuni d'Italia"@scn ;
                              rdfs:label "Italienische Regionen"@de ;
                              skos:prefLabel "Italienische Regionen"@de ;
                              rdfs:label "Rejoni de l'Itałia"@vec ;
                              skos:prefLabel "Rejoni de l'Itałia"@vec ;
                              rdfs:label "Regioni d'Italia"@nap ;
                              skos:prefLabel "Regioni d'Italia"@nap ;
                              rdfs:label "Regiuni d'Itàlia"@lmo ;
                              skos:prefLabel "Regiuni d'Itàlia"@lmo ;
                              rdfs:label "Régions de l'Italie"@fr ;
                              skos:prefLabel "Régions de l'Italie"@fr ;
                              rdfs:label "Region d'Italia"@pms ;
                              skos:prefLabel "Region d'Italia"@pms ;
                              rdfs:label "Règ·ions d'Étalie"@frp ;
                              skos:prefLabel "Règ·ions d'Étalie"@frp ;
                              rdfs:label "Upravna delitev Italije"@sl ;
                              skos:prefLabel "Upravna delitev Italije"@sl ;
                              rdfs:label "Talijanske regije"@hr ;
                              skos:prefLabel "Talijanske regije"@hr ;
                              rdfs:label "Regije Italije"@sh ;
                              skos:prefLabel "Regije Italije"@sh ;
                              rdfs:label "Регије Италије"@sr ;
                              skos:prefLabel "Регије Италије"@sr ;
                              rdfs:label "Reġjuni ta' L-Italja"@mt ;
                              skos:prefLabel "Reġjuni ta' L-Italja"@mt .
  
  emergelModules:ITRegionISOCodeList a emergelModules:Country ;
                                   rdfs:label "ISO codes for the regions of Italy" ;
                                   skos:prefLabel "ISO codes for the regions of Italy" ;
                                   rdfs:label "ISO codes for the regions of Italy"@en ;
                                   skos:prefLabel "ISO codes for the regions of Italy"@en ;
                                   rdfs:label "Codici ISO delle regioni d'Italia"@it ;
                                   skos:prefLabel "Codici ISO delle regioni d'Italia"@it ;
                                   rdfs:label "Codis ISO de les regions d'Itàlia"@ca ;
                                   skos:prefLabel "Codis ISO de les regions d'Itàlia"@ca ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:ITRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of Italy" ;
                                   skos:prefLabel "FIPS codes for the regions of Italy" ;
                                   rdfs:label "FIPS codes for the regions of Italy"@en ;
                                   skos:prefLabel "FIPS codes for the regions of Italy"@en ;
                                   rdfs:label "Codici FIPS delle regioni d'Italia"@it ;
                                   skos:prefLabel "Codici FIPS delle regioni d'Italia"@it ;
                                   rdfs:label "Codis FIPS de les regions d'Itàlia"@ca ;
                                   skos:prefLabel "Codis FIPS de les regions d'Itàlia"@ca ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:ITRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Italy" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Italy" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Italy"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Italy"@en ;
                                   rdfs:label "Codici HASC (Hierarchical administrative subdivision codes) delle regioni d'Italia"@it ;
                                   skos:prefLabel "Codici HASC (Hierarchical administrative subdivision codes) delle regioni d'Italia"@it ;
                                   rdfs:label "Codis HASC (Hierarchical administrative subdivision codes) de les regions d'Itàlia"@ca ;
                                   skos:prefLabel "Codis HASC (Hierarchical administrative subdivision codes) de les regions d'Itàlia"@ca ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:ITRegionISTATCodeList a emergelModules:Country;
                                   rdfs:label "ISTAT codes for the regions of Italy" ;
                                   skos:prefLabel "ISTAT codes for the regions of Italy" ;
                                   rdfs:label "ISTAT codes for the regions of Italy"@en ;
                                   skos:prefLabel "ISTAT codes for the regions of Italy"@en ;
                                   rdfs:label "Codici ISTAT delle regioni d'Italia"@it ;
                                   skos:prefLabel "Codici ISTAT delle regioni d'Italia"@it ;
                                   rdfs:label "Codis ISTAT de les regions d'Itàlia"@ca ;
                                   skos:prefLabel "Codis ISTAT de les regions d'Itàlia"@ca ;
                                   dct:publisher ?ISTATPublisher .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   dct:publisher ?EurostatPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?itLabel ;
                  skos:prefLabel ?caLangLabel ;
                  skos:prefLabel ?deLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?slLangLabel ;
                  skos:prefLabel ?frLangLabel ;
                  skos:prefLabel ?frpLangLabel ;
                  skos:prefLabel ?furLangLabel ;
                  skos:prefLabel ?lijLangLabel ;
                  skos:prefLabel ?lmoLangLabel ;
                  skos:prefLabel ?napLangLabel ;
                  skos:prefLabel ?ocLangLabel ;
                  skos:prefLabel ?pmsLangLabel ;
                  skos:prefLabel ?scLangLabel ;
                  skos:prefLabel ?shLangLabel ;
                  skos:prefLabel ?srLangLabel ;
                  skos:prefLabel ?hrLangLabel ;
                  skos:prefLabel ?scnLangLabel ;
                  skos:prefLabel ?sqLangLabel ;
                  skos:prefLabel ?mtLangLabel ;
                  skos:prefLabel ?vecLangLabel ;
                  skos:altLabel ?mtaltLangLabel ;
                  skos:prefLabel ?ipaitLangLabel ;
                  ontolex:phoneticRep ?ipaitLangLabel ;
                  skos:inScheme emergelModules:ITRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionISTATCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?ocCountryLabel ;
               skos:prefLabel ?laCountryLabel ;
               skos:prefLabel ?arCountryLabel ;
               skos:prefLabel ?lijCountryLabel ;
               skos:prefLabel ?lmoCountryLabel ;
               skos:prefLabel ?furCountryLabel ;
               skos:prefLabel ?rmCountryLabel ;
               skos:prefLabel ?pmsCountryLabel ;
               skos:prefLabel ?frpCountryLabel ;
               skos:prefLabel ?napCountryLabel ;
               //skos:prefLabel ?roa-taraCountryLabel ;
               skos:prefLabel ?emlCountryLabel ;
               skos:prefLabel ?scCountryLabel ;
               skos:prefLabel ?scnCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:ITRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:ITRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:ITRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionISTATCodeResource a skos:Concept;
                  rdfs:label ?ISTATCode ;
                  skos:prefLabel ?ISTATCode ; 
                  skos:inScheme emergelModules:ITRegionISTATCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource emergel:hasCode ?nuts2Res .
  
  ?nuts2Res a skos:Concept;
           skos:inScheme emergelModules:NUTS2CodeList ;
           emergel:codeOf ?regionResource ;
           skos:broader ?nuts1Res .

  ?nuts1Res a skos:Concept;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nuts2Res .
}

CONSTRUCT {
  ?regionResource skos:altLabel ?ipaitaltLangLabel ;
                    ontolex:phoneticRep ?ipaitaltLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?ipalijLangLabel ;
                    ontolex:phoneticRep ?ipalijLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?ipalmoLangLabel ;
                    ontolex:phoneticRep ?ipalmoLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?ipascLangLabel ;
                    ontolex:phoneticRep ?ipascLangLabel .
}