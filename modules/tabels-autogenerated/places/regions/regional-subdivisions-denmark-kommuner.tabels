PREFIX project: <http://idi.fundacionctic.org/tabels/project/kommunerDKN/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/kommunerDKN/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?nuts3,?kommune,?daaltlabel,?kommuneCode,?enlabel] IN horizontal 

  LET ?kommuneResource = resource(concat("DK_DIVISION_LEVEL_3_",replace(?kommuneCode,"-","_")),emergelModules)
  LET ?kommuneCodeResource = resource(concat("DK_DIVISION_LEVEL_3_LAU1_CODE_",replace(?kommuneCode,"-","_")),emergelModules)
  LET ?provinceResource = resource(concat("DK_DIVISION_LEVEL_2_",replace(?nuts3,"-","_")),emergelModules)

  LET ?daLangLabel = setLangTag(?kommune, "da")
  LET ?enLangLabel = setLangTag(?enlabel, "en")
  LET ?nut3Res = resource(?nuts3, nuts)

  LET ?DanmarksStatistikPublisher = resource("STANDARDIZATION_ORGANIZATION_DanmarksStatistik",emergelModules)

   WHEN not matches(?daaltlabel,"") DO
   LET ?daaltlabelLangLabel = setLangTag(?daaltlabel, "da")

CONSTRUCT {
  
  emergelModules:DKMunicipalityList a emergelModules:Country;
                              rdfs:label "Municipalities of Denmark" ;
                              skos:prefLabel "Municipalities of Denmark" ;
                              rdfs:label "Municipalities of Denmark"@en ;
                              skos:prefLabel "Municipalities of Denmark"@en ;
                              rdfs:label "Kommuner i Danmark"@da ;
                              skos:prefLabel "Kommuner i Danmark"@da ;
                              rdfs:label "Danmarks kommuner"@sv ;
                              skos:prefLabel "Danmarks kommuner"@sv ;
                              rdfs:label "Danmarks kommuner"@no ;
                              skos:prefLabel "Danmarks kommuner"@no ;
                              rdfs:label "Dänemarks Kommunen"@de ;
                              skos:prefLabel "Dänemarks Kommunen"@de .
  
  emergelModules:DKMunicipalityCodeList a emergelModules:Country;
                                   rdfs:label "Danmarks Statistik codes for the municipalities of Denmark"@en ;
                                   skos:prefLabel "Danmarks Statistik codes for the municipalities of Denmark"@en ;
                                   rdfs:label "Listen over Danmarks Statistik-koder for Danmark"@da ;
                                   skos:prefLabel "Listen over Danmarks Statistik-koder for Danmark"@da ;
                                   dct:publisher ?DanmarksStatistikPublisher .
}

CONSTRUCT {
  ?kommuneResource a skos:Concept;
                  rdfs:label ?kommune ;
                  skos:prefLabel ?kommune ; 
                  skos:prefLabel ?daLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:inScheme emergelModules:DKMunicipalityList;
                  emergel:hasCode ?kommuneCodeResource ;
                  skos:notation ?kommuneCode ;
                  skos:broader ?provinceResource .

  ?provinceResource skos:narrower ?kommuneResource .
  
  ?kommuneCodeResource a skos:Concept;
                  rdfs:label ?kommuneCode ;
                  skos:prefLabel ?kommuneCode ; 
                  skos:inScheme emergelModules:DKMunicipalityCodeList;
                  emergel:codeOf ?kommuneResource .
}

CONSTRUCT {
  ?kommuneResource skos:altLabel ?daaltlabelLangLabel .
}