PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsCZE/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsCZE/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>

FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?HASCCode,?region,?ISOCode,?delabel,?sklabel,?szllabel,?nuts1,?nuts2,?nuts3,?FIPSCode,?enlabel,?PLATECode,?pop2011,?pop2001,?areakm2,?areami2,?capital,?dealtlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("CZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("CZ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("CZ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("CZ_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionPLATECodeResource = resource(concat("CZ_DIVISION_LEVEL_1_CARPLATE_CODE_",replace(?PLATECode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
      /*name of the country in local languages*/
      LET ?szlCountryLabel = setLangTag("Czesko Republika", "szl")

  LET ?csLabel = setLangTag(?region, "cs")
  LET ?deLabel = setLangTag(?delabel, "de")
  LET ?dealtLabel = setLangTag(?dealtlabel, "de")
  LET ?skLabel = setLangTag(?sklabel, "sk")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)

WHEN not matches(?szllabel,"") DO
LET ?szlLangLabel = setLangTag(?szllabel, "szl") 
    

CONSTRUCT {
  
  emergelModules:CZRegionList a emergelModules:Country;
                              rdfs:label "Regions of the Czech Republic" ;
                              skos:prefLabel "Regions of the Czech Republic" ;
                              rdfs:label "Regions of the Czech Republic"@en ;
                              skos:prefLabel "Regions of the Czech Republic"@en ;
                              rdfs:label "Kraje v Česku"@cs ;
                              skos:prefLabel "Kraje v Česku"@cs ;
                              rdfs:label "České kraje"@sk ;
                              skos:prefLabel "České kraje"@sk .
  
  emergelModules:CZRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of the Czech Republic" ;
                                   skos:prefLabel "ISO codes for the regions of the Czech Republic" ;
                                   rdfs:label "ISO codes for the regions of the Czech Republic"@en ;
                                   skos:prefLabel "ISO codes for the regions of the Czech Republic"@en ;
                                   rdfs:label "Kódy ISO 3166-2 pro Česko identifikují českých krajů"@cs ;
                                   skos:prefLabel "Kódy ISO 3166-2 pro Česko identifikují českých krajů"@cs ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:CZRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of the Czech Republic" ;
                                   skos:prefLabel "FIPS codes for the regions of the Czech Republic" ;
                                   rdfs:label "FIPS codes for the regions of the Czech Republic"@en ;
                                   skos:prefLabel "FIPS codes for the regions of the Czech Republic"@en ;
                                   rdfs:label "Kódy FIPS pro Česko identifikují českých krajů"@cs ;
                                   skos:prefLabel "Kódy FIPS pro Česko identifikují českých krajů"@cs ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:CZRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of the Czech Republic" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of the Czech Republic" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of the Czech Republic"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of the Czech Republic"@en ;
                                   rdfs:label "Kódy HASC pro Česko identifikují českých krajů"@cs ;
                                   skos:prefLabel "Kódy HASC pro Česko identifikují českých krajů"@cs ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:CZRegionPLATECodeList a emergelModules:Country;
                                   rdfs:label "Car plate district codes of the regions of the Czech Republic" ;
                                   skos:prefLabel "Car plate district codes of the regions of the Czech Republic" ;
                                   rdfs:label "Car plate district codes of the regions of the Czech Republic"@en ;
                                   skos:prefLabel "Car plate district codes of the regions of the Czech Republic"@en ;
                                   rdfs:label "Státní poznávací značky v Česku: Kódy pro Česko identifikují českých krajů"@cs ;
                                   skos:prefLabel "Státní poznávací značky v Česku: Kódy pro Česko identifikují českých krajů"@cs .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   rdfs:label "Liste der NUTS-Regionen 1"@de ;
                                   skos:prefLabel "Liste der NUTS-Regionen 1"@de ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   rdfs:label "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 1"@pl ;
                                   skos:prefLabel "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 1"@pl ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@hr ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@hr ;
                                   rdfs:label "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 1"@ru ;
                                   skos:prefLabel "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 1"@ru ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   rdfs:label "Liste der NUTS-Regionen 2"@de ;
                                   skos:prefLabel "Liste der NUTS-Regionen 2"@de ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   rdfs:label "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 2"@pl ;
                                   skos:prefLabel "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 2"@pl ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@hr ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@hr ;
                                   rdfs:label "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 2"@ru ;
                                   skos:prefLabel "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 2"@ru ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   rdfs:label "Liste der NUTS-Regionen 3"@de ;
                                   skos:prefLabel "Liste der NUTS-Regionen 3"@de ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   rdfs:label "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 3"@pl ;
                                   skos:prefLabel "Klasyfikacja Jednostek Terytorialnych do Celów Statystycznych (NUTS) - Poziom 3"@pl ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@hr ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@hr ;
                                   rdfs:label "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 3"@ru ;
                                   skos:prefLabel "Номенклатура территориальных единиц для целей статистики (NUTS) - уровень 3"@ru ;
                                   dct:publisher ?EurostatPublisher .
}


CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?csLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:altLabel ?dealtLabel ;
                  skos:prefLabel ?skLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:inScheme emergelModules:CZRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionPLATECodeResource ;
                  emergel:hasCode ?nuts3Res ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?szlCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:CZRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:CZRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:CZRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionPLATECodeResource a skos:Concept;
                  rdfs:label ?PLATECode ;
                  skos:prefLabel ?PLATECode ; 
                  skos:inScheme emergelModules:CZRegionPLATECodeList;
                  emergel:codeOf ?regionResource .

  ?nuts3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts2Res .
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts2Res ;
           skos:inScheme emergelModules:NUTS1CodeList .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?szlLangLabel .
 
}
