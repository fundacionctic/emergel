PREFIX project: <http://idi.fundacionctic.org/tabels/project/CODepartments/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/CODepartments/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?nute1,?nute2,?region,?esalt,?ennote,?esnote,?frnote,?ptnote,?qunote,?ISOCode,?en,?fr,?guc,?pt,?qu,?capital,?capitalCode] IN horizontal 
    
  LET ?regionResource = resource(concat("CO_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("CO_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?FIPSCodeResource = resource(concat("CO_DIVISION_LEVEL_1_FIPS_CODE_",?FIPSCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
    LET ?quCountryLabel = setLangTag("Kulumbya", "qu")
    LET ?ayCountryLabel = setLangTag("Kuluwya", "ay")

  LET ?esLabel = setLangTag(?region, "es")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?esaltLabel = setLangTag(?esalt, "es")
  LET ?quLabel = setLangTag(?qu, "qu")
  LET ?enNoteLabel = setLangTag(?ennote,"en")
  LET ?esNoteLabel = setLangTag(?esnote,"es")
  LET ?frNoteLabel = setLangTag(?frnote,"fr")
  LET ?ptNoteLabel = setLangTag(?ptnote,"pt")
  LET ?quNoteLabel = setLangTag(?qunote,"qu")

  LET ?nute1Res = resource(concat("NUTE1_CODE_LIST_",?nute1), emergelModules)
  LET ?nute2Res = resource(concat("NUTE2_CODE_LIST_",?nute2), emergelModules)

{
  WHEN not matches(?capitalCode,"") do
   LET ?capitalResource = resource(concat("CO_PLACE_",replace(?capitalCode," ","_")),emergelModules)
   LET ?capitalCodeResource = resource(concat("CO_PLACE_LOCODE_",replace(?capitalCode," ","_")),emergelModules)
;
  WHEN not matches(?guc,"") DO
  LET ?gucLangLabel = setLangTag(?guc, "guc") 
}

LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
LET ?CANPublisher = resource("STANDARDIZATION_ORGANIZATION_CAN",emergelModules)
LET ?UNPublisher = resource("STANDARDIZATION_ORGANIZATION_UN",emergelModules)
    

CONSTRUCT {
  
  emergelModules:CODepartmentsList a emergelModules:Country ;
                              rdfs:label "Departments of Colombia"@en ;
                              skos:prefLabel "Departments of Colombia"@en ;
                              rdfs:label "Departamentos de Colombia"@es ;
                              skos:prefLabel "Departamentos de Colombia"@es ;                              
                              rdfs:label "Kulumbya - Suyukuna"@qu ;
                              skos:prefLabel "Kulumbya - Suyukuna"@qu ;                              
                              rdfs:label "Departamentos de Colômbia"@pt ;
                              skos:prefLabel "Departamentos de Colômbia"@pt ;                              
                              rdfs:label "Départements de Colombia"@fr ;
                              skos:prefLabel "Départements de Colombia"@fr .
  
  emergelModules:CODepartmentsISOCodeList a emergelModules:Country ;
                                   rdfs:label "ISO codes for the departments of Colombia"@en ;
                                   skos:prefLabel "ISO codes for the departments of Colombia"@en ;
                                   rdfs:label "Códigos ISO de los departamentos de Colombia"@es ;
                                   skos:prefLabel "Códigos ISO de los departamentos de Colombia"@es ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:CODepartmentsFIPSCodeList a emergelModules:Country ;
                                   rdfs:label "FIPS codes for the departments of Colombia"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Colombia"@en ;
                                   rdfs:label "Códigos FIPS de los departamentos de Colombia"@es ;
                                   skos:prefLabel "Códigos FIPS de los departamentos de Colombia"@es ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:COPlacesList a emergelModules:Country;
                                   rdfs:label "Places in Colombia"@en ;
                                   skos:prefLabel "Places in Colombia"@en ;
                                   rdfs:label "Lugares en Colombia"@es ;
                                   skos:prefLabel "Lugares en Colombia"@es .
  
  emergelModules:COPlacesLOCODEList a emergelModules:Country;
                                   rdfs:label "UN/LOCODEs for places in Colombia"@en ;
                                   skos:prefLabel "UN/LOCODEs for places in Colombia"@en ;
                                   rdfs:label "Códigos UN/LOCODEs para lugares en Colombia"@es ;
                                   skos:prefLabel "Códigos UN/LOCODEs para lugares en Colombia"@es ;
                                   dct:publisher ?UNPublisher .

  emergelModules:PlacesLOCODEList a emergelModules:Country;
                                   rdfs:label "UN/LOCODE Code List by Country"@en ;
                                   skos:prefLabel "UN/LOCODE Code List by Country"@en ;
                                   dct:publisher ?UNPublisher .

  emergelModules:NUTE1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Statistical Territorial Units (NUTE ANDINA) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Statistical Territorial Units (NUTE ANDINA) level 1 codes"@en ;
                                   rdfs:label "Nomenclatura de las Unidades Territoriales Estadísticas de la Comunidad Andina (NUTE ANDINA) nivel 1"@es ;
                                   skos:prefLabel "Nomenclatura de las Unidades Territoriales Estadísticas de la Comunidad Andina (NUTE ANDINA) nivel 1"@es ;
                                   dct:publisher ?CANPublisher .

  emergelModules:NUTE2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Statistical Territorial Units (NUTE ANDINA) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Statistical Territorial Units (NUTE ANDINA) level 2 codes"@en ;
                                   rdfs:label "Nomenclatura de las Unidades Territoriales Estadísticas de la Comunidad Andina (NUTE ANDINA) nivel 2"@es ;
                                   skos:prefLabel "Nomenclatura de las Unidades Territoriales Estadísticas de la Comunidad Andina (NUTE ANDINA) nivel 2"@es ;
                                   dct:publisher ?CANPublisher . 
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?esaltLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?quLabel ;
                  skos:note ?enNoteLabel ;
                  skos:note ?quNoteLabel ;
                  skos:note ?frNoteLabel ;
                  skos:note ?ptNoteLabel ;
                  skos:note ?esNoteLabel ;
                  skos:inScheme emergelModules:CODepartmentsList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?FIPSCodeResource ;
                  emergel:hasCode ?nute2Res ;
                  skos:broader ?ISOResource .
  
  ?nute2Res a skos:Concept;
           rdfs:label ?nute2 ;
           skos:prefLabel ?nute2 ; 
           skos:inScheme emergelModules:NUTE2CodeList ;
           skos:broader ?nute1Res .

  ?nute1Res a skos:Concept;
           rdfs:label ?nute1 ;
           skos:prefLabel ?nute1 ; 
           skos:inScheme emergelModules:NUTE1CodeList ;
           skos:narrower ?nute2Res .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?ayCountryLabel ;
               skos:prefLabel ?quCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:CODepartmentsISOCodeList;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:CODepartmentsFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{  
   ?regionResource emergel:capital ?capitalResource ;
                   skos:narrower ?capitalResource .
  
  ?capitalResource a skos:Concept ;
                   rdfs:label ?capital ;
                   skos:prefLabel ?capital ;
                   emergel:hasCode ?capitalCodeResource ;
                   skos:inScheme emergelModules:COPlacesList ;
                   skos:broader ?regionResource .
  
  ?capitalCodeResource a skos:Concept ;
                   rdfs:label ?capitalCode ;
                   skos:prefLabel ?capitalCode ;
                   emergel:codeOf ?capitalResource ;
                   skos:inScheme emergelModules:COPlacesLOCODEList ;
                   skos:inScheme emergelModules:PlacesLOCODEList .
                     
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?gucLangLabel .
}