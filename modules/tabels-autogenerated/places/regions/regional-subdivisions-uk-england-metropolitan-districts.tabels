PREFIX project: <http://idi.fundacionctic.org/tabels/project/2nd-levelGBR/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/2nd-levelGBR/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?provinceCode,?ONSCode,?GSSCode,?OSGRIDCode,?nuts1,?nuts2,?nuts3,?PCAREACode,?FIPSCode,?HASCCode,?province,?note_en,?regionCode,?ISOCode,?enalt,?ipaen,?ipaenalt,?ipaenalt1] IN horizontal 

  LET ?provinceResource = resource(concat("GB_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("GB_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("GB_DIVISION_LEVEL_1_GB_",replace(?regionCode,"-","_")),emergelModules)
  LET ?provinceONSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_ONS_CODE_",replace(?ONSCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?provinceGSSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_GSS_CODE_",replace(?GSSCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("GB_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?provincePCAREACodeResource = resource(concat("GB_POSTCODE_AREA_",replace(?PCAREACode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
    LET ?enLangLabel = setLangTag(?province, "en")
    LET ?enaltLangLabel = setLangTag(?enalt, "en")
    LET ?note_enLang = setLangTag(?note_en, "en")
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)

{
WHEN not matches(?nuts3,"") DO
LET ?nuts3Res = resource(?nuts3, nuts)
  ;
WHEN not matches (?OSGRIDCode,"") DO
LET ?provinceOSGRIDCodeResource = resource(concat("GB_DIVISION_LEVEL_2_OSGRID_CODE_",replace(?OSGRIDCode,"-","_")),emergelModules)
;  
WHEN not matches(?ipaen,"") DO
LET ?ipaenLangLabel = setLangTag(?ipaen, "en-GB-fonipa")
;  
WHEN not matches(?ipaenalt,"") DO
LET ?ipaenaltLangLabel = setLangTag(?ipaenalt, "en-GB-fonipa")
;  
WHEN not matches(?ipaenalt1,"") DO
LET ?ipaenalt1LangLabel = setLangTag(?ipaenalt1, "en-GB-fonipa")
}
  
    

CONSTRUCT {
  
  emergelModules:GB_ENGMetropolitanDistrictsProvinceList a emergelModules:Country;
                              rdfs:label "Metropolitan districts of England" ;
                              skos:prefLabel "Metropolitan districts of England" ;
                              rdfs:label "Metropolitan districts of England"@en ;
                              skos:prefLabel "Metropolitan districts of England"@en .

  emergelModules:GB_ENGProvinceList a emergelModules:Country;
                              rdfs:label "First-level subdivisions of England" ;
                              skos:prefLabel "First-level subdivisions of England" ;
                              rdfs:label "First-level subdivisions of England"@en ;
                              skos:prefLabel "First-level subdivisions of England"@en .
  
  emergelModules:GB_ENGProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the metropolitan districts of England" ;
                                   skos:prefLabel "ISO codes for the metropolitan districts of England" ;
                                   rdfs:label "ISO codes for the metropolitan districts of England"@en ;
                                   skos:prefLabel "ISO codes for the metropolitan districts of England"@en .
  
  emergelModules:GB_ENGProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the metropolitan districts of England" ;
                                   skos:prefLabel "FIPS codes for the metropolitan districts of England" ;
                                   rdfs:label "FIPS codes for the metropolitan districts of England"@en ;
                                   skos:prefLabel "FIPS codes for the metropolitan districts of England"@en .

  emergelModules:GB_ENGProvinceONSCodeList a emergelModules:Country;
                                   rdfs:label "ONS codes for the metropolitan districts of England" ;
                                   skos:prefLabel "ONS codes for the metropolitan districts of England" ;
                                   rdfs:label "ONS codes for the metropolitan districts of England"@en ;
                                   skos:prefLabel "ONS codes for the metropolitan districts of England"@en .

  emergelModules:GB_ENGProvinceGSSCodeList a emergelModules:Country;
                                   rdfs:label "GSS codes for the metropolitan districts of England" ;
                                   skos:prefLabel "GSS codes for the metropolitan districts of England" ;
                                   rdfs:label "GSS codes for the metropolitan districts of England"@en ;
                                   skos:prefLabel "GSS codes for the metropolitan districts of England"@en .

  emergelModules:GB_ENGProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the metropolitan districts of England" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the metropolitan districts of England" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the metropolitan districts of England"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the metropolitan districts of England"@en .

  emergelModules:GBRegionPCAREACodeList a emergelModules:Country;
                                   rdfs:label "Postcode areas of the United Kingdom" ;
                                   skos:prefLabel "Postcode areas of the United Kingdom" ;
                                   rdfs:label "Postcode areas of the United Kingdom"@en ;
                                   skos:prefLabel "Postcode areas of the United Kingdom"@en .

  emergelModules:GB_ENGProvinceOSGRIDReferenceCodeList a emergelModules:Country;
                                   rdfs:label "Ordnance Survey National Grid reference codes for the metropolitan districts of England" ;
                                   skos:prefLabel "Ordnance Survey National Grid reference codes for the metropolitan districts of England" ;
                                   rdfs:label "Ordnance Survey National Grid reference codes for the metropolitan districts of England"@en ;
                                   skos:prefLabel "Ordnance Survey National Grid reference codes for the metropolitan districts of England"@en .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?enLangLabel ;
                  skos:altLabel ?enaltLangLabel ;
                  skos:note ?note_enLang ;
                  skos:inScheme emergelModules:GB_ENGProvinceList;
                  skos:inScheme emergelModules:GB_ENGMetropolitanDistrictsProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceONSCodeResource ;
                  emergel:hasCode ?provinceGSSCodeResource ;
                  emergel:hasCode ?provincePCAREACodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
  
  
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .
  
  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceONSCodeResource a skos:Concept;
                  rdfs:label ?ONSCode ;
                  skos:prefLabel ?ONSCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceONSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceGSSCodeResource a skos:Concept;
                  rdfs:label ?GSSCode ;
                  skos:prefLabel ?GSSCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceGSSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .

  ?provincePCAREACodeResource a skos:Concept;
                  rdfs:label ?PCAREACode ;
                  skos:prefLabel ?PCAREACode ; 
                  skos:inScheme emergelModules:GBRegionPCAREACodeList;
                  emergel:codeOf ?provinceResource .
}


CONSTRUCT {
  ?nuts3Res a skos:Concept;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts3Res ;
           emergel:codeOf ?provinceResource .

  ?provinceResource emergel:hasCode ?nuts3Res .
}


CONSTRUCT {
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts2Res ;
           skos:inScheme emergelModules:NUTS1CodeList .
 } 

CONSTRUCT {

  ?provinceOSGRIDCodeResource a skos:Concept;
                  rdfs:label ?OSGRIDCode ;
                  skos:prefLabel ?OSGRIDCode ; 
                  skos:inScheme emergelModules:GB_ENGProvinceOSGRIDReferenceCodeList;
                  emergel:codeOf ?provinceResource .
   
  ?provinceResource emergel:hasCode ?provinceOSGRIDCodeResource .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?ipaenLangLabel ;
                    ontolex:phoneticRep ?ipaenLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?ipaenaltLangLabel ;
                    ontolex:phoneticRep ?ipaenaltLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?ipaenalt1LangLabel ;
                    ontolex:phoneticRep ?ipaenalt1LangLabel .
}