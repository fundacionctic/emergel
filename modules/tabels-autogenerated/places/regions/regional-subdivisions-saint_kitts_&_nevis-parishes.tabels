PREFIX project: <http://idi.fundacionctic.org/tabels/project/parishesKN/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/parishesKN/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?province,?HASCCode,?provinceCode,?FIPSCode,?pop2001,?areakm2,?areami2,?regionCode,?capital,?ISOCode,?frlabel,?eslabel,?ptlabel] IN horizontal 
    
  LET ?provinceResource = resource(concat("KN_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("KN_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("KN_DIVISION_LEVEL_2_ISO_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("KN_DIVISION_LEVEL_2_ISO_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("KN_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?province = setLangTag(?province, "en")
  LET ?esLanglabel = setLangTag(?eslabel, "es")
  LET ?frLanglabel = setLangTag(?frlabel, "fr")
  LET ?ptLanglabel = setLangTag(?ptlabel, "pt")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

CONSTRUCT {
  
  emergelModules:KNParishList a emergelModules:Country;
                              rdfs:label "Parishes of Saint Kitts and Nevis" ;
                              skos:prefLabel "Parishes of Saint Kitts and Nevis" ;
                              rdfs:label "Parishes of Saint Kitts and Nevis"@en ;
                              skos:prefLabel "Parishes of Saint Kitts and Nevis"@en ;
                              rdfs:label "Parroquias de San Cristóbal y Nieves"@es ;
                              skos:prefLabel "Parroquias de San Cristóbal y Nieves"@es ;
                              rdfs:label "Paróquias de São Cristóvão e Nevis"@pt ;
                              skos:prefLabel "Paróquias de São Cristóvão e Nevis"@pt ;
                              rdfs:label "Paroisses de Saint-Christophe-et-Niévès"@fr ;
                              skos:prefLabel "Paroisses de Saint-Christophe-et-Niévès"@fr .
  
  emergelModules:KNParishISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the parishes of Saint Kitts and Nevis" ;
                                   skos:prefLabel "ISO codes for the parishes of Saint Kitts and Nevis" ;
                                   rdfs:label "ISO codes for the parishes of Saint Kitts and Nevis"@en ;
                                   skos:prefLabel "ISO codes for the parishes of Saint Kitts and Nevis"@en ;
                                   rdfs:label "Códigos ISO de las parroquias de San Cristóbal y Nievesa"@es ;
                                   skos:prefLabel "Códigos ISO de las parroquias de San Cristóbal y Nieves"@es ;
                                   rdfs:label "Códigos ISO das paróquias de São Cristóvão e Nevis"@pt ;
                                   skos:prefLabel "Códigos ISO das paróquias de São Cristóvão e Nevis"@pt ;
                                   rdfs:label "Codes ISO des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   skos:prefLabel "Codes ISO des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:KNParishFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the parishes of Saint Kitts and Nevis" ;
                                   skos:prefLabel "FIPS codes for the parishes of Saint Kitts and Nevis" ;
                                   rdfs:label "FIPS codes for the parishes of Saint Kitts and Nevis"@en ;
                                   skos:prefLabel "FIPS codes for the parishes of Saint Kitts and Nevis"@en ;
                                   rdfs:label "Códigos FIPS de las parroquias de San Cristóbal y Nievesa"@es ;
                                   skos:prefLabel "Códigos FIPS de las parroquias de San Cristóbal y Nieves"@es ;
                                   rdfs:label "Códigos FIPS das paróquias de São Cristóvão e Nevis"@pt ;
                                   skos:prefLabel "Códigos FIPS das paróquias de São Cristóvão e Nevis"@pt ;
                                   rdfs:label "Codes FIPS des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   skos:prefLabel "Codes FIPS des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:KNParishHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Saint Kitts and Nevis" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Saint Kitts and Nevis" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Saint Kitts and Nevis"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Saint Kitts and Nevis"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de San Cristóbal y Nievesa"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de San Cristóbal y Nieves"@es ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) das paróquias de São Cristóvão e Nevis"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) das paróquias de São Cristóvão e Nevis"@pt ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des paroisses de Saint-Christophe-et-Niévès"@fr ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ;
                  skos:prefLabel ?esLanglabel ;
                  skos:prefLabel ?frLanglabel ;
                  skos:prefLabel ?ptLanglabel ;
                  skos:inScheme emergelModules:KNParishList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
  
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:KNParishISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:KNParishFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:KNParishHASCCodeList;
                  emergel:codeOf ?provinceResource .
}
