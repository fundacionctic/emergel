PREFIX project: <http://idi.fundacionctic.org/tabels/project/MZregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/MZregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?ISOCode,?HASCCode,?INEMZCode,?noteen,?notept,?FIPSCode,?en,?sw,?ru,?ptalt,?ipapt] IN horizontal 
    
  LET ?regionResource = resource(concat("MZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionINEMZCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_INEMZ_CODE_",replace(?INEMZCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?ptLabel = setLangTag(?region, "pt")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?swLabel = setLangTag(?sw, "sw")
  LET ?ruLabel = setLangTag(?ru, "ru")
  LET ?ipaptLabel = setLangTag(?ipapt, "pt-MZ-fonipa")
  LET ?ptaltLabel = setLangTag(?ptalt, "pt")
  LET ?enNoteLabel = setLangTag(?noteen, "en")
  LET ?ptNoteLabel = setLangTag(?notept, "pt")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?INEMZPublisher = resource("STANDARDIZATION_ORGANIZATION_INEMZ",emergelModules)
    

CONSTRUCT {
  
  emergelModules:MZProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Mozambique" ;
                              skos:prefLabel "Provinces of Mozambique" ;
                              rdfs:label "Provinces of Mozambique"@en ;
                              skos:prefLabel "Provinces of Mozambique"@en ;
                              rdfs:label "Províncias de Moçambique"@pt ;
                              skos:prefLabel "Províncias de Moçambique"@pt ;
                              rdfs:label "pru ˈvĩ sjɐʒ də mu sɐ̃ ˈbi kɨ"@pt-MZ-fonipa ;
                              skos:prefLabel "pru ˈvĩ sjɐʒ də mu sɐ̃ ˈbi kɨ"@pt-MZ-fonipa ;
                              rdfs:label "Провинций Мозамбика"@ru ;
                              skos:prefLabel "Провинций Мозамбика"@ru ;
                              rdfs:label "Mikoa ya Msumbiji"@sw ;
                              skos:prefLabel "Mikoa ya Msumbiji"@sw .
  
  emergelModules:MZProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Mozambique" ;
                                   skos:prefLabel "ISO codes for the provinces of Mozambique" ;
                                   rdfs:label "ISO codes for the provinces of Mozambique"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Mozambique"@en ;
                                   rdfs:label "Códigos ISO das províncias de Moçambique"@pt ;
                                   skos:prefLabel "Códigos ISO das províncias de Moçambique"@pt ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:MZProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Mozambique" ;
                                   skos:prefLabel "FIPS codes for the provinces of Mozambique" ;
                                   rdfs:label "FIPS codes for the provinces of Mozambique"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Mozambique"@en ;
                                   rdfs:label "Códigos FIPS das províncias de Moçambique"@pt ;
                                   skos:prefLabel "Códigos FIPS das províncias de Moçambique"@pt ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:MZProvinceINEMZCodeList a emergelModules:Country;
                                   rdfs:label "INE (Instituto Nacional de Estatística de Moçambique) codes for the provinces of Angola" ;
                                   skos:prefLabel "INE (Instituto Nacional de Estatística de Moçambique) codes for the provinces of Angola" ;
                                   rdfs:label "INE (Instituto Nacional de Estatística de Moçambique) codes for the provinces of Angola"@en ;
                                   skos:prefLabel "INE (Instituto Nacional de Estatística de Moçambique) codes for the provinces of Angola"@en ;
                                   rdfs:label "Códigos INE (Instituto Nacional de Estatística de Moçambique) das províncias de Angola"@pt ;
                                   skos:prefLabel "Códigos INE (Instituto Nacional de Estatística de Moçambique) das províncias de Angola"@pt ;
                                   rdfs:label "Codes INE (Instituto Nacional de Estatística de Moçambique) des provinces d'Angola"@fr ;
                                   skos:prefLabel "Codes INE (Instituto Nacional de Estatística de Moçambique) des provinces d'Angola"@fr ;
                                   dct:publisher ?INEMZPublisher .

  emergelModules:MZProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Mozambique" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Mozambique" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Mozambique"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Mozambique"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des provinces de Mozambique"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des provinces de Mozambique"@fr ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) das províncias de Moçambique"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) das províncias de Moçambique"@pt ;
                                   dct:publisher ?HASCPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?swLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:prefLabel ?ipaptLabel ;
                  ontolex:phoneticRep ?ipaptLabel ;
                  skos:prefLabel ?ptaltLabel ;
                  skos:note ?ptNoteLabel ;
                  skos:note ?enNoteLabel ;
                  skos:inScheme emergelModules:MZProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionINEMZCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MZProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:MZProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:MZProvinceHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionINEMZCodeResource a skos:Concept;
                  rdfs:label ?INEMZCode ;
                  skos:prefLabel ?INEMZCode ; 
                  skos:inScheme emergelModules:MZProvinceINEMZCodeList;
                  emergel:codeOf ?regionResource .
}
