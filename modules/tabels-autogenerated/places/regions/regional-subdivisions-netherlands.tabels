PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsNLD/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsNLD/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?ISOCode,?nuts1,?nuts2,?dalabel,?nlLabel,?fylabel,?lilabel,?enlabel,?delabel,?frlabel,?ipanl,?ipafy,?ipands,?ipazea,?ennote,?FIPSCode,?lblabel,?zealabel,?pcdlabel,?paplabel,?HASCCode,?ABBRCode,?pop2012,?areakm,?areami,?capital,?ndslabel] IN horizontal 
    
  LET ?regionResource = resource(concat("NL_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("NL_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?nlLangLabel = setLangTag(?nlLabel, "nl")
  LET ?ipanlLangLabel = setLangTag(?ipanl, "nl-NL-fonipa")
  LET ?fyLangLabel = setLangTag(?fylabel, "fy")
  LET ?daLangLabel = setLangTag(?dalabel, "da")
  LET ?liLangLabel = setLangTag(?lilabel, "li")
  LET ?enLangLabel = setLangTag(?enlabel, "en")
  LET ?frLangLabel = setLangTag(?frlabel, "fr")
  LET ?deLangLabel = setLangTag(?delabel, "de")
  LET ?lbLangLabel = setLangTag(?lblabel, "lb")
  LET ?zeaLangLabel = setLangTag(?zealabel, "zea")
  LET ?pcdLangLabel = setLangTag(?pcdlabel, "pcd")
  LET ?ndsLangLabel = setLangTag(?ndslabel, "nds")
  LET ?enLangNote = setLangTag(?ennote, "en")
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  
{
  WHEN not matches(?nuts1,"") DO
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)
;
  WHEN NOT MATCHES (?FIPSCode,"") DO
  LET ?regionFIPSCodeResource = resource(concat("NL_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
;
  WHEN NOT MATCHES (?HASCCode,"") DO
  LET ?regionHASCCodeResource = resource(concat("NL_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
;
  WHEN NOT MATCHES (?ABBRCode,"") DO
  LET ?regionABBRCodeResource = resource(concat("NL_DIVISION_LEVEL_1_ABBR_CODE_",replace(?ABBRCode,"-","_")),emergelModules)
;
    WHEN matches(?ISOCode,"NLD") DO
    /*name of the country in local languages*/
      LET ?zeaCountryLabel = setLangTag("Nederland", "zea")
      LET ?liCountryLabel = setLangTag("Nederland", "li")
      LET ?lbCountryLabel = setLangTag("Holland", "lb")
      LET ?fyCountryLabel = setLangTag("Nederlân", "fy")
      LET ?frrCountryLabel = setLangTag("Nederlönje", "frr")
      LET ?vlsCountryLabel = setLangTag("Holland", "vls")
      LET ?waCountryLabel = setLangTag("Bas Payis", "wa")
      LET ?ndsCountryLabel = setLangTag("Nedderlannen", "nds")
      LET ?papCountryLabel = setLangTag("Hulanda", "pap")
      LET ?pcdCountryLabel = setLangTag("Bas-Païs", "pcd")
      LET ?pcdaltCountryLabel = setLangTag("Bos-Païs", "pcd")
      LET ?pflCountryLabel = setLangTag("Niedalonde", "pfl")
;
    WHEN not matches(?paplabel,"") DO
      LET ?papLangLabel = setLangTag(?paplabel, "pap")
;
    WHEN not matches(?ipafy,"") DO
      LET ?ipafyLangLabel = setLangTag(?ipafy, "fy-NL-fonipa") 
;
    WHEN not matches(?ipands,"") DO
      LET ?ipandsLangLabel = setLangTag(?ipands, "nds-NL-fonipa") 
;
    WHEN not matches(?ipazea,"") DO
      LET ?ipazeaLangLabel = setLangTag(?ipazea, "zea-NL-fonipa")
}

CONSTRUCT {
  
  emergelModules:NLRegionList a emergelModules:Country;
                              rdfs:label "Provinces of the Netherlands" ;
                              skos:prefLabel "Provinces of the Netherlands" ;
                              rdfs:label "Provinces of the Netherlands"@en ;
                              skos:prefLabel "Provinces of the Netherlands"@en ;
                              rdfs:label "Provinzen der Niederlande"@de ;
                              skos:prefLabel "Provinzen der Niederlande"@de ;
                              rdfs:label "Provincies van Nederland"@nl ;
                              skos:prefLabel "Provincies van Nederland"@nl ;
                              rdfs:label "Provinces des Pays-Bas"@fr ;
                              skos:prefLabel "Provinces des Pays-Bas"@fr ;
                              rdfs:label "Provincies vaan Nederland"@li ;
                              skos:prefLabel "Provincies vaan Nederland"@li ;
                              rdfs:label "Provinsjes yn Nederlân"@fy ;
                              skos:prefLabel "Provinsjes yn Nederlân"@fy ;
                              rdfs:label "Nederlandske provinser"@da ;
                              skos:prefLabel "Nederlandske provinser"@da ;
                              rdfs:label "Prouvinches chés Bas-Paÿs"@pcd ;
                              skos:prefLabel "Prouvinches chés Bas-Paÿs"@pcd ;
                              rdfs:label "Hollännesch Provënzen"@lb ;
                              skos:prefLabel "Hollännesch Provënzen"@lb ;
                              rdfs:label "Administratieve indeêlienge in Nederland: Provincies"@zea ;
                              skos:prefLabel "Administratieve indeêlienge in Nederland: Provincies"@zea .
  
  emergelModules:NLRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of the Netherlands" ;
                                   skos:prefLabel "ISO codes for the provinces of the Netherlands" ;
                                   rdfs:label "ISO codes for the provinces of the Netherlands"@en ;
                                   skos:prefLabel "ISO codes for the provinces of the Netherlands"@en ;
                                   rdfs:label "Liste der ISO-3166-2-Codes für Niederlande"@de ;
                                   skos:prefLabel "Liste der ISO-3166-2-Codes für Niederlande"@de ;
                                   rdfs:label "ISO-3166-2-Codes voor Nederland: Provincies"@nl ;
                                   skos:prefLabel "ISO-3166-2-Codes voor Nederland: Provincies"@nl ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:NLRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of the Netherlands" ;
                                   skos:prefLabel "FIPS codes for the provinces of the Netherlands" ;
                                   rdfs:label "FIPS codes for the provinces of the Netherlands"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of the Netherlands"@en ;
                                   rdfs:label "Liste der FIPS-Codes für Niederlande"@de ;
                                   skos:prefLabel "Liste der FIPS-Codes für Niederlande"@de ;
                                   rdfs:label "FIPS-Codes voor Nederland: Provincies"@nl ;
                                   skos:prefLabel "FIPS-Codes voor Nederland: Provincies"@nl ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:NLRegionABBRCodeList a emergelModules:Country;
                                   rdfs:label "Conventional abbreviations used for the provinces of the Netherlands" ;
                                   skos:prefLabel "Conventional abbreviations used for the provinces of the Netherlands" ;
                                   rdfs:label "Conventional abbreviations used for the provinces of the Netherlands"@en ;
                                   skos:prefLabel "Conventional abbreviations used for the provinces of the Netherlands"@en ;
                                   rdfs:label "Herkömmliche Abkürzungen für die Provinzen der Niederlande"@de ;
                                   skos:prefLabel "Herkömmliche Abkürzungen für die Provinzen der Niederlande"@de ;
                                   rdfs:label "Conventionele afkortingen gebruikt voor de provincies van Nederland"@nl ;
                                   skos:prefLabel "Conventionele afkortingen gebruikt voor de provincies van Nederland"@nl .
  
  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NLRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of the Netherlands"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of the Netherlands"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des provinces des Pays Bas"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des provinces des Pays Bas"@fr ;
                                   rdfs:label "HASC-Codes (Statistics Belgium) voor Nederland: Provincies"@nl ;
                                   skos:prefLabel "HASC-Codes (Statistics Belgium) voor Nederland: Provincies"@nl ;
                                   rdfs:label "Liste der HASC-Codes (Hierarchical administrative subdivision codes) für Niederlande: Provinzen"@de ;
                                   skos:prefLabel "Liste der HASC-Codes (Hierarchical administrative subdivision codes) für Niederlande: Provinzen"@de ;
                                   dct:publisher ?HASCPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?nlLangLabel ;
                  skos:prefLabel ?fyLangLabel ;
                  skos:prefLabel ?daLangLabel ;
                  skos:prefLabel ?liLangLabel ;
                  skos:prefLabel ?lbLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?deLangLabel ;
                  skos:prefLabel ?frLangLabel ;
                  skos:prefLabel ?zeaLangLabel ;
                  skos:prefLabel ?pcdLangLabel ;
                  skos:prefLabel ?ndsLangLabel ;
                  skos:prefLabel ?ipanlLangLabel ;
                  skos:note ?enLangNote ;
                  skos:inScheme emergelModules:NLRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:NLRegionISOCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource emergel:hasCode ?nuts2Res .
  
  ?nuts2Res a skos:Concept;
           skos:inScheme emergelModules:NUTS2CodeList ;
           emergel:codeOf ?regionResource ;
           skos:broader ?nuts1Res .

  ?nuts1Res a skos:Concept;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nuts2Res .
}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionFIPSCodeResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:NLRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionHASCCodeResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:NLRegionHASCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {

  ?regionResource emergel:hasCode ?regionABBRCodeResource .

  ?regionABBRCodeResource a skos:Concept;
                  rdfs:label ?ABBRCode ;
                  skos:prefLabel ?ABBRCode ; 
                  skos:inScheme emergelModules:NLRegionABBRCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?ISOResource skos:prefLabel ?zeaCountryLabel ;
               skos:prefLabel ?liCountryLabel ;
               skos:prefLabel ?lbCountryLabel ;
               skos:prefLabel ?fyCountryLabel ;
               skos:prefLabel ?frrCountryLabel ;
               skos:prefLabel ?vlsCountryLabel ;
               skos:prefLabel ?waCountryLabel ;
               skos:prefLabel ?papCountryLabel ;
               skos:prefLabel ?pflCountryLabel ;
               skos:prefLabel ?pcdCountryLabel ;
               skos:altLabel ?pcdaltCountryLabel ;
               skos:prefLabel ?ndsCountryLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?papLangLabel .
 
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?ipafyLangLabel ;
                  ontolex:phoneticRep ?ipafyLangLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?ipandsLangLabel ;
                  ontolex:phoneticRep ?ipandsLangLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?ipazeaLangLabel ;
                  ontolex:phoneticRep ?ipazeaLangLabel .
}