PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsBS/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsBS/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?region,?eslabel,?esalt,?noteen,?notees] IN horizontal 
    
  LET ?regionResource = resource(concat("BS_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("BS_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("BS_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "en")
  LET ?esLanglabel = setLangTag(?eslabel, "es")
  LET ?esaltLanglabel = setLangTag(?esalt, "es")

  LET ?noteenLanglabel = setLangTag(?noteen, "en")
  LET ?noteesLanglabel = setLangTag(?notees, "es")

CONSTRUCT {
  
  emergelModules:BSDistrictList a emergelModules:Country;
                              rdfs:label "Districts of Bahamas" ;
                              skos:prefLabel "Districts of Bahamas" ;
                              rdfs:label "Districts of Bahamas"@en ;
                              skos:prefLabel "Districts of Bahamas"@en ;
                              rdfs:label "Distritos de Bahamas"@es ;
                              skos:prefLabel "Distritos de Bahamas"@es .
  
  emergelModules:BSDistrictISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of Bahamas" ;
                                   skos:prefLabel "ISO codes for the districts of Bahamas" ;
                                   rdfs:label "ISO codes for the districts of Bahamas"@en ;
                                   skos:prefLabel "ISO codes for the districts of Bahamas"@en ;
                                   rdfs:label "Códigos ISO de los distritos de Bahamas"@es ;
                                   skos:prefLabel "Códigos ISO de los distritos de Bahamas"@es .

  emergelModules:BSDistrictFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts of Bahamas" ;
                                   skos:prefLabel "FIPS codes for the districts of Bahamas" ;
                                   rdfs:label "FIPS codes for the districts of Bahamas"@en ;
                                   skos:prefLabel "FIPS codes for the districts of Bahamas"@en ;
                                   rdfs:label "Códigos FIPS de los distritos de Bahamas"@es ;
                                   skos:prefLabel "Códigos FIPS de los distritos de Bahamas"@es .

}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?esLanglabel ;
                  skos:altLabel ?esaltLanglabel ;
                  skos:note ?noteenLanglabel ;
                  skos:note ?noteesLanglabel ;
                  skos:inScheme emergelModules:BSDistrictList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:BSDistrictISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BSDistrictFIPSCodeList;
                  emergel:codeOf ?regionResource .
}
