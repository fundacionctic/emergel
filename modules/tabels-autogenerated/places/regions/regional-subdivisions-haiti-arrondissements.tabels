PREFIX project: <http://idi.fundacionctic.org/tabels/project/HTarrondissements/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/HTarrondissements/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?province,?PCCode,?pop2003,?areakm2,?htlabel,?enlabel,?ISOCode,?LOCode] IN horizontal 
    
  LET ?provinceResource = resource(concat("HT_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("HT_DIVISION_LEVEL_2_HASC_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?PCCodeResource = resource(concat("HT_DIVISION_LEVEL_2_POSTAL_CODE_",?PCCode),emergelModules)
  LET ?regionResource = resource(concat("HT_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?frLabel = setLangTag(?province, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?htLabel = setLangTag(?htlabel, "ht")

  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?UNPublisher = resource("STANDARDIZATION_ORGANIZATION_UN",emergelModules)

  WHEN not matches(?LOCode,"") DO
  LET ?provinceLOCodeResource = resource(concat("HT_PLACE_LOCODE_",replace(?LOCode," ","_")),emergelModules)
    

CONSTRUCT {
  
  emergelModules:HTArrondissementList a emergelModules:Country;
                              rdfs:label "Arrondissements of Haiti"@en ;
                              skos:prefLabel "Arrondissements of Haiti"@en ;
                              rdfs:label "Distritos de Haiti"@es ;
                              skos:prefLabel "Distritos de Haiti"@es ;
                              rdfs:label "Arrondissements d'Haïti"@fr ;
                              skos:prefLabel "Arrondissements d'Haïti"@fr ;
                              rdfs:label "Awondisman nan Ayiti"@ht ;
                              skos:prefLabel "Awondisman nan Ayiti"@ht ;
                              rdfs:label "Distritos de Haiti"@pt ;
                              skos:prefLabel "Distritos de Haiti"@pt .
  
  emergelModules:HTArrondissementHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the arrondissements of Haiti" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the arrondissements of Haiti" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the arrondissements of Haiti"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the arrondissements of Haiti"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los distritos de Haiti"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los distritos de Haiti"@es ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos de Haiti"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos de Haiti"@pt ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des arrondissements d'Haïti"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des arrondissements d'Haïti"@fr ;
                                   rdfs:label "Kód HASC (Hierarchical administrative subdivision codes) - Awondisman nan Ayiti"@ht ;
                                   skos:prefLabel "Kód HASC (Hierarchical administrative subdivision codes) - Awondisman nan Ayiti"@ht ;
                                   dct:publisher ?HASCPublisher .
  
  emergelModules:HTArrondissementPCCodeList a emergelModules:Country;
                                   rdfs:label "Postal codes for the arrondissements of Haiti"@en ;
                                   skos:prefLabel "Postal codes for the arrondissements of Haiti"@en ;
                                   rdfs:label "Códigos postales de los distritos de Haiti"@es ;
                                   skos:prefLabel "Códigos postales de los distritos de Haiti"@es ;
                                   rdfs:label "Códigos postais dos distritos de Haiti"@pt ;
                                   skos:prefLabel "Códigos postais dos distritos de Haiti"@pt ;
                                   rdfs:label "Codes postaux des arrondissements d'Haïti"@fr ;
                                   skos:prefLabel "Codes postaux des arrondissements d'Haïti"@fr ;
                                   rdfs:label "Kòd Sèvis - Awondisman nan Ayiti"@ht ;
                                   skos:prefLabel "Kòd Sèvis - Awondisman nan Ayiti"@ht .

    emergelModules:HTPlacesLOCodeList a emergelModules:Country;
                                   rdfs:label "UN/LOCODEs for arrondissements in Haiti" ;
                                   skos:prefLabel "UN/LOCODEs for arrondissements in Haiti" ;
                                   rdfs:label "UN/LOCODEs for arrondissements in Haiti"@en ;
                                   skos:prefLabel "UN/LOCODEs for arrondissements in Haiti"@en ;
                                   rdfs:label "Codis UN/LOCODEs per districtes a Haiti"@ca ;
                                   skos:prefLabel "Codis UN/LOCODEs per districtes a Haiti"@ca ;
                                   rdfs:label "Códigos UN/LOCODEs para distritos de Haiti"@es ;
                                   skos:prefLabel "Códigos UN/LOCODEs para distritos de Haiti"@es ;
                                   rdfs:label "Kód UN/LOCODE: Awondisman nan Ayiti"@ht ;
                                   skos:prefLabel "Kód UN/LOCODE: Awondisman nan Ayiti"@ht ;
                                   rdfs:label "UN/LOCODEs des arrondissements d'Haïti"@fr ;
                                   skos:prefLabel "UN/LOCODEs des arrondissements d'Haïti"@fr ;
                                   dct:publisher ?UNPublisher .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?htLabel ;
                  skos:inScheme emergelModules:HTArrondissementList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?PCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?ISOResource .
  
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:HTArrondissementHASCCodeList;
                  emergel:codeOf ?provinceResource .
  
  ?PCCodeResource a skos:Concept;
                  rdfs:label ?PCCode ;
                  skos:prefLabel ?PCCode ; 
                  skos:inScheme emergelModules:HTArrondissementPCCodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {

  ?provinceLOCodeResource a skos:Concept;
                  rdfs:label ?LOCode ;
                  skos:prefLabel ?LOCode ; 
                  skos:inScheme emergelModules:HTPlacesLOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceResource emergel:hasCode ?provinceLOCodeResource . 

}