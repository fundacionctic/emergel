PREFIX project: <http://idi.fundacionctic.org/tabels/project/BJregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/BJregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?region,?frlabel,?fraltlabel,?pop2010,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("BJ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("BJ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("BJ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("BJ_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?enLabel = setLangTag(?region, "en")
  LET ?frLabel = setLangTag(?frlabel, "fr")
  LET ?fraltLangLabel = setLangTag(?fraltlabel, "fr")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)

CONSTRUCT {
  
  emergelModules:BJProvinceList a emergelModules:Country;
                              rdfs:label "Departments of Benin" ;
                              skos:prefLabel "Departments of Benin" ;
                              rdfs:label "Departments of Benin"@en ;
                              skos:prefLabel "Departments of Benin"@en ;
                              rdfs:label "Départements du Bénin"@fr ;
                              skos:prefLabel "Départements du Bénin"@fr .
  
  emergelModules:BJProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the departments of Benin"@en ;
                                   skos:prefLabel "ISO codes for the departments of Benin"@en ;
                                   rdfs:label "ISO codes for the departments of Benin" ;
                                   skos:prefLabel "ISO codes for the departments of Benin" ;
                                   rdfs:label "Codes ISO des départements du Bénin"@fr ;
                                   skos:prefLabel "Codes ISO des départements du Bénin"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:BJProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the departments of Benin"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Benin"@en ;
                                   rdfs:label "FIPS codes for the departments of Benin" ;
                                   skos:prefLabel "FIPS codes for the departments of Benin" ;
                                   rdfs:label "Codes FIPS des départements du Bénin"@fr ;
                                   skos:prefLabel "Codes FIPS des départements du Bénin"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:BJProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Benin" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Benin" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Benin"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Benin"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des départements du Bénin"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des départements du Bénin"@fr ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:altLabel ?fraltLangLabel ;
                  skos:inScheme emergelModules:BJProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:BJProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BJProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:BJProvinceHASCCodeList;
                  emergel:codeOf ?regionResource .
}