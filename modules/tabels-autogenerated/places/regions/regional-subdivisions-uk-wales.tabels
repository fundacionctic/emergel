PREFIX project: <http://idi.fundacionctic.org/tabels/project/2nd-levelGB-WLS/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/2nd-levelGB-WLS/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
  MATCH [?provinceCode,?ONSCode,?GSSCode,?nuts1,?nuts2,?nuts3,?PCAREACode,?FIPSCode,?HASCCode,?BSIALTCode,?province,?cyalt,?en,?enalt,?ipaen,?ipaga,?ipaenalt,?note_en,?regionCode,?ISOCode,?cor,?kw,?FRS1,?FRS2,?FRS3,?PF1,?PF2,?PF3,?PF4] IN horizontal 

  LET ?provinceResource = resource(concat("GB_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("GB_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceONSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_ONS_CODE_",replace(?ONSCode,"-","_")),emergelModules)
  LET ?provinceGSSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_GSS_CODE_",replace(?GSSCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("GB_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("GB_DIVISION_LEVEL_2_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?provincePCAREACodeResource = resource(concat("GB_POSTCODE_AREA_",replace(?PCAREACode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("GB_DIVISION_LEVEL_1_GB_",replace(?regionCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
    LET ?cyLangLabel = setLangTag(?province, "cy")
    LET ?enLangLabel = setLangTag(?en, "en")
    LET ?note_enLang = setLangTag(?note_en, "en")
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)

{
WHEN not matches(?nuts3,"") DO
LET ?nuts3Res = resource(?nuts3, nuts)
  ;
WHEN not matches (?BSIALTCode,"") DO
LET ?provinceBSIALTCodeResource = resource(concat("GB_DIVISION_LEVEL_2_BSI_ALTCODE_",replace(?BSIALTCode,"-","_")),emergelModules)
  ;
WHEN not matches(?enalt,"") DO
LET ?enaltLangLabel = setLangTag(?enalt, "en")
    ;
WHEN not matches(?cyalt,"") DO
LET ?cyaltLangLabel = setLangTag(?cyalt, "cy")
  ;
WHEN not matches(?cor,"") DO
LET ?corLangLabel = setLangTag(?cor, "cor")
  ;
WHEN not matches(?kw,"") DO
LET ?kwLangLabel = setLangTag(?kw, "kw")
 ;
WHEN matches(?FRS1, "yes") DO
LET ?FRS1Schema = ?provinceResource
;
WHEN matches(?FRS2, "yes") DO
LET ?FRS2Schema = ?provinceResource
;
WHEN matches(?FRS3, "yes") DO
LET ?FRS3Schema = ?provinceResource
 ;
WHEN matches(?PF1, "yes") DO
LET ?PF1Schema = ?provinceResource
;
WHEN matches(?PF2, "yes") DO
LET ?PF2Schema = ?provinceResource
;
WHEN matches(?PF3, "yes") DO
LET ?PF3Schema = ?provinceResource
;
WHEN matches(?PF4, "yes") DO
LET ?PF4Schema = ?provinceResource
  ;
WHEN not matches(?ipaen,"") DO
LET ?ipaenLangLabel = setLangTag(?ipaen, "en-GB-fonipa")
  ;
WHEN not matches(?ipaga,"") DO
LET ?ipagaLangLabel = setLangTag(?ipaga, "ga-GB-fonipa")
  ;
WHEN not matches(?ipaenalt,"") DO
LET ?ipaenaltLangLabel = setLangTag(?ipaenalt, "en-GB-fonipa")
}


CONSTRUCT {
  
  emergelModules:GB_WLSProvinceList a emergelModules:Country;
                              rdfs:label "Unitary authorities of Wales"@en ;
                              skos:prefLabel "Unitary authorities of Wales"@en ;
                              rdfs:label "Awdurdodau unedol yng Nghymru"@cy ;
                              skos:prefLabel "Awdurdodau unedol yng Nghymru"@cy .
  
  emergelModules:GB_WLSProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "ISO codes for the unitary authorities in Wales"@en .
  
  emergelModules:GB_WLSProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "FIPS codes for the unitary authorities in Wales"@en .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .

  emergelModules:GB_WLSProvinceONSCodeList a emergelModules:Country;
                                   rdfs:label "ONS (United Kingdom's Office for National Statistics) codes for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "ONS (United Kingdom's Office for National Statistics) codes for the unitary authorities in Wales"@en .

  emergelModules:GB_WLSProvinceGSSCodeList a emergelModules:Country;
                                   rdfs:label "GSS (United Kingdom's Government Statistical Service) codes for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "GSS (United Kingdom's Government Statistical Service) codes for the unitary authorities in Wales"@en .

  emergelModules:GB_WLSProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the unitary authorities in Wales" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the unitary authorities in Wales" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the unitary authorities in Wales"@en .

  emergelModules:GBRegionPCAREACodeList a emergelModules:Country;
                                   rdfs:label "Postcode areas of the United Kingdom"@en ;
                                   skos:prefLabel "Postcode areas of the United Kingdom"@en .

  emergelModules:GB_WLSProvinceBSIALTCodeList a emergelModules:Country;
                                   rdfs:label "BSI alternative codes for the unitary authorities in Wales"@en ;
                                   skos:prefLabel "BSI alternative codes for the unitary authorities in Wales"@en .
  
  emergelModules:GB_WLSFRSaRegionList a emergelModules:Country;
                              rdfs:label "North Wales Fire and Rescue Service" ;
                              skos:prefLabel "North Wales Fire and Rescue Service" ;
                              rdfs:label "North Wales Fire and Rescue Service"@en ;
                              skos:prefLabel "North Wales Fire and Rescue Service"@en ;
                              rdfs:label "Gwasanaeth Tân ac Achub Gogledd Cymru"@cy ;
                              skos:prefLabel "Gwasanaeth Tân ac Achub Gogledd Cymru"@cy .
  
  emergelModules:GB_WLSFRSbRegionList a emergelModules:Country;
                              rdfs:label "Mid and West Wales Fire and Rescue Service" ;
                              skos:prefLabel "Mid and West Wales Fire and Rescue Service" ;
                              rdfs:label "Mid and West Wales Fire and Rescue Service"@en ;
                              skos:prefLabel "Mid and West Wales Fire and Rescue Service"@en ;
                              rdfs:label "Gwasanaeth Tân ac Achub Canolbarth a Gorllewin Cymru"@cy ;
                              skos:prefLabel "Gwasanaeth Tân ac Achub Canolbarth a Gorllewin Cymru"@cy .
  
  emergelModules:GB_WLSFRScRegionList a emergelModules:Country;
                              rdfs:label "South Wales Fire and Rescue Service" ;
                              skos:prefLabel "South Wales Fire and Rescue Service" ;
                              rdfs:label "South Wales Fire and Rescue Service"@en ;
                              skos:prefLabel "South Wales Fire and Rescue Service"@en ;
                              rdfs:label "Gwasanaeth Tân ac Achub De Cymru"@cy ;
                              skos:prefLabel "Gwasanaeth Tân ac Achub De Cymru"@cy .
  
  emergelModules:GB_WLSPFaRegionList a emergelModules:Country;
                              rdfs:label "North Wales Police Area" ;
                              skos:prefLabel "North Wales Police Area" ;
                              rdfs:label "North Wales Police Area"@en ;
                              skos:prefLabel "North Wales Police Area"@en ;
                              rdfs:label "Heddlu Gogledd Cymru"@cy ;
                              skos:prefLabel "Heddlu Gogledd Cymru"@cy .
  
  emergelModules:GB_WLSPFbRegionList a emergelModules:Country;
                              rdfs:label "Dyfed-Powys Police Area" ;
                              skos:prefLabel "Dyfed-Powys Police Area" ;
                              rdfs:label "Dyfed-Powys Police Area"@en ;
                              skos:prefLabel "Dyfed-Powys Police Area"@en ;
                              rdfs:label "Heddlu Dyfed-Powys"@cy ;
                              skos:prefLabel "Heddlu Dyfed-Powys"@cy .
  
  emergelModules:GB_WLSPFcRegionList a emergelModules:Country;
                              rdfs:label "South Wales Police Area" ;
                              skos:prefLabel "South Wales Police Area" ;
                              rdfs:label "South Wales Police Area"@en ;
                              skos:prefLabel "South Wales Police Area"@en ;
                              rdfs:label "Heddlu De Cymru"@cy ;
                              skos:prefLabel "Heddlu De Cymru"@cy .
  
  emergelModules:GB_WLSPFdRegionList a emergelModules:Country;
                              rdfs:label "Gwent Police Area" ;
                              skos:prefLabel "Gwent Police Area" ;
                              rdfs:label "Gwent Police Area"@en ;
                              skos:prefLabel "Gwent Police Area"@en ;
                              rdfs:label "Heddlu Gwent"@cy ;
                              skos:prefLabel "Heddlu Gwent"@cy .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?cyLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:note ?note_enLang ;
                  skos:inScheme emergelModules:GB_WLSProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceONSCodeResource ;
                  emergel:hasCode ?provinceGSSCodeResource ;
                  emergel:hasCode ?provincePCAREACodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .

  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceGSSCodeResource a skos:Concept;
                  rdfs:label ?GSSCode ;
                  skos:prefLabel ?GSSCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceGSSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .

  ?provincePCAREACodeResource a skos:Concept;
                  rdfs:label ?PCAREACode ;
                  skos:prefLabel ?PCAREACode ;
                  skos:inScheme emergelModules:GBRegionPCAREACodeList;
                  emergel:codeOf ?provinceResource .


  ?provinceONSCodeResource a skos:Concept;
                  rdfs:label ?ONSCode ;
                  skos:prefLabel ?ONSCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceONSCodeList;
                  emergel:codeOf ?provinceResource .

}

CONSTRUCT {
  ?regionResource skos:narrower ?provinceResource .
}

CONSTRUCT {
  ?nuts3Res a skos:Concept;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts3Res ;
           emergel:codeOf ?provinceResource .

  ?provinceResource emergel:hasCode ?nuts3Res .
}

CONSTRUCT {
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts2Res ;
           skos:inScheme emergelModules:NUTS1CodeList .
 } 

 CONSTRUCT {

  ?provinceBSIALTCodeResource a skos:Concept;
                  rdfs:label ?BSIALTCode ;
                  skos:prefLabel ?BSIALTCode ; 
                  skos:inScheme emergelModules:GB_WLSProvinceBSIALTCodeList;
                  emergel:codeOf ?provinceResource .
   
  ?provinceResource emergel:hasCode ?provinceBSIALTCodeResource .
}
 
CONSTRUCT {
  ?provinceResource skos:altLabel ?enaltLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?cyaltLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?corLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?kwLangLabel .
}

CONSTRUCT {
  
  ?FRS1Schema skos:inScheme emergelModules:GB_WLSFRSaRegionList .
}

CONSTRUCT {
  
  ?FRS2Schema skos:inScheme emergelModules:GB_WLSFRSbRegionList .
}

CONSTRUCT {
  
  ?FRS3Schema skos:inScheme emergelModules:GB_WLSFRScRegionList .
}

CONSTRUCT {
  
  ?PF1Schema skos:inScheme emergelModules:GB_WLSPFaRegionList .
}

CONSTRUCT {
  
  ?PF2Schema skos:inScheme emergelModules:GB_WLSPFbRegionList .
}

CONSTRUCT {
  
  ?PF3Schema skos:inScheme emergelModules:GB_WLSPFcRegionList .
}

CONSTRUCT {
  
  ?PF4Schema skos:inScheme emergelModules:GB_WLSPFdRegionList .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?ipaenLangLabel ;
                    ontolex:phoneticRep ?ipaenLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?ipagaLangLabel ;
                    ontolex:phoneticRep ?ipagaLangLabel .
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?ipaenaltLangLabel ;
                    ontolex:phoneticRep ?ipaenaltLangLabel .
}