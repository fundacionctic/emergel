PREFIX project: <http://idi.fundacionctic.org/tabels/project/municipalitiesES/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/municipalitiesES/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?provinceCode,?INEProvinceCode,?lau2,?INEMunicipalityCode,?HASCCode,?LOCode,?esLabel,?enLabel,?astLabel,?caLabel,?glLabel,?euLabel,?ocLabel,?areakm2,?pop2001,?pop2011,?anLabel,?extLabel] IN horizontal 
    
  LET ?provinceResource = resource(concat("ES_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?municipalityResource = resource(concat("ES_DIVISION_LEVEL_3_",replace(?INEMunicipalityCode,"-","_")),emergelModules)
  LET ?municipalityINECodeResource = resource(concat("ES_DIVISION_LEVEL_3_INE_CODE_",replace(?INEMunicipalityCode,"-","_")),emergelModules)
  LET ?municipalityLAU2CodeResource = resource(concat("ES_DIVISION_LEVEL_3_LAU2_CODE_",replace(?lau2,"-","_")),emergelModules)
  LET ?INEPublisher = resource("STANDARDIZATION_ORGANIZATION_INE",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?UNPublisher = resource("STANDARDIZATION_ORGANIZATION_UN",emergelModules)
  
  LET ?esLangLabel = setLangTag(?esLabel, "es")
  LET ?enLangLabel = setLangTag(?enLabel, "en")

{
  WHEN not matches(?HASCCode,"") DO
  LET ?municipalityHASCCodeResource = resource(concat("ES_DIVISION_LEVEL_3_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  ;
  WHEN not matches(?LOCode,"") DO
  LET ?municipalityLOCodeResource = resource(concat("ES_PLACE_LOCODE_",replace(?LOCode," ","_")),emergelModules)
  ;
  WHEN not matches(?astLabel,"") DO
  LET ?astLangLabel = setLangTag(?astLabel,"ast")
  ;
  WHEN not matches(?caLabel,"") DO
  LET ?caLangLabel = setLangTag(?caLabel,"ca")
  ;
  WHEN not matches(?glLabel,"") DO
  LET ?glLangLabel = setLangTag(?glLabel,"gl")
  ;
  WHEN not matches(?ocLabel,"") DO
  LET ?ocLangLabel = setLangTag(?ocLabel,"oc")
  ;
  WHEN not matches(?euLabel,"") DO
  LET ?euLangLabel = setLangTag(?euLabel,"eu")
  ;
  WHEN not matches(?anLabel,"") DO
  LET ?anLangLabel = setLangTag(?anLabel,"an")
  ;
  WHEN not matches(?extLabel,"") DO
  LET ?extLangLabel = setLangTag(?extLabel,"ext")
}

  /*  LET ?caLangLabel = setLangTag(?calabel, "ca")
    LET ?euLangLabel = setLangTag(?eulabel, "eu")
    LET ?glLangLabel = setLangTag(?gllabel, "gl")
    LET ?anLangLabel = setLangTag(?anlabel, "an")
    LET ?ocLangLabel = setLangTag(?oclabel, "oc")
    LET ?ptLangLabel = setLangTag(?ptlabel, "pt")
    LET ?note_enLang = setLangTag(?note_en, "en") */    

CONSTRUCT {
  
  emergelModules:ESMunicipalityList a emergelModules:Country;
                              rdfs:label "Municipalities of Spain" ;
                              skos:prefLabel "Municipalities of Spain" ;
                              rdfs:label "Municipalities of Spain"@en ;
                              skos:prefLabel "Municipalities of Spain"@en ;
                              rdfs:label "Municipios de España"@es ;
                              skos:prefLabel "Municipios de España"@es ;
                              rdfs:label "Municipios d'España"@ast ;
                              skos:prefLabel "Municipios d'España"@ast ;
                              rdfs:label "Concellos de España"@gl ;
                              skos:prefLabel "Concellos de España"@gl ;
                              rdfs:label "Municipis d'Espanya"@ca ;
                              skos:prefLabel "Municipis d'Espanya"@ca ;
                              rdfs:label "Monicípius d'España"@ext ;
                              skos:prefLabel "Monicípius d'España"@ext .

  
  emergelModules:ESMunicipalityINECodeList a emergelModules:Country;
                                   rdfs:label "INE codes for the municipalities of Spain" ;
                                   skos:prefLabel "INE codes for the municipalities of Spain" ;
                                   rdfs:label "INE codes for the municipalities of Spain"@en ;
                                   skos:prefLabel "INE codes for the municipalities of Spain"@en ;
                                   rdfs:label "Códigos INE de los municipios d'España"@ast ;
                                   skos:prefLabel "Códigos INE de los municipios d'España"@ast ;
                                   rdfs:label "Códigos INE de los municipios de España"@es ;
                                   skos:prefLabel "Códigos INE de los municipios de España"@es ;
                                   rdfs:label "Codis INE dels municipis d'Espanya'"@ca ;
                                   skos:prefLabel "Codis INE dels municipis d'Espanya"@ca ;
                                   dct:publisher ?INEPublisher .

  emergelModules:ESMunicipalityHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Spain" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Spain" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Spain"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Spain"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios de España"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios de España"@es ;
                                   rdfs:label "Codis HASC (Hierarchical administrative subdivision codes) dels municipis d'Espanya"@ca ;
                                   skos:prefLabel "Codis HASC (Hierarchical administrative subdivision codes) dels municipis d'Espanya"@ca ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios d'España"@ast ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios d'España"@ast ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos municípios de Espanha"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos municípios de Espanha"@pt ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos municipios de España"@gl ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos municipios de España"@gl ;
                                   dct:publisher ?HASCPublisher .

    emergelModules:ESPlacesLOCodeList a emergelModules:Country;
                                   rdfs:label "UN/LOCODEs for towns and municipalities of Spain" ;
                                   skos:prefLabel "UN/LOCODEs for towns and municipalities of Spain" ;
                                   rdfs:label "UN/LOCODEs for towns and municipalities of Spain"@en ;
                                   skos:prefLabel "UN/LOCODEs for towns and municipalities of Spain"@en ;
                                   rdfs:label "Codis UN/LOCODEs per localitats i municipis d'Espanya"@ca ;
                                   skos:prefLabel "Codis UN/LOCODEs per localitats i municipis d'Espanya"@ca ;
                                   rdfs:label "Códigos UN/LOCODEs para localidades y municipios de España"@es ;
                                   skos:prefLabel "Códigos UN/LOCODEs para localidades y municipios de España"@es ;
                                   rdfs:label "Códigos UN/LOCODEs para localidades e concellos de España"@gl ;
                                   skos:prefLabel "Códigos UN/LOCODEs para localidades e concellos de España"@gl ;
                                   rdfs:label "Códigos UN/LOCODEs pa llugares y conceyos d'España"@ast ;
                                   skos:prefLabel "Códigos UN/LOCODEs pa llugares y conceyos d'España"@ast ;
                                   dct:publisher ?UNPublisher .

   emergelModules:ESMunicipalityLAU2CodeList a emergelModules:Country;
                                   rdfs:label "LAU-2 codes for the municipalities of Spain" ;
                                   skos:prefLabel "LAU-2 codes for the municipalities of Spain" ;
                                   rdfs:label "LAU-2 codes for the municipalities of Spain"@en ;
                                   skos:prefLabel "LAU-2 codes for the municipalities of Spain"@en ;
                                   rdfs:label "Códigos LAU-2 de los conceyos d'España"@ast ;
                                   skos:prefLabel "Códigos LAU-2 de los conceyos d'España"@ast ;
                                   rdfs:label "Códigos LAU-2 de los municipios de España"@es ;
                                   skos:prefLabel "Códigos LAU-2 de los municipios de España"@es ;
                                   rdfs:label "Codis LAU-2 dels municipis d'Espanya"@ca ;
                                   skos:prefLabel "Codis LAU-2 dels municipis d'Espanya"@ca ;
                                   rdfs:label "Códigos LAU-2 dos concellos de España"@gl ;
                                   skos:prefLabel "Códigos LAU-2 dos concellos de España"@gl .

}

CONSTRUCT {
  ?municipalityResource a skos:Concept;
                  rdfs:label ?enLabel ;
                  skos:prefLabel ?esLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:inScheme emergelModules:ESMunicipalityList;
                  emergel:hasCode ?municipalityINECodeResource ;
                  skos:notation ?lau2 ;
                  emergel:hasCode ?municipalityLAU2CodeResource ;
                  skos:broader ?provinceResource .

  
  ?provinceResource skos:narrower ?municipalityResource .
  
  ?municipalityINECodeResource a skos:Concept;
                  rdfs:label ?INEMunicipalityCode ;
                  skos:prefLabel ?INEMunicipalityCode ; 
                  skos:inScheme emergelModules:ESMunicipalityINECodeList;
                  emergel:codeOf ?municipalityResource .

  ?municipalityLAU2CodeResource a skos:Concept;
                  rdfs:label ?lau2 ;
                  skos:prefLabel ?lau2 ;
                  skos:inScheme emergelModules:ESMunicipalityLAU2CodeList;
                  emergel:codeOf ?municipalityResource .
}

CONSTRUCT {

  ?municipalityHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:ESMunicipalityHASCCodeList;
                  emergel:codeOf ?municipalityResource .

  ?municipalityResource emergel:hasCode ?municipalityHASCCodeResource . 

}

CONSTRUCT {

  ?municipalityLOCodeResource a skos:Concept;
                  rdfs:label ?LOCode ;
                  skos:prefLabel ?LOCode ; 
                  skos:inScheme emergelModules:ESPlacesLOCodeList;
                  emergel:codeOf ?municipalityResource .

  ?municipalityResource emergel:hasCode ?municipalityLOCodeResource . 

}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?astLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?caLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?glLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?ocLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?euLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?anLangLabel .
 
}

CONSTRUCT {
  ?municipalityResource skos:prefLabel ?extLangLabel .
 
}