PREFIX project: <http://idi.fundacionctic.org/tabels/project/GLregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/GLregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?ISOCode,?da,?note_da,?note_en,?en,?fo,?FIPSCode] IN horizontal 
    
  LET ?regionResource = resource(concat("GL_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("GL_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("GL_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
          /*name of the country in local languages*/
      LET ?iuCountryLabel = setLangTag("ᐊᑯᑭᑦᑐᑦ", "iu")

  LET ?klLabel = setLangTag(?region, "kl")
  LET ?daLabel = setLangTag(?da, "da")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?foLabel = setLangTag(?fo, "fo")
  LET ?note_enLangLabel = setLangTag(?note_en, "en")
  
  WHEN not matches(?note_da,"") DO
    LET ?note_daLangLabel = setLangTag(?note_da, "da")

CONSTRUCT {
  
  emergelModules:GLMunicipalitiesList a emergelModules:Country;
                              rdfs:label "Municipalities of Greenland"@en ;
                              skos:prefLabel "Municipalities of Greenland"@en ;
                              rdfs:label "Kalaallit Nunaanni kommunit"@kl ;
                              skos:prefLabel "Kalaallit Nunaanni kommunit"@kl ;
                              rdfs:label "Kommuner i Grønland"@da ;
                              skos:prefLabel "Kommuner i Grønland"@da ;
                              rdfs:label "Kommunur í Grønlandi"@fo ;
                              skos:prefLabel "Kommunur í Grønlandi"@fo .
  
  emergelModules:GLMunicipalitiesISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the municipalities of Greenland" ;
                                   skos:prefLabel "ISO codes for the municipalities of Greenland" ;
                                   rdfs:label "ISO codes for the municipalities of Greenland"@en ;
                                   skos:prefLabel "ISO codes for the municipalities of Greenland"@en .

  emergelModules:GLMunicipalitiesFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the municipalities of Greenland" ;
                                   skos:prefLabel "FIPS codes for the municipalities of Greenland" ;
                                   rdfs:label "FIPS codes for the municipalities of Greenland"@en ;
                                   skos:prefLabel "FIPS codes for the municipalities of Greenland"@en .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?klLabel ;
                  skos:prefLabel ?daLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?foLabel ;
                  skos:note ?note_enLangLabel ;
                  skos:inScheme emergelModules:GLMunicipalitiesList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?iuCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:GLMunicipalitiesISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GLMunicipalitiesFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:note ?note_daLangLabel .
}