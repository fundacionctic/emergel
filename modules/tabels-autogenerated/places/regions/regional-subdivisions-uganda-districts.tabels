PREFIX project: <http://idi.fundacionctic.org/tabels/project/UGdistricts/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/UGdistricts/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?HASCCode,?FIPSCode,?province,?frlabel,?ptlabel,?swlabel,?lglabel,?pop2014,?pop2002,?areakm2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("UG_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?provinceResource = resource(concat("UG_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("UG_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("UG_DIVISION_LEVEL_2_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("UG_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)

  LET ?enLabel = setLangTag(?province, "en")
  LET ?frLabel = setLangTag(?frlabel, "fr")
  LET ?ptLabel = setLangTag(?ptlabel, "pt")
  LET ?swLabel = setLangTag(?swlabel, "sw")
  LET ?lgLabel = setLangTag(?lglabel, "lg")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

CONSTRUCT {
  
  emergelModules:UGProvinceList a emergelModules:Country;
                              rdfs:label "Districts of Uganda" ;
                              skos:prefLabel "Districts of Uganda" ;
                              rdfs:label "Districts of Uganda"@en ;
                              skos:prefLabel "Districts of Uganda"@en ;
                              rdfs:label "Distritos da Uganda"@pt ;
                              skos:prefLabel "Distritos da Uganda"@pt ;
                              rdfs:label "Disitulikiti mu Yuganda"@lg ;
                              skos:prefLabel "Disitulikiti mu Yuganda"@lg ;
                              rdfs:label "Wilaya za Uganda"@sw ;
                              skos:prefLabel "Wilaya za Uganda"@sw ;
                              rdfs:label "Districts du Ouganda"@fr ;
                              skos:prefLabel "Districts du Ouganda"@fr .
  
  emergelModules:UGProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of Uganda"@en ;
                                   skos:prefLabel "ISO codes for the districts of Uganda"@en ;
                                   rdfs:label "ISO codes for the districts of Uganda" ;
                                   skos:prefLabel "ISO codes for the districts of Uganda" ;
                                   rdfs:label "Codes ISO des districts du Ouganda"@fr ;
                                   skos:prefLabel "Codes ISO des districts du Ouganda"@fr ;
                                   rdfs:label "Códigos ISO dos distritos da Uganda"@pt ;
                                   skos:prefLabel "Códigos ISO dos distritos da Uganda"@pt ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:UGProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts of Uganda"@en ;
                                   skos:prefLabel "FIPS codes for the districts of Uganda"@en ;
                                   rdfs:label "FIPS codes for the districts of Uganda" ;
                                   skos:prefLabel "FIPS codes for the districts of Uganda" ;
                                   rdfs:label "Codes FIPS des districts du Ouganda"@fr ;
                                   skos:prefLabel "Codes FIPS des districts du Ouganda"@fr ;
                                   rdfs:label "Códigos FIPS dos distritos da Uganda"@pt ;
                                   skos:prefLabel "Códigos FIPS dos distritos da Uganda"@pt ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:UGProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Uganda" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Uganda" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Uganda"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Uganda"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des districts de la Ouganda"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des districts de la Ouganda"@fr ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos da Uganda"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos da Uganda"@pt ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?swLabel ;
                  skos:prefLabel ?lgLabel ;
                  skos:inScheme emergelModules:UGProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .

  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:UGProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:UGProvinceFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:UGProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .
}