PREFIX project: <http://idi.fundacionctic.org/tabels/project/municipalitiesESAS/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/municipalitiesESAS/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?INERegionCode,?INEMunicipalityCode,?ISOcode,?astLabel,?esLabel,?enLabel,?nuts2,?nuts3,?lau2,?HASCCode,?astaltLabel,?capital,?capitalast,?capitales,?capitalCode,?ipaastlabel] IN horizontal 
    
  LET ?provinceResource = resource(concat("ES_DIVISION_LEVEL_2_ES_",replace(?provinceCode,"[^a-zA-Z0-9]","")),emergelModules)
  LET ?municipalityResource = resource(concat("ES_DIVISION_LEVEL_3_",replace(?INEMunicipalityCode,"-","_")),emergelModules)
  LET ?municipalityINECodeResource = resource(concat("ES_DIVISION_LEVEL_3_INE_CODE_",replace(?INEMunicipalityCode,"-","_")),emergelModules)
  LET ?municipalityLauCodeResource = resource(concat("ES_DIVISION_LEVEL_3_LAU_2_CODE_",replace(?lau2,"-","_")),emergelModules)
  LET ?municipalityHASCCodeResource = resource(concat("ES_DIVISION_LEVEL_3_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?INEPublisher = resource("STANDARDIZATION_ORGANIZATION_INE",emergelModules)
  LET ?UNPublisher = resource("STANDARDIZATION_ORGANIZATION_UN",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  
  LET ?esLangLabel = setLangTag(?esLabel, "es")
  LET ?astLangLabel = setLangTag(?astLabel, "ast")
  LET ?enLangLabel = setLangTag(?enLabel, "en")
  LET ?ipaastLangLabel = setLangTag(?ipaastlabel, "ast-ES-fonipa")

{
  WHEN not matches(?capitalCode,"") do
  LET ?capitalResource = resource(concat("ES_PLACE_",replace(?capitalCode," ","_")),emergelModules)
  LET ?capitalCodeResource = resource(concat("ES_PLACE_LOCODE_",replace(?capitalCode," ","_")),emergelModules)
  LET ?CapitalLangEn = setLangTag(?capital,"en")
  LET ?CapitalLangAst = setLangTag(?capitalast,"ast")
  LET ?CapitalLangEs = setLangTag(?capitales,"es")
;
  WHEN not matches(?astaltLabel,"") DO
  LET ?astaltLangLabel = setLangTag(?astaltLabel,"ast")
}

  /*  LET ?caLangLabel = setLangTag(?calabel, "ca")
    LET ?euLangLabel = setLangTag(?eulabel, "eu")
    LET ?glLangLabel = setLangTag(?gllabel, "gl")
    LET ?anLangLabel = setLangTag(?anlabel, "an")
    LET ?ocLangLabel = setLangTag(?oclabel, "oc")
    LET ?ptLangLabel = setLangTag(?ptlabel, "pt")
    LET ?note_enLang = setLangTag(?note_en, "en") */    

CONSTRUCT {
  
  emergelModules:ESASOMunicipalityList a emergelModules:Country;
                              rdfs:label "Municipalities of Asturias"@en ;
                              skos:prefLabel "Municipalities of Asturias"@en ;
                              rdfs:label "Concejos del Principado de Asturias"@es ;
                              skos:prefLabel "Concejos del Principado de Asturias"@es ;
                             
                             /* rdfs:label "Municipis d'Astúries"@ca ;
                              skos:prefLabel "Municipis d'Astúries"@ca ;
                              rdfs:label "Asturiasko udalerrien zerrenda"@eu ;
                              skos:prefLabel "Asturiasko udalerrien zerrenda"@eu ;*/

                              rdfs:label "Conceyos d'Asturies"@ast ;
                              skos:prefLabel "Conceyos d'Asturies"@ast ;
                              rdfs:label "kon̪ˈθejos ðasˈtuɾjes"@ast-ES-fonipa ;
                              skos:prefLabel "kon̪ˈθejos ðasˈtuɾjes"@ast-ES-fonipa 
                            
                             /* rdfs:label "Municipios d'Asturias"@an ;
                              skos:prefLabel "Municipios d'Asturias"@an ;
                              rdfs:label "Concellos de Asturias@gl ;
                              skos:prefLabel "Concellos de Asturias"@gl ;
                              rdfs:label "Municípios das Astúrias"@pt ;
                              skos:prefLabel "Municípios das Astúrias"@pt */.
  
  emergelModules:ESASOMunicipalityINECodeList a emergelModules:Country;
                                   rdfs:label "INE codes for the municipalities of Asturias"@en ;
                                   skos:prefLabel "INE codes for the municipalities of Asturias"@en ;
                                   rdfs:label "Códigos INE de los conceyos d'Asturies"@ast ;
                                   skos:prefLabel "Códigos INE de los conceyos d'Asturies"@ast ;
                                   rdfs:label "Códigos INE de los concejos de Asturias"@es ;
                                   skos:prefLabel "Códigos INE de los concejos de Asturias"@es ;
                                   dct:publisher ?INEPublisher .

  emergelModules:ESASOMunicipalityHASCCodeList a emergelModules:Country;
                                   rdfs:label "HASC (Hierarchical administrative subdivision codes) codes for the municipalities of Spain" ;
                                   skos:prefLabel "HASC (Hierarchical administrative subdivision codes) codes for the municipalities of Spain" ;
                                   rdfs:label "HASC (Hierarchical administrative subdivision codes) codes for the municipalities of Spain"@en ;
                                   skos:prefLabel "HASC (Hierarchical administrative subdivision codes) codes for the municipalities of Spain"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios de España"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios de España"@es ;
                                   rdfs:label "Codis HASC (Hierarchical administrative subdivision codes) dels municipis d'Espanya"@ca ;
                                   skos:prefLabel "Codis HASC (Hierarchical administrative subdivision codes) dels municipis d'Espanya"@ca ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios d'España"@ast ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los municipios d'España"@ast ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:ESASOMunicipalityLAU2CodeList a emergelModules:Country;
                                   rdfs:label "LAU-2 codes for the municipalities of Asturias"@en ;
                                   skos:prefLabel "LAU-2 codes for the municipalities of Asturias"@en ;
                                   rdfs:label "Códigos LAU-2 de los conceyos d'Asturies"@ast ;
                                   skos:prefLabel "Códigos LAU-2 de los conceyos d'Asturies"@ast ;
                                   rdfs:label "Códigos LAU-2 de los concejos de Asturias"@es ;
                                   skos:prefLabel "Códigos LAU-2 de los concejos de Asturias"@es .

  emergelModules:ESPlacesList a emergelModules:Country;
                                   rdfs:label "Places in Spain"@en ;
                                   skos:prefLabel "Places in Spain"@en ;
                                   rdfs:label "Llugares n'España"@ast ;
                                   skos:prefLabel "Llugares n'España"@ast .
  
  emergelModules:ESPlacesLOCODEList a emergelModules:Country;
                                   rdfs:label "UN/LOCODEs for places in Spain"@en ;
                                   skos:prefLabel "UN/LOCODEs for places in Spain"@en ;
                                   rdfs:label "Códigos UN/LOCODE de llugares n'España"@ast ;
                                   skos:prefLabel "Códigos UN/LOCODE de llugares n'España"@ast ;
                                   dct:publisher ?UNPublisher .

  emergelModules:PlacesLOCODEList a emergelModules:Country;
                                   rdfs:label "UN/LOCODE Code List by Country"@en ;
                                   skos:prefLabel "UN/LOCODE Code List by Country"@en .
}

CONSTRUCT {
  ?municipalityResource a skos:Concept;
                  rdfs:label ?astLabel ;
                  skos:prefLabel ?esLangLabel ;
                  skos:prefLabel ?astLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?ipaastLangLabel ;
                  ontolex:phoneticRep ?ipaastLangLabel ;
                  skos:inScheme emergelModules:ESASOMunicipalityList;
                  skos:inScheme emergelModules:ESMunicipalityList;
                  emergel:hasCode ?municipalityLauCodeResource ;
                  emergel:hasCode ?municipalityINECodeResource ;
                  emergel:hasCode ?municipalityHASCCodeResource ;
                  skos:broader ?provinceResource .

  
  ?provinceResource skos:narrower ?municipalityResource .
  
  ?municipalityINECodeResource a skos:Concept;
                  rdfs:label ?INEMunicipalityCode ;
                  skos:prefLabel ?INEMunicipalityCode ; 
                  skos:inScheme emergelModules:ESASOMunicipalityINECodeList;
                  skos:inScheme emergelModules:ESMunicipalityINECodeList;
                  emergel:codeOf ?municipalityResource .

  ?municipalityLauCodeResource a skos:Concept;
                  rdfs:label ?lau2 ;
                  skos:prefLabel ?lau2 ; 
                  skos:inScheme emergelModules:ESASOMunicipalityLAU2CodeList;
                  skos:inScheme emergelModules:ESMunicipalityLAU2CodeList;
                  emergel:codeOf ?municipalityResource .

  ?municipalityHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:ESASOMunicipalityHASCCodeList;
                  skos:inScheme emergelModules:ESMunicipalityHASCCodeList;
                  emergel:codeOf ?municipalityResource .
}

CONSTRUCT{  
   ?municipalityResource emergel:capital ?capitalResource ;
                   skos:narrower ?capitalResource .
  
  ?capitalResource a skos:Concept ;
                   rdfs:label ?capital ;
                   emergel:hasCode ?capitalCodeResource ;
                   skos:inScheme emergelModules:ESPlacesList ;
                   skos:broader ?municipalityResource .
  
  ?capitalCodeResource a skos:Concept ;
                   rdfs:label ?capitalCode ;
                   skos:prefLabel ?capitalCode ;
                   emergel:codeOf ?capitalResource ;
                   skos:inScheme emergelModules:ESPlacesLOCODEList ;
                   skos:inScheme emergelModules:PlacesLOCODEList .
                     
}

CONSTRUCT {
  ?municipalityResource skos:altLabel ?astaltLangLabel .
 
}

CONSTRUCT {
  ?capitalResource skos:prefLabel ?CapitalLangEn .
 
}

CONSTRUCT {
  ?capitalResource skos:prefLabel ?CapitalLangEs .
 
}

CONSTRUCT {
  ?capitalResource skos:prefLabel ?CapitalLangAst .
 
}