PREFIX project: <http://idi.fundacionctic.org/tabels/project/cantonsLUX/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/cantonsLUX/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?cantonCode,?LAUCode,?HASCCode,?STATECCode,?canton,?de,?fr,?lb,?enalt] IN horizontal 
    
  LET ?cantonResource = resource(concat("LU_DIVISION_LEVEL_2_",replace(?cantonCode,"-","_")),emergelModules)
  LET ?cantonCodeResource = resource(concat("LU_DIVISION_LEVEL_2_ISO_CODE_",?cantonCode),emergelModules)
  LET ?LAUCodeResource = resource(concat("LU_DIVISION_LEVEL_2_LAU_CODE_",?LAUCode),emergelModules)
  LET ?HASCCodeResource = resource(concat("LU_DIVISION_LEVEL_2_HASC_CODE_",?HASCCode),emergelModules)
  LET ?STATECCodeResource = resource(concat("LU_DIVISION_LEVEL_2_STATEC_CODE_",?STATECCode),emergelModules)

  LET ?regionResource = resource(concat("LU_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)

  LET ?enLabel = setLangTag(?canton, "en")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?lbLabel = setLangTag(?lb, "lb")
  LET ?deLabel = setLangTag(?de, "de")
  LET ?enaltLabel = setLangTag(?enalt, "en")



  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?STATECPublisher = resource("STANDARDIZATION_ORGANIZATION_STATEC",emergelModules)

CONSTRUCT {
  
  emergelModules:LUCantonList a emergelModules:Country;
                              rdfs:label "Cantons of Luxembourg" ;
                              skos:prefLabel "Cantons of Luxembourg" ;
                              rdfs:label "Cantons of Luxembourg"@en ;
                              skos:prefLabel "Cantons of Luxembourg"@en ;
                              rdfs:label "Cantons of Luxembourg" ;
                              skos:prefLabel "Cantons of Luxembourg" ;
                              rdfs:label "Kantone im Großherzogtum Luxemburg"@de ;
                              skos:prefLabel "Kantone im Großherzogtum Luxemburg"@de ;  
                              rdfs:label "Kantonen zu Lëtzebuerg"@lb ;
                              skos:prefLabel "Kantonen zu Lëtzebuerg"@lb ;                              
                              rdfs:label "Cantons du Luxembourg"@fr ;
                              skos:prefLabel "Cantons du Luxembourg"@fr .

  emergelModules:LUCantonLAUCodeList a emergelModules:Country;
                                   rdfs:label "LAU codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "LAU codes for the cantons of Luxembourg" ;
                                   rdfs:label "LAU codes for the cantons of Luxembourg"@en ;
                                   skos:prefLabel "LAU codes for the cantons of Luxembourg"@en ;
                                   rdfs:label "LAU codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "LAU codes for the cantons of Luxembourg" ;
                                   rdfs:label "Codes LAU des cantons du Luxembourg"@fr ;
                                   skos:prefLabel "Codes LAU des cantons du Luxembourg"@fr ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:LUCantonISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "ISO codes for the cantons of Luxembourg" ;
                                   rdfs:label "ISO codes for the cantons of Luxembourg"@en ;
                                   skos:prefLabel "ISO codes for the cantons of Luxembourg"@en ;
                                   rdfs:label "ISO codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "ISO codes for the cantons of Luxembourg" ;
                                   rdfs:label "Codes ISO des cantons du Luxembourg"@fr ;
                                   skos:prefLabel "Codes ISO des cantons du Luxembourg"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:LUCantonSTATECCodeList a emergelModules:Country;
                                   rdfs:label "STATEC codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "STATEC codes for the cantons of Luxembourg" ;
                                   rdfs:label "STATEC codes for the cantons of Luxembourg"@en ;
                                   skos:prefLabel "STATEC codes for the cantons of Luxembourg"@en ;
                                   rdfs:label "STATEC codes for the cantons of Luxembourg" ;
                                   skos:prefLabel "STATEC codes for the cantons of Luxembourg" ;
                                   rdfs:label "Codes STATEC des cantons du Luxembourg"@fr ;
                                   skos:prefLabel "Codes STATEC des cantons du Luxembourg"@fr ;
                                   dct:publisher ?STATECPublisher .

  emergelModules:LUCantonHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the cantons of Luxembourg" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the cantons of Luxembourg" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the cantons of Luxembourg"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the cantons of Luxembourg"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des cantons de Luxembourg"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des cantons de Luxembourg"@fr ;
                                   rdfs:label "HASC-Codes (Hierarchical administrative subdivision codes) voor Luxemburg: Kantons"@nl ;
                                   skos:prefLabel "HASC-Codes (Hierarchical administrative subdivision codes) voor Luxemburg: Kantons"@nl ;
                                   rdfs:label "Liste der HASC-Codes (Hierarchical administrative subdivision codes) für Großherzogtum Luxemburg: Kantone"@de ;
                                   skos:prefLabel "Liste der HASC-Codes (Hierarchical administrative subdivision codes) für Großherzogtum Luxemburg: Kantone"@de ;
                                   rdfs:label "Lëscht vun den HASC-Codë (Hierarchical administrative subdivision codes) fir Lëtzebuerg: Kantonen"@lb ;
                                   skos:prefLabel "Lëscht vun den HASC-Codë (Hierarchical administrative subdivision codes) fir Lëtzebuerg: Kantonen"@lb ;
                                   dct:publisher ?HASCPublisher .

}


CONSTRUCT {
  ?cantonResource a skos:Concept;
                  rdfs:label ?canton ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:prefLabel ?lbLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:inScheme emergelModules:LUCantonList;
                  emergel:hasCode ?cantonCodeResource ;
                  emergel:hasCode ?LAUCodeResource ;
                  emergel:hasCode ?HASCCodeResource ;
                  emergel:hasCode ?STATECCodeResource ;
                  skos:notation ?cantonCode ;
                  skos:broader ?regionResource .
  
  ?regionResource skos:narrower ?cantonResource .

  ?cantonCodeResource a skos:Concept;
                  rdfs:label ?cantonCode ;
                  skos:prefLabel ?cantonCode ; 
                  skos:inScheme emergelModules:LUCantonISOCodeList;
                  emergel:codeOf ?cantonResource .

  ?LAUCodeResource a skos:Concept;
                  rdfs:label ?LAUCode ;
                  skos:prefLabel ?LAUCode ; 
                  skos:inScheme emergelModules:LUCantonLAUCodeList;
                  emergel:codeOf ?cantonResource .

  ?HASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:LUCantonHASCCodeList;
                  emergel:codeOf ?cantonResource .

  ?STATECCodeResource a skos:Concept;
                  rdfs:label ?STATECCode ;
                  skos:prefLabel ?STATECCode ; 
                  skos:inScheme emergelModules:LUCantonSTATECCodeList;
                  emergel:codeOf ?cantonResource .
}