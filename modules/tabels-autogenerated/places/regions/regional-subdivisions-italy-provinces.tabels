PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsITA/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsITA/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
WHEN not matches(?rowId,"") DO
    MATCH [?provinceCode,?HASCCode,?province,?italt,?italt1,?regionCode,?ISOCode,?nuts2,?nuts3,?ca,?de,?fr,?fur,?lij,?lmo,?mt,?nap,?oc,?noteoc,?sc,?scn,?sdc,?sdn,?co,?vec,?en,?enalt,?ISTATCode,?PlateCode] IN horizontal 
    
  LET ?provinceResource = resource(concat("IT_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("IT_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceISTATCodeResource = resource(concat("IT_DIVISION_LEVEL_2_ISTAT_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provincePLATECodeResource = resource(concat("IT_DIVISION_LEVEL_2_PROVINCEPLATE_CODE_",replace(?PlateCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("IT_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("IT_DIVISION_LEVEL_1_IT_",?regionCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)
  LET ?itLabel = setLangTag(?province, "it")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?deLabel = setLangTag(?de, "de")
  LET ?caLabel = setLangTag(?ca, "ca")
  LET ?ocLabel = setLangTag(?oc, "oc")
  LET ?coLabel = setLangTag(?co, "co")
  LET ?scLabel = setLangTag(?sc, "sc")
  LET ?vecLabel = setLangTag(?vec, "vec")
  LET ?lijLabel = setLangTag(?lij, "lij")
  LET ?italtLabel = setLangTag(?italt, "it")
  LET ?enaltLabel = setLangTag(?enalt, "en")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?ISTATPublisher = resource("STANDARDIZATION_ORGANIZATION_ISTAT",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)


    {
    WHEN not matches(?scn,"") DO
      LET ?scnLangLabel = setLangTag(?scn, "scn") 
    ;
    WHEN not matches(?fur,"") DO
      LET ?furLangLabel = setLangTag(?fur, "fur") 
    ;
    WHEN not matches(?sdc,"") DO
      LET ?sdcLangLabel = setLangTag(?sdc, "sdc")
    ;
    WHEN not matches(?sdn,"") DO
      LET ?sdnLangLabel = setLangTag(?sdn, "sdn")
    ;
    WHEN not matches(?italt1,"") DO
      LET ?italt1LangLabel = setLangTag(?italt1, "it") 
    ;
    WHEN not matches(?noteoc,"") DO
      LET ?noteocLangLabel = setLangTag(?noteoc, "oc") 
    ;
    WHEN not matches(?nap,"") DO
      LET ?napLangLabel = setLangTag(?nap, "nap") 
    ;
    WHEN not matches(?lmo,"") DO
      LET ?lmoLangLabel = setLangTag(?lmo, "lmo") 
    ;
    WHEN not matches(?mt,"") DO
      LET ?mtLangLabel = setLangTag(?mt, "mt") 
    }

CONSTRUCT {
  
  emergelModules:ITProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Italy" ;
                              skos:prefLabel "Provinces of Italy" ;
                              rdfs:label "Provinces of Italy"@en ;
                              skos:prefLabel "Provinces of Italy"@en ;
                              rdfs:label "Provinze d'Italia"@it ;
                              skos:prefLabel "Provinze d'Italia"@it ;
                              rdfs:label "Províncies d'Itàlia"@ca ;
                              skos:prefLabel "Províncies d'Itàlia"@ca ;
                              rdfs:label "Provìntzias de Itàlia"@sc ;
                              skos:prefLabel "Provìntzias de Itàlia"@sc ;
                              rdfs:label "Provinciis de Italie"@fur ;
                              skos:prefLabel "Provinciis de Italie"@fur ;
                              rdfs:label "Province itałiane"@vec ;
                              skos:prefLabel "Province itałiane"@vec ;
                              rdfs:label "Pruvincî d'Italia"@scn ;
                              skos:prefLabel "Pruvincî d'Italia"@scn ;
                              rdfs:label "Provinces de l'Italie"@fr ;
                              skos:prefLabel "Provinces de l'Italie"@fr ;
                              rdfs:label "Italienische Provinzen"@de ;
                              skos:prefLabel "Italienische Provinzen"@de ;
                              rdfs:label "Pruvincie taliane"@co ;
                              skos:prefLabel "Pruvincie taliane"@co ;
                              rdfs:label "Províncias d'Itàlia"@oc ;
                              skos:prefLabel "Províncias d'Itàlia"@oc ;
                              rdfs:label "Pruvincie 'e l'Italia"@nap ;
                              skos:prefLabel "Pruvincie 'e l'Italia"@nap ;
                              rdfs:label "Provinse d'Italia"@lij ;
                              skos:prefLabel "Provinse d'Italia"@lij ;
                              rdfs:label "Pruvinz d'Italia"@lmo ;
                              skos:prefLabel "Pruvinz d'Italia"@lmo ;
                              rdfs:label "Provinċji ta' L-Italja"@mt ;
                              skos:prefLabel "Provinċji ta' L-Italja"@mt .
  
  emergelModules:ITProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Italy" ;
                                   skos:prefLabel "ISO codes for the provinces of Italy" ;
                                   rdfs:label "ISO codes for the provinces of Italy"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Italy"@en ;
                                   rdfs:label "Codici ISO per le province dell'Italia"@it ;
                                   skos:prefLabel "Codici ISO per le province dell'Italia"@it ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:ITProvinceISTATCodeList a emergelModules:Country;
                                   rdfs:label "ISTAT codes for the provinces of Italy" ;
                                   skos:prefLabel "ISTAT codes for the provinces of Italy" ;
                                   rdfs:label "ISTAT codes for the provinces of Italy"@en ;
                                   skos:prefLabel "ISTAT codes for the provinces of Italy"@en ;
                                   rdfs:label "Codici ISTAT per le province dell'Italia"@it ;
                                   skos:prefLabel "Codici ISTAT per le province dell'Italia"@it ;
                                   dct:publisher ?ISTATPublisher .

  emergelModules:ITProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Italy" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Italy" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Italy"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Italy"@en ;
                                   rdfs:label "Codici HASC (Hierarchical administrative subdivision codes) per le province dell'Italia"@it ;
                                   skos:prefLabel "Codici HASC (Hierarchical administrative subdivision codes) per le province dell'Italia"@it ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:ITProvincePLATECodeList a emergelModules:Country;
                                   rdfs:label "Vehicle registration plate codes for the provinces of Italy" ;
                                   skos:prefLabel "Vehicle registration plate codes for the provinces of Italy" ;
                                   rdfs:label "Vehicle registration plate codes for the provinces of Italy"@en ;
                                   skos:prefLabel "Vehicle registration plate codes for the provinces of Italy"@en ;
                                   rdfs:label "Codici di provincia nelle targhe automobilistiche italiane"@it ;
                                   skos:prefLabel "Codici di provincia nelle targhe automobilistiche italiane"@it .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   dct:publisher ?EurostatPublisher .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?itLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?caLabel ;
                  skos:prefLabel ?coLabel ;
                  skos:prefLabel ?ocLabel ;
                  skos:prefLabel ?scLabel ;
                  skos:prefLabel ?vecLabel ;
                  skos:prefLabel ?lijLabel ;
                  skos:altLabel ?italtLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:inScheme emergelModules:ITProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?nuts3Res ;
                  emergel:hasCode ?provinceISTATCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  emergel:hasCode ?provincePLATECodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
                  //skos:broader ?ISOResource .

  ?nuts3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts2Res .
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList .
  
  //?ISOResource skos:narrower ?provinceResource .
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:ITProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceISTATCodeResource a skos:Concept;
                  rdfs:label ?ISTATCode ;
                  skos:prefLabel ?ISTATCode ; 
                  skos:inScheme emergelModules:ITProvinceISTATCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:ITProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .

  ?provincePLATECodeResource a skos:Concept;
                  rdfs:label ?PlateCode ;
                  skos:prefLabel ?PlateCode ; 
                  skos:inScheme emergelModules:ITProvincePLATECodeList;
                  emergel:codeOf ?provinceResource .
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?scnLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?sdcLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?sdnLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?furLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:altLabel ?italt1LangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:note ?noteocLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?napLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?lmoLangLabel .
 
}

CONSTRUCT {
  ?provinceResource skos:prefLabel ?mtLangLabel .
 
}