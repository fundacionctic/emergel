PREFIX project: <http://idi.fundacionctic.org/tabels/project/MKregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/MKregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode, ?altLabel, ?HASCCode, ?STATMAKCode, ?ISOCode, ?region, ?alblabel, ?bs, ?bg, ?el, ?en, ?hr, ?sh, ?sr, ?nuts1, ?nuts2, ?nuts3, ?Eastern, ?Northeastern, ?Pelagonia, ?Polog, ?Skopje, ?Southeastern, ?Southwestern, ?Vardar, ?FIPSCode] IN horizontal 
    
  LET ?regionResource = resource(concat("MK_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MK_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("MK_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("MK_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionSTATMAKCodeResource = resource(concat("MK_DIVISION_LEVEL_1_STATMAK_CODE_",replace(?STATMAKCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
      LET ?shCountryLabel = setLangTag("Makedonija", "sh")
      LET ?shcyrlCountryLabel = setLangTag("Македонија", "sh-Cyrl")
      LET ?shglagCountryLabel = setLangTag("Ⰿⰰⰽⰵⰴⱁⱀⰻⰺⰰ", "sh-Glag")
      LET ?sharabCountryLabel = setLangTag("آياٖىنۉدەقآم", "sh-Arab")

  LET ?mkLabel = setLangTag(?region, "mk")
  LET ?sqLabel = setLangTag(?alblabel, "sq")
  LET ?bsLabel = setLangTag(?bs, "bs")
  LET ?hrLabel = setLangTag(?hr, "hr")
  LET ?srLabel = setLangTag(?sr, "sr")
  LET ?shLabel = setLangTag(?sh, "sh")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?elLabel = setLangTag(?el, "el")
  LET ?mkAltLabel = setLangTag(?altLabel, "mk-Latn")
  LET ?nut1Res = resource(concat("NUTS1_CODE_LIST_",?nuts1), emergelModules)
  LET ?nut2Res = resource(concat("NUTS2_CODE_LIST_",?nuts2), emergelModules)
  LET ?nut3Res = resource(concat("NUTS3_CODE_LIST_",?nuts3), emergelModules)

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?STATMAKPublisher = resource("STANDARDIZATION_ORGANIZATION_STATMAK",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
  
{
  WHEN matches(?Eastern, "yes") DO
    LET ?easternSchema = ?regionResource
;
  WHEN matches(?Northeastern, "yes") DO
    LET ?northeasternSchema = ?regionResource
;
  WHEN matches(?Pelagonia, "yes") DO
    LET ?pelagoniaSchema = ?regionResource
;
  WHEN matches(?Polog, "yes") DO
    LET ?pologSchema = ?regionResource
;
  WHEN matches(?Skopje, "yes") DO
    LET ?skopjeSchema = ?regionResource
;
  WHEN matches(?Southeastern, "yes") DO
    LET ?southeasternSchema = ?regionResource
;
  WHEN matches(?Southwestern, "yes") DO
    LET ?southwesternSchema = ?regionResource
;
  WHEN matches(?Vardar, "yes") DO
    LET ?vardarSchema = ?regionResource
 }   

CONSTRUCT {
  
  emergelModules:MKRegionList a emergelModules:Country;
                              rdfs:label "Municipalities of Macedonia" ;
                              skos:prefLabel "Municipalities of Macedonia" ;
                              rdfs:label "Municipalities of Macedonia"@en ;
                              skos:prefLabel "Municipalities of Macedonia"@en ;
                              rdfs:label "Општини во Македонија"@mk ;
                              skos:prefLabel "Општини во Македонија"@mk ;
                              rdfs:label "Njësitë administrative të Ish Republikës Jugosllave e Maqedonisë"@sq ;
                              skos:prefLabel "Njësitë administrative të Ish Republikës Jugosllave e Maqedonisë"@sq ;
                              rdfs:label "Opštine Republike Makedonije"@sh ;
                              skos:prefLabel "Opštine Republike Makedonije"@sh ;
                              rdfs:label "Δήμοι της πρώην Γιουγκοσλαβικής Δημοκρατίας της Μακεδονίας"@el ;
                              skos:prefLabel "Δήμοι της πρώην Γιουγκοσλαβικής Δημοκρατίας της Μακεδονίας"@el ;
                              rdfs:label "Općine u Makedoniji"@hr ;
                              skos:prefLabel "Općine u Makedoniji"@hr ;
                              rdfs:label "Općine Makedonije"@bs ;
                              skos:prefLabel "Općine Makedonije"@bs ;
                              rdfs:label "Општине Републике Македоније"@sr ;
                              skos:prefLabel "Општине Републике Македоније"@sr .
  
  emergelModules:MKRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the municipalities of Macedonia" ;
                                   skos:prefLabel "ISO codes for the municipalities of Macedonia" ;
                                   rdfs:label "ISO codes for the municipalities of Macedonia"@en ;
                                   skos:prefLabel "ISO codes for the municipalities of Macedonia"@en ;
                                   rdfs:label "Кодови ИСО/ISO за Република Македонија (ISO 3166-2:MK)"@mk ;
                                   skos:prefLabel "Кодови ИСО/ISO за Република Македонија (ISO 3166-2:MK)"@mk ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:MKRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the municipalities of Macedonia" ;
                                   skos:prefLabel "FIPS codes for the municipalities of Macedonia" ;
                                   rdfs:label "FIPS codes for the municipalities of Macedonia"@en ;
                                   skos:prefLabel "FIPS codes for the municipalities of Macedonia"@en ;
                                   rdfs:label "Кодови ФИПС/FIPS за Република Македонија"@mk ;
                                   skos:prefLabel "Кодови ФИПС/FIPS за Република Македонија"@mk ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:MKRegionSTATMAKCodeList a emergelModules:Country;
                                   rdfs:label "STATMAK codes for the municipalities of Macedonia" ;
                                   skos:prefLabel "STATMAK codes for the municipalities of Macedonia" ;
                                   rdfs:label "STATMAK codes for the municipalities of Macedonia"@en ;
                                   skos:prefLabel "STATMAK codes for the municipalities of Macedonia"@en ;
                                   rdfs:label "Кодови СТАТМАК за Република Македонија"@mk ;
                                   skos:prefLabel "Кодови СТАТМАК за Република Македонија"@mk ;
                                   dct:publisher ?STATMAKPublisher .

  emergelModules:MKRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Macedonia" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Macedonia" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Macedonia"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Macedonia"@en ;
                                   rdfs:label "Кодови ХАСК/HASC (Hierarchical administrative subdivision codes) за Република Македонија"@mk ;
                                   skos:prefLabel "Кодови ХАСК/HASC (Hierarchical administrative subdivision codes) за Република Македонија"@mk ;
                                   dct:publisher ?HASCPublisher .
  
  emergelModules:MKEasternRegionList a emergelModules:Country;
                              rdfs:label "Eastern municipalities of Macedonia" ;
                              skos:prefLabel "Eastern municipalities of Macedonia" ;
                              rdfs:label "Eastern municipalities of Macedonia"@en ;
                              skos:prefLabel "Eastern municipalities of Macedonia"@en ;
                              rdfs:label "Општини во Македонија - Источен"@mk ;
                              skos:prefLabel "Општини во Македонија - Источен"@mk .
  
  emergelModules:MKNortheasternRegionList a emergelModules:Country;
                              rdfs:label "Northeastern municipalities of Macedonia" ;
                              skos:prefLabel "Northeastern municipalities of Macedonia" ;
                              rdfs:label "Northeastern municipalities of Macedonia"@en ;
                              skos:prefLabel "Northeastern municipalities of Macedonia"@en ;
                              rdfs:label "Општини во Македонија - Североисточен"@mk ;
                              skos:prefLabel "Општини во Македонија - Североисточен"@mk .
  
  emergelModules:MKPelagoniaRegionList a emergelModules:Country;
                              rdfs:label "Municipalities of Macedonia - Pelagonia" ;
                              skos:prefLabel "Municipalities of Macedonia - Pelagonia" ;
                              rdfs:label "Municipalities of Macedonia - Pelagonia"@en ;
                              skos:prefLabel "Municipalities of Macedonia - Pelagonia"@en ;
                              rdfs:label "Општини во Македонија - Пелагониски"@mk ;
                              skos:prefLabel "Општини во Македонија - Пелагониски"@mk .
  
  emergelModules:MKPologRegionList a emergelModules:Country;
                              rdfs:label "Municipalities of Macedonia - Polog"@en ;
                              skos:prefLabel "Municipalities of Macedonia - Polog"@en ;
                              rdfs:label "Municipalities of Macedonia - Polog"@en ;
                              skos:prefLabel "Municipalities of Macedonia - Polog"@en ;
                              rdfs:label "Општини во Македонија - Полошки"@mk ;
                              skos:prefLabel "Општини во Македонија - Полошки"@mk .
  
  emergelModules:MKSkopjeRegionList a emergelModules:Country;
                              rdfs:label "Municipalities of Macedonia - Skopje" ;
                              skos:prefLabel "Municipalities of Macedonia - Skopje" ;
                              rdfs:label "Municipalities of Macedonia - Skopje"@en ;
                              skos:prefLabel "Municipalities of Macedonia - Skopje"@en ;
                              rdfs:label "Општини во Македонија - Скопски"@mk ;
                              skos:prefLabel "Општини во Македонија - Скопски"@mk .
  
  emergelModules:MKSoutheasternRegionList a emergelModules:Country;
                              rdfs:label "Southeastern municipalities of Macedonia" ;
                              skos:prefLabel "Southeastern municipalities of Macedonia" ;
                              rdfs:label "Southeastern municipalities of Macedonia"@en ;
                              skos:prefLabel "Southeastern municipalities of Macedonia"@en ;
                              rdfs:label "Општини во Македонија - Југоисточен"@mk ;
                              skos:prefLabel "Општини во Македонија - Југоисточен"@mk .
  
  emergelModules:MKSouthwesternRegionList a emergelModules:Country;
                              rdfs:label "Southwestern municipalities of Macedonia" ;
                              skos:prefLabel "Southwestern municipalities of Macedonia" ;
                              rdfs:label "Southwestern municipalities of Macedonia"@en ;
                              skos:prefLabel "Southwestern municipalities of Macedonia"@en ;
                              rdfs:label "Општини во Македонија - Југозападен"@mk ;
                              skos:prefLabel "Општини во Македонија - Југозападен"@mk .
  
  emergelModules:MKVardarnRegionList a emergelModules:Country;
                              rdfs:label "Municipalities of Macedonia - Vardar" ;
                              skos:prefLabel "Municipalities of Macedonia - Vardar" ;
                              rdfs:label "Municipalities of Macedonia - Vardar"@en ;
                              skos:prefLabel "Municipalities of Macedonia - Vardar"@en ;
                              rdfs:label "Општини во Македонија - Вардарски"@mk ;
                              skos:prefLabel "Општини во Македонија - Вардарски"@mk .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   rdfs:label "Номенклатурата на територијални статистички единици (NUTS) кодови степен 1"@mk ;
                                   skos:prefLabel "Номенклатурата на територијални статистички единици (NUTS) кодови степен 1"@mk ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Номенклатурата на територијални статистички единици (NUTS) кодови степен 2"@mk ;
                                   skos:prefLabel "Номенклатурата на територијални статистички единици (NUTS) кодови степен 2"@mk ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   rdfs:label "Номенклатурата на територијални статистички единици (NUTS) кодови степен 3"@mk ;
                                   skos:prefLabel "Номенклатурата на територијални статистички единици (NUTS) кодови степен 3"@mk ;
                                   dct:publisher ?EurostatPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?mkLabel ;
                  skos:prefLabel ?bsLabel ;
                  skos:prefLabel ?elLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?hrLabel ;
                  skos:prefLabel ?shLabel ;
                  skos:prefLabel ?srLabel ;
                  skos:altLabel ?mkAltLabel ;
                  skos:prefLabel ?sqLabel ;
                  skos:inScheme emergelModules:MKRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionSTATMAKCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .

  ?nut3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nut2Res .
  
  ?nut2Res a skos:Concept;
           skos:narrower ?nut3Res ;
           skos:inScheme emergelModules:NUTS2CodeList ;
           skos:broader ?nut1Res .

  ?nut1Res a skos:Concept;
           skos:inScheme emergelModules:NUTS1CodeList ;
           skos:narrower ?nut2Res .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MKRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:MKRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:MKRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionSTATMAKCodeResource a skos:Concept;
                  rdfs:label ?STATMAKCode ;
                  skos:prefLabel ?STATMAKCode ; 
                  skos:inScheme emergelModules:MKRegionSTATMAKCodeList;
                  emergel:codeOf ?regionResource .

  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?shCountryLabel ;
               skos:prefLabel ?shcyrlCountryLabel ;
               skos:prefLabel ?sharabCountryLabel ;
               skos:prefLabel ?shglagCountryLabel .
}

CONSTRUCT {
  
  ?easternSchema skos:inScheme emergelModules:MKEasternRegionList ;
                 emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?northeasternSchema skos:inScheme emergelModules:MKNortheasternRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?pelagoniaSchema skos:inScheme emergelModules:MKPelagoniaRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?pologSchema skos:inScheme emergelModules:MKPologRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?skopjeSchema skos:inScheme emergelModules:MKSkopjeRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?southeasternSchema skos:inScheme emergelModules:MKSoutheasternRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?southwesternSchema skos:inScheme emergelModules:MKSouthwesternRegionList ;
                      emergel:hasCode ?nut3Res .
}

CONSTRUCT {
  
  ?vardarSchema skos:inScheme emergelModules:MKVardarnRegionList ;
                emergel:hasCode ?nut3Res .
}