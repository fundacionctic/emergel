PREFIX project: <http://idi.fundacionctic.org/tabels/project/USstates/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/USstates/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?stateCode, ?HASCCode, ?FIPSCode, ?MARCCode, ?state, ?ennote, ?ISOCode, ?ru, ?runote, ?fr, ?frnote, ?es, ?esnote, ?nl, ?nlnote, ?it, ?itnote, ?sm, ?ch, ?haw, ?nah, ?nahnote, ?nv, ?enalt, ?iu, ?tz, ?zip, ?ABBRCode] IN horizontal 
    
  LET ?stateResource = resource(concat("US_DIVISION_LEVEL_1_",replace(?stateCode,"-","_")),emergelModules)
  LET ?stateCodeResource = resource(concat("US_DIVISION_LEVEL_1_ISO_CODE_",replace(?stateCode,"-","_")),emergelModules)
  LET ?stateFIPSCodeResource = resource(concat("US_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?stateHASCCodeResource = resource(concat("US_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?stateMARCCodeResource = resource(concat("US_DIVISION_LEVEL_1_MARC_CODE_",replace(?MARCCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
      /*name of the country in local languages*/
      LET ?nvCountryLabel = setLangTag("Wááshindoon Bikéyah Ałhidadiidzooígíí", "nv")
      LET ?nahCountryLabel = setLangTag("Tlacetilīlli Tlahtohcāyōtl Ixachitlān", "nah")
      LET ?hawCountryLabel = setLangTag("‘Amelika Hui Pū ‘ia", "haw")
      LET ?papCountryLabel = setLangTag("Estadonan Uni di Merka", "pap")
      LET ?iuCountryLabel = setLangTag("ᐊᒥᐊᓕᑲ", "iu")

  LET ?enLangLabel = setLangTag(?state, "en")
  LET ?ruLangLabel = setLangTag(?ru, "ru")
  LET ?frLangLabel = setLangTag(?fr, "fr")
  LET ?esLangLabel = setLangTag(?es, "es")
  LET ?nlLangLabel = setLangTag(?nl, "nl")
  LET ?itLangLabel = setLangTag(?it, "it")
  LET ?enaltLangLabel = setLangTag(?enalt, "en")
  LET ?enNoteLangLabel = setLangTag(?ennote, "en")
  LET ?esNoteLangLabel = setLangTag(?esnote, "es")
  LET ?frNoteLangLabel = setLangTag(?frnote, "fr")
  LET ?ruNoteLangLabel = setLangTag(?runote, "ru")
  LET ?nlNoteLangLabel = setLangTag(?nlnote, "nl")
  LET ?itNoteLangLabel = setLangTag(?itnote, "it")


LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
LET ?MARCPublisher = resource("STANDARDIZATION_ORGANIZATION_MARC",emergelModules)
LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  

{
  WHEN not matches(?sm,"") DO
  LET ?smLangLabel = setLangTag(?sm, "sm")
;
  WHEN not matches(?ch,"") DO
  LET ?chLangLabel = setLangTag(?ch, "ch")
;
  WHEN not matches(?nah,"") DO
  LET ?nahLangLabel = setLangTag(?nah, "nah")
;
  WHEN not matches(?nahnote,"") DO
  LET ?nahNoteLangLabel = setLangTag(?nahnote, "nah")
;
  WHEN not matches(?nv,"") DO
  LET ?nvLangLabel = setLangTag(?nv, "nv")
;
  WHEN not matches(?haw,"") DO
  LET ?hawLangLabel = setLangTag(?haw, "haw")
;
  WHEN not matches(?iu,"") DO
  LET ?iuLangLabel = setLangTag(?iu, "iu")
;
  WHEN not matches(?ABBRCode,"") DO
  LET ?stateABBRCodeResource = resource(concat("US_DIVISION_LEVEL_1_ABBR_CODE_",?ABBRCode),emergelModules)
}

CONSTRUCT {
  
  emergelModules:USStateList a emergelModules:Country;
                              rdfs:label "Administrative divisions of the United States of America: States, Associated States, Federal Districts and Territories" ;
                              skos:prefLabel "Administrative divisions of the United States of America: States, Associated States, Federal Districts and Territories" ;
                              rdfs:label "Administrative divisions of the United States of America: States, Associated States, Federal Districts and Territories"@en ;
                              skos:prefLabel "Administrative divisions of the United States of America: States, Associated States, Federal Districts and Territories"@en ;
                              rdfs:label "Stati, distretti ed aree esterne degli Stati Uniti d'America"@it ;
                              skos:prefLabel "Stati, distretti ed aree esterne degli Stati Uniti d'America"@it ;
                              rdfs:label "Staten van de Verenigde Staten"@nl ;
                              skos:prefLabel "Staten van de Verenigde Staten"@nl ;
                              rdfs:label "Territorios de los Estados Unidos de América: estados, estados asociados, distritos y territorios no incorporados"@es ;
                              skos:prefLabel "Territorios de los Estados Unidos de América: estados, estados asociados, distritos y territorios no incorporados"@es ;
                              rdfs:label "Административное деление США: штаты, Федеральный округ, островные территории, внешние малые острова"@ru ;
                              skos:prefLabel "Административное деление США: штаты, Федеральный округ, островные территории, внешние малые острова"@ru ;
                              rdfs:label "États-Unis: états, districts et territoires non incorporés"@fr ;
                              skos:prefLabel "États-Unis: états, districts et territoires non incorporés"@fr ;
                              rdfs:label "Tlacetilīlli Tlahtohcāyōtl Ixachitlān īxeliuhca"@nah ;
                              skos:prefLabel "Tlacetilīlli Tlahtohcāyōtl Ixachitlān īxeliuhca"@nah ;
                              rdfs:label "Wááshindoon Bikéyah bił hadahasdzooígíí"@nv ;
                              skos:prefLabel "Wááshindoon Bikéyah bił hadahasdzooígíí"@nv .
  
  emergelModules:USStateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the administrative divisions of the United States of America" ;
                                   skos:prefLabel "ISO codes for the administrative divisions of the United States of America" ;
                                   rdfs:label "ISO codes for the administrative divisions of the United States of America"@en ;
                                   skos:prefLabel "ISO codes for the administrative divisions of the United States of America"@en ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:USStateMARCCodeList a emergelModules:Country;
                                   rdfs:label "MARC (Machine Readable Cataloging) codes for the administrative divisions of the United States of America" ;
                                   skos:prefLabel "MARC (Machine Readable Cataloging) codes for the administrative divisions of the United States of America" ;
                                   rdfs:label "MARC (Machine Readable Cataloging) codes for the administrative divisions of the United States of America"@en ;
                                   skos:prefLabel "MARC (Machine Readable Cataloging) codes for the administrative divisions of the United States of America"@en ;
                                   dct:publisher ?MARCPublisher .

  emergelModules:USStateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the administrative divisions of the United States of America" ;
                                   skos:prefLabel "FIPS codes for the administrative divisions of the United States of America" ;
                                   rdfs:label "FIPS codes for the administrative divisions of the United States of America"@en ;
                                   skos:prefLabel "FIPS codes for the administrative divisions of the United States of America"@en ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:USStateHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the administrative divisions of the United States of America" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the administrative divisions of the United States of America" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the administrative divisions of the United States of America"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the administrative divisions of the United States of America"@en ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:USStateABBRCodeList a emergelModules:Country;
                                   rdfs:label "Abbreviations conventionales des divisions administratives des États Unis d'Amérique"@fr ;
                                   skos:prefLabel "Abbreviations conventionales des divisions administratives des États Unis d'Amérique"@fr ;
                                   rdfs:label "Conventional abbreviations for the administrative divisions of the United States of America"@en ;
                                   skos:prefLabel "Conventional abbreviations for the administrative divisions of the United States of America"@en ;
                                   rdfs:label "Conventional abbreviations for the administrative divisions of the United States of America" ;
                                   skos:prefLabel "Conventional abbreviations for the administrative divisions of the United States of America" .

}

CONSTRUCT {
  ?stateResource a skos:Concept;
                  rdfs:label ?state ;
                  skos:prefLabel ?enLangLabel ; 
                  skos:prefLabel ?esLangLabel ;
                  skos:prefLabel ?frLangLabel ;
                  skos:prefLabel ?ruLangLabel ;
                  skos:prefLabel ?nlLangLabel ;
                  skos:prefLabel ?itLangLabel ;
                  skos:altLabel ?enaltLangLabel ;
                  skos:note ?enNoteLangLabel ;
                  skos:note ?frNoteLangLabel ;
                  skos:note ?esNoteLangLabel ;
                  skos:note ?ruNoteLangLabel ;
                  skos:note ?nlNoteLangLabel ;
                  skos:note ?itNoteLangLabel ;
                  skos:inScheme emergelModules:USStateList;
                  emergel:hasCode ?stateCodeResource ;
                  emergel:hasCode ?stateFIPSCodeResource ;
                  emergel:hasCode ?stateHASCCodeResource ;
                  emergel:hasCode ?stateMARCCodeResource ;
                  skos:notation ?stateCode ;
                  skos:broader ?ISOResource .
  
  
  ?ISOResource skos:narrower ?stateResource ;
              skos:prefLabel ?nvCountryLabel ;
              skos:prefLabel ?nahCountryLabel ;
              skos:prefLabel ?papCountryLabel ;
              skos:prefLabel ?iuCountryLabel ;
              skos:prefLabel ?hawCountryLabel .
  
  ?stateCodeResource a skos:Concept;
                  rdfs:label ?stateCode ;
                  skos:prefLabel ?stateCode ; 
                  skos:inScheme emergelModules:USStateISOCodeList;
                  emergel:codeOf ?stateResource .

  ?stateFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:USStateFIPSCodeList;
                  emergel:codeOf ?stateResource .

  ?stateHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:USStateHASCCodeList;
                  emergel:codeOf ?stateResource .

  ?stateMARCCodeResource a skos:Concept;
                  rdfs:label ?MARCCode ;
                  skos:prefLabel ?MARCCode ; 
                  skos:inScheme emergelModules:USStateMARCCodeList;
                  emergel:codeOf ?stateResource .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?smLangLabel .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?chLangLabel .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?nvLangLabel .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?nahLangLabel .
}

CONSTRUCT {
  ?stateResource skos:note ?nahNoteLangLabel .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?hawLangLabel .
}

CONSTRUCT {
  ?stateResource skos:prefLabel ?iuLangLabel .
}

CONSTRUCT {

  ?stateResource emergel:hasCode ?stateABBRCodeResource .

  ?stateABBRCodeResource a skos:Concept;
                  rdfs:label ?ABBRCode ;
                  skos:prefLabel ?ABBRCode ; 
                  skos:inScheme emergelModules:USStateABBRCodeList;
                  emergel:codeOf ?stateResource .
}