PREFIX project: <http://idi.fundacionctic.org/tabels/project/CUProvinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/CUProvinces/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?ONECode,?region,?esalt,?en,?Note,?esnote,?HASCCode] IN horizontal 
    
  LET ?regionResource = resource(concat("CU_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("CU_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?FIPSCodeResource = resource(concat("CU_DIVISION_LEVEL_1_FIPS_CODE_",?FIPSCode),emergelModules)
  LET ?ONECodeResource = resource(concat("CU_DIVISION_LEVEL_1_ONE_CODE_",?ONECode),emergelModules)
  LET ?HASCCodeResource = resource(concat("CU_DIVISION_LEVEL_1_HASC_CODE_",?HASCCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?esLabel = setLangTag(?region, "es")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?esaltLabel = setLangTag(?esalt, "es")

  LET ?NoteLabel = setLangTag(?Note,"en")
  LET ?esNoteLabel = setLangTag(?esnote,"es")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?ONECUPublisher = resource("STANDARDIZATION_ORGANIZATION_ONECU",emergelModules)
    

CONSTRUCT {
  
  emergelModules:CUProvincesList a emergelModules:Country;
                              rdfs:label "Provinces of Cuba" ;
                              skos:prefLabel "Provinces of Cuba" ;
                              rdfs:label "Provinces of Cuba"@en ;
                              skos:prefLabel "Provinces of Cuba"@en ;                          
                              rdfs:label "Provincias de Cuba"@es ;
                              skos:prefLabel "Provincias de Cuba"@es .
  
  emergelModules:CUProvincesISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Cuba" ;
                                   skos:prefLabel "ISO codes for the provinces of Cuba" ;
                                   rdfs:label "ISO codes for the provinces of Cuba"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Cuba"@en ;
                                   rdfs:label "Códigos ISO de las provincias de Cuba"@es ;
                                   skos:prefLabel "Códigos ISO de las provincias de Cuba"@es ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:CUProvincesHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Cuba" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Cuba" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Cuba"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Cuba"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de las provincias de Cuba"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de las provincias de Cuba"@es ;
                                   dct:publisher ?HASCPublisher . 

  emergelModules:CUProvincesFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Cuba" ;
                                   skos:prefLabel "FIPS codes for the provinces of Cuba" ;
                                   rdfs:label "FIPS codes for the provinces of Cuba"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Cuba"@en ;
                                   rdfs:label "Códigos FIPS de las provincias de Cuba"@es ;
                                   skos:prefLabel "Códigos FIPS de las provincias de Cuba"@es ;
                                   dct:publisher ?FIPSPublisher . 

  emergelModules:CUProvincesONECodeList a emergelModules:Country;
                                   rdfs:label "ONE (Oficina Nacional de Estadísticas) codes for the provinces of Cuba" ;
                                   skos:prefLabel "ONE (Oficina Nacional de Estadísticas) codes for the provinces of Cuba" ;
                                   rdfs:label "ONE (Oficina Nacional de Estadísticas) codes for the provinces of Cuba"@en ;
                                   skos:prefLabel "ONE (Oficina Nacional de Estadísticas) codes for the provinces of Cuba"@en ;
                                   rdfs:label "Códigos ONE (Oficina Nacional de Estadísticas) de las provincias de Cuba"@es ;
                                   skos:prefLabel "Códigos ONE (Oficina Nacional de Estadísticas) de las provincias de Cuba"@es ;
                                   dct:publisher ?ONECUPublisher .                                
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:altLabel ?esaltLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:note ?NoteLabel ;
                  skos:note ?esNoteLabel ;
                  skos:inScheme emergelModules:CUProvincesList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?FIPSCodeResource ;
                  emergel:hasCode ?HASCCodeResource ;
                  emergel:hasCode ?ONECodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:CUProvincesISOCodeList;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:CUProvincesFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?HASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:CUProvincesHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?ONECodeResource a skos:Concept;
                  rdfs:label ?ONECode ;
                  skos:prefLabel ?ONECode ; 
                  skos:inScheme emergelModules:CUProvincesONECodeList;
                  emergel:codeOf ?regionResource .
}
