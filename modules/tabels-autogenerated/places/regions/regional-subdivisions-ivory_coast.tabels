PREFIX project: <http://idi.fundacionctic.org/tabels/project/CIdistricts/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/CIdistricts/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?GENCCode,?region,?enlabel,?noteen,?notefr,?pop2003,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("CI_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("CI_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("CI_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("CI_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionGENCCodeResource = resource(concat("CI_DIVISION_LEVEL_1_GENC_CODE_",replace(?GENCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
  LET ?enaltCountryLabel = setLangTag("Ivory Coast", "en")

  LET ?frLabel = setLangTag(?region, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")

  LET ?noteenLabel = setLangTag(?noteen, "en")
  LET ?notefrLabel = setLangTag(?notefr, "fr")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)


CONSTRUCT {
  
  emergelModules:CIProvinceList a emergelModules:Country;
                              rdfs:label "Districts of Ivory Coast" ;
                              skos:prefLabel "Districts of Ivory Coast" ;
                              rdfs:label "Districts of Ivory Coast"@en ;
                              skos:prefLabel "Districts of Ivory Coast"@en ;
                              rdfs:label "Districts de la Côte d'Ivoire"@fr ;
                              skos:prefLabel "Districts de la Côte d'Ivoire"@fr .
  
  emergelModules:CIProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of Ivory Coast"@en ;
                                   skos:prefLabel "ISO codes for the districts of Ivory Coast"@en ;
                                   rdfs:label "ISO codes for the districts of Ivory Coast" ;
                                   skos:prefLabel "ISO codes for the districts of Ivory Coast" ;
                                   rdfs:label "Codes ISO des districts de la Côte d'Ivoire"@fr ;
                                   skos:prefLabel "Codes ISO des districts de la Côte d'Ivoire"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:CIProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts of Ivory Coast"@en ;
                                   skos:prefLabel "FIPS codes for the districts of Ivory Coast"@en ;
                                   rdfs:label "FIPS codes for the districts of Ivory Coast" ;
                                   skos:prefLabel "FIPS codes for the districts of Ivory Coast" ;
                                   rdfs:label "Codes FIPS des districts de la Côte d'Ivoire"@fr ;
                                   skos:prefLabel "Codes FIPS des districts de la Côte d'Ivoire"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:CIProvinceGENCCodeList a emergelModules:Country;
                                   rdfs:label "GENC codes for the districts of Ivory Coast"@en ;
                                   skos:prefLabel "GENC codes for the districts of Ivory Coast"@en ;
                                   rdfs:label "GENC codes for the districts of Ivory Coast" ;
                                   skos:prefLabel "GENC codes for the districts of Ivory Coast" ;
                                   rdfs:label "Codes GENC des districts de la Côte d'Ivoire"@fr ;
                                   skos:prefLabel "Codes GENC des districts de la Côte d'Ivoire"@fr .

  emergelModules:CIProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Ivory Coast" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Ivory Coast" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of Ivory Coast"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of Ivory Coast"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des districts de la Côte d'Ivoire"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des districts de la Côte d'Ivoire"@fr ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:note ?noteenLabel ;
                  skos:note ?notefrLabel ;
                  skos:inScheme emergelModules:CIProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionGENCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:altLabel ?enaltCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:CIProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:CIProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:CIProvinceHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionGENCCodeResource a skos:Concept;
                  rdfs:label ?GENCCode ;
                  skos:prefLabel ?GENCCode ; 
                  skos:inScheme emergelModules:CIProvinceGENCCodeList;
                  emergel:codeOf ?regionResource .
}