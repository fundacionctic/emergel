PREFIX project: <http://idi.fundacionctic.org/tabels/project/ESmaritimeprovinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/ESmaritimeprovinces/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX symbol: <http://idi.fundacionctic.org/map-styles/symbols/countries/spanishmaritimeprovinces/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?districtCode, ?provinceCode, ?NATOWaterbodyCode, ?district, ?esalt, ?en, ?enalt, ?ca, ?caalt, ?ast, ?esnote, ?ennote, ?canote, ?gl] IN horizontal 
    
  LET ?districtResource = resource(concat("ES_MARITIME_DIVISION_LEVEL_2_",replace(?districtCode,"-","_")),emergelModules)
  LET ?districtCodeResource = resource(concat("ES_MARITIME_LEVEL_2_CODE_",replace(?districtCode,"-","_")),emergelModules)
  LET ?provinceResource = resource(concat("ES_MARITIME_DIVISION_LEVEL_1_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?waterbodyResource = resource(concat("NATO_STANAG_WATER_BODIES_",?NATOWaterbodyCode),emergelModules)

  LET ?esLangLabel = setLangTag(?district, "es")
  LET ?caLangLabel = setLangTag(?ca, "ca")
  LET ?caaltLangLabel = setLangTag(?caalt, "ca")
  LET ?esaltLangLabel = setLangTag(?esalt, "es")
  LET ?enLangLabel = setLangTag(?en, "en")
  LET ?enaltLangLabel = setLangTag(?enalt, "en")
  LET ?astLangLabel = setLangTag(?ast, "ast")
  LET ?enaltLangLabel = setLangTag(?enalt, "en")
  LET ?glLangLabel = setLangTag(?gl, "gl")

{
  WHEN not matches(?ennote,"") DO
  LET ?enNoteLangLabel = setLangTag(?ennote, "en")
  ;
  WHEN not matches(?esnote,"") DO
  LET ?esNoteLangLabel = setLangTag(?esnote, "es")
  ;
  WHEN not matches(?canote,"") DO
  LET ?caNoteLangLabel = setLangTag(?canote, "ca")
}

CONSTRUCT {
  
  emergelModules:ESMaritimeDistrictsList a emergelModules:Country;
                              rdfs:label "Maritime districts of Spain" ;
                              skos:prefLabel "Maritime districts of Spain" ;
                              rdfs:label "Maritime districts of Spain"@en ;
                              skos:prefLabel "Maritime districts of Spain"@en ;
                              rdfs:label "Distritos marítimos de España"@es ;
                              skos:prefLabel "Distritos marítimos de España"@es ;
                              rdfs:label "Districtes marítims d'Espanya"@ca ;
                              skos:prefLabel "Districtes marítims d'Espanya"@ca ;
                              rdfs:label "Distritos marítimos d'España"@ast ;
                              skos:prefLabel "Distritos marítimos d'España"@ast ;
                              rdfs:label "Distritos marítimos de España"@gl ;
                              skos:prefLabel "Distritos marítimos de España"@gl .
  
  emergelModules:ESMaritimeDistrictsCodeList a emergelModules:Country;
                                   rdfs:label "Codes for the maritime districts of Spain" ;
                                   skos:prefLabel "Codes for the maritime districts of Spain" ;
                                   rdfs:label "Codes for the maritime districts of Spain"@en ;
                                   skos:prefLabel "Codes for the maritime districts of Spain"@en ;
                                   rdfs:label "Codis dels districtes marítims d'Espanya"@ca ;
                                   skos:prefLabel "Codis dels districtes marítims d'Espanya"@ca ;
                                   rdfs:label "Códigos de los distritos marítimos de España"@es ;
                                   skos:prefLabel "Códigos de los distritos marítimos de España"@es ;
                                   rdfs:label "Códigos de los distritos marítimos d'España"@ast ;
                                   skos:prefLabel "Códigos de los distritos marítimos d'España"@ast ;
                                   rdfs:label "Códigos dos distritos marítimos de España"@gl ;
                                   skos:prefLabel "Códigos dos distritos marítimos de España"@gl .

}

CONSTRUCT {
  ?districtResource a skos:Concept;
                  rdfs:label ?district ;
                  skos:prefLabel ?enLangLabel ; 
                  skos:prefLabel ?esLangLabel ;
                  skos:prefLabel ?astLangLabel ;
                  skos:prefLabel ?caLangLabel ;
                  skos:prefLabel ?glLangLabel ;
                  skos:altLabel ?enaltLangLabel ;
                  skos:altLabel ?caaltLangLabel ;
                  skos:altLabel ?esaltLangLabel ;
                  skos:inScheme emergelModules:ESMaritimeDistrictsList;
                  emergel:hasCode ?districtCodeResource ;
                  emergel:waterLocation ?waterbodyResource ;
                  skos:broader ?provinceResource .
  
  
  ?provinceResource skos:narrower ?districtResource .
  
  ?districtCodeResource a skos:Concept;
                  rdfs:label ?districtCode ;
                  skos:prefLabel ?districtCode ; 
                  skos:inScheme emergelModules:ESMaritimeDistrictsCodeList;
                  emergel:codeOf ?districtResource .
}

CONSTRUCT {
  ?districtResource skos:note ?enNoteLangLabel .
}

CONSTRUCT {
  ?districtResource skos:note ?esNoteLangLabel .
}

CONSTRUCT {
  ?districtResource skos:note ?caNoteLangLabel .
}