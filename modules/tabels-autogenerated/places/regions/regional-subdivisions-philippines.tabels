PREFIX project: <http://idi.fundacionctic.org/tabels/project/PHregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/PHregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?NSOPHCode,?NSOPHfullCode,?region,?tlaltlabel,?filabel,?fialtlabel,?mslabel,?enlabel,?enaltlabel,?enalt1label,?noteen,?ilolabel,?iloaltlabel,?bcllabel,?bclaltlabel,?btolabel,?ceblabel,?cebaltlabel,?pamlabel,?pamaltlabel,?paglabel,?warlabel,?waraltlabel,?hillabel,?ibglabel,?vilabel,?vialtlabel,?arlabel,?araltlabel,?zhlabel,?jalabel,?rulabel,?rualtlabel,?Luzon,?Visayas,?Mindanao] IN horizontal 
    
  LET ?regionResource = resource(concat("PH_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("PH_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionNSOPHCodeResource = resource(concat("PH_DIVISION_LEVEL_1_NSOPH_CODE_",replace(?NSOPHCode,"-","_")),emergelModules)
  LET ?regionNSOPHfullCodeResource = resource(concat("PH_DIVISION_LEVEL_1_NSOPHfull_CODE_",replace(?NSOPHfullCode,"-","_")),emergelModules)

    LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
    LET ?tlCountryLabel = setLangTag("Pilipinas", "tl")
    LET ?fiCountryLabel = setLangTag("Pilipinas", "fi")
    LET ?iloCountryLabel = setLangTag("Filipinas", "ilo")
    LET ?bclCountryLabel = setLangTag("Filipinas", "bcl")
    LET ?cebCountryLabel = setLangTag("Pilipinas", "ceb")
    LET ?pamCountryLabel = setLangTag("Filipinas", "pam")
    LET ?warCountryLabel = setLangTag("Pilipinas", "war")
    LET ?viCountryLabel = setLangTag("Philippines", "vi")
    LET ?arCountryLabel = setLangTag("الفلبين", "ar")
    LET ?idCountryLabel = setLangTag("Filipina", "id")
    LET ?jaCountryLabel = setLangTag("フィリピン", "ja")
    LET ?ruCountryLabel = setLangTag("Филиппины", "ru")

  LET ?tlLabel = setLangTag(?region, "tl")
  LET ?tlaltLabel = setLangTag(?tlaltlabel, "tl")
  LET ?fiLabel = setLangTag(?filabel, "fi")
  LET ?fialtLabel = setLangTag(?fialtlabel, "fi")
  LET ?msLabel = setLangTag(?mslabel, "ms")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?enaltLabel = setLangTag(?enaltlabel, "en")
  LET ?iloLabel = setLangTag(?ilolabel, "ilo")
  LET ?iloaltLabel = setLangTag(?iloaltlabel, "ilo")
  LET ?bclLabel = setLangTag(?bcllabel, "bcl")
  LET ?bclaltLabel = setLangTag(?bclaltlabel, "bcl")
  LET ?cebLabel = setLangTag(?ceblabel, "ceb")
  LET ?cebaltLabel = setLangTag(?cebaltlabel, "ceb")
  LET ?pamLabel = setLangTag(?pamlabel, "pam")
  LET ?pamaltLabel = setLangTag(?pamaltlabel, "pam")
  LET ?warLabel = setLangTag(?warlabel, "war")
  LET ?waraltLabel = setLangTag(?waraltlabel, "war")
  LET ?viLabel = setLangTag(?vilabel, "vi")
  LET ?vialtLabel = setLangTag(?vialtlabel, "vi")
  LET ?ruLabel = setLangTag(?rulabel, "ru")
  LET ?rualtLabel = setLangTag(?rualtlabel, "ru")
  LET ?zhLabel = setLangTag(?zhlabel, "zh")
  LET ?jaLabel = setLangTag(?jalabel, "ja")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?NSOPHPublisher = resource("STANDARDIZATION_ORGANIZATION_NSOPH",emergelModules)

{
WHEN not matches(?arlabel,"") DO
LET ?arLabel = setLangTag(?arlabel, "ar")
;
WHEN not matches(?araltlabel,"") DO
LET ?araltLabel = setLangTag(?araltlabel, "ar")
;
WHEN not matches(?ibglabel,"") DO
LET ?ibgLabel = setLangTag(?ibglabel, "ibg")
;
WHEN not matches(?paglabel,"") DO
LET ?pagLabel = setLangTag(?paglabel, "pag")
;
WHEN not matches(?btolabel,"") DO
LET ?btoLabel = setLangTag(?btolabel, "bto")
;
WHEN not matches(?hillabel,"") DO
LET ?hilLabel = setLangTag(?hillabel, "hil")
;
WHEN not matches(?enalt1label,"") DO
LET ?enalt1Label = setLangTag(?enalt1label, "en")
;
WHEN not matches(?noteen,"") DO
LET ?noteenLabel = setLangTag(?noteen, "en")
;
  WHEN matches(?Luzon, "yes") DO
    LET ?luzonSchema = ?regionResource
;
  WHEN matches(?Visayas, "yes") DO
    LET ?visayasSchema = ?regionResource
;
  WHEN matches(?Mindanao, "yes") DO
    LET ?mindanaoSchema = ?regionResource
}

CONSTRUCT {
  
  emergelModules:PHRegionList a emergelModules:Country;
                              rdfs:label "Regions of the Philippines" ;
                              skos:prefLabel "Regions of the Philippines" ;
                              rdfs:label "Regions of the Philippines"@en ;
                              skos:prefLabel "Regions of the Philippines"@en ;
                              rdfs:label "Mga rehiyon ng Pilipinas"@tl ;
                              skos:prefLabel "Mga rehiyon ng Pilipinas"@tl ;
                              rdfs:label "Mga rehiyon ng Pilipinas"@fi ;
                              skos:prefLabel "Mga rehiyon ng Pilipinas"@fi ;
                              rdfs:label "Ding Labuad ning Filipinas"@pam ;
                              skos:prefLabel "Ding Labuad ning Filipinas"@pam ;
                              rdfs:label "Mga rehiyon han Pilipinas"@war ;
                              skos:prefLabel "Mga rehiyon han Pilipinas"@war ;
                              rdfs:label "Dagiti rehion ti Filipinas"@ilo ;
                              skos:prefLabel "Dagiti rehion ti Filipinas"@ilo ;
                              rdfs:label "菲律賓行政區劃"@zh ;
                              skos:prefLabel "菲律賓行政區劃"@zh ;
                              rdfs:label "مناطق الفلبين"@ar ;
                              skos:prefLabel "مناطق الفلبين"@ar ;
                              rdfs:label "フィリピンの地方"@ja ;
                              skos:prefLabel "フィリピンの地方"@ja ;
                              rdfs:label "Vùng của Philippines"@vi ;
                              skos:prefLabel "Vùng của Philippines"@vi ;
                              rdfs:label "Region di Filipina"@id ;
                              skos:prefLabel "Region di Filipina"@id ;
                              rdfs:label "Административное деление Филиппин: Регионы Филиппин"@ru ;
                              skos:prefLabel "Административное деление Филиппин: Регионы Филиппин"@ru ;
                              rdfs:label "Daerah di Filipina"@ms ;
                              skos:prefLabel "Daerah di Filipina"@ms ;
                              rdfs:label "Mga rehiyon ug lalawigan sa Pilipinas"@ceb ;
                              skos:prefLabel "Mga rehiyon ug lalawigan sa Pilipinas"@ceb .
  
  emergelModules:PHRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of the Philippines"@en ;
                                   skos:prefLabel "ISO codes for the regions of the Philippines"@en ;
                                   rdfs:label "ISO codes for the regions of the Philippines" ;
                                   skos:prefLabel "ISO codes for the regions of the Philippines" ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:PHRegionNSOPHCodeList a emergelModules:Country;
                                   rdfs:label "NSOPh (National Statistical Office of Philippines) codes for the regions of the Philippines"@en ;
                                   skos:prefLabel "NSOPh (National Statistical Office of Philippines) codes for the regions of the Philippines"@en ;
                                   rdfs:label "NSOPh (National Statistical Office of Philippines) codes for the regions of the Philippines" ;
                                   skos:prefLabel "NSOPh (National Statistical Office of Philippines) codes for the regions of the Philippines" ;
                                   dct:publisher ?NSOPHPublisher .

  emergelModules:PHRegionNSOPHfullCodeList a emergelModules:Country;
                                   rdfs:label "NSOPh (National Statistical Office of Philippines) full geographical codes for the regions of the Philippines"@en ;
                                   skos:prefLabel "NSOPh (National Statistical Office of Philippines) full geographical codes for the regions of the Philippines"@en ;
                                   rdfs:label "NSOPh (National Statistical Office of Philippines) full geographical codes for the regions of the Philippines" ;
                                   skos:prefLabel "NSOPh (National Statistical Office of Philippines) full geographical codes for the regions of the Philippines" ;
                                   dct:publisher ?NSOPHPublisher .

  emergelModules:PHLuzonIslandList a emergelModules:Country;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Luzon" ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Luzon" ;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Luzon"@en ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Luzon"@en ;
                              rdfs:label "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Luzon"@ilo ;
                              skos:prefLabel "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Luzon"@ilo ;
                              rdfs:label "Pulo grupo ng Pilipinas: Mga rehiyon ng Luzon"@tl ;
                              skos:prefLabel "Pulo grupo ng Pilipinas: Mga rehiyon ng Luzon"@tl .
  
  emergelModules:PHVisayasIslandList a emergelModules:Country;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Visayas" ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Visayas" ;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Visayas"@en ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Visayas"@en ;
                              rdfs:label "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Visayas"@ilo ;
                              skos:prefLabel "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Visayas"@ilo ;
                              rdfs:label "Pulo grupo ng Pilipinas: Mga rehiyon ng Visayas"@tl ;
                              skos:prefLabel "Pulo grupo ng Pilipinas: Mga rehiyon ng Visayas"@tl .
  
  emergelModules:PHMindanaoIslandList a emergelModules:Country;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Mindanao" ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Mindanao" ;
                              rdfs:label "Island groups of the Philippines: Regions in the island group of Mindanao"@en ;
                              skos:prefLabel "Island groups of the Philippines: Regions in the island group of Mindanao"@en ;
                              rdfs:label "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Mindanao"@ilo ;
                              skos:prefLabel "Dagiti grupo ti isla ti Filipinas: Rehionda ti grupo ti isla Mindanao"@ilo ;
                              rdfs:label "Pulo grupo ng Pilipinas: Mga rehiyon ng Mindanao"@tl ;
                              skos:prefLabel "Pulo grupo ng Pilipinas: Mga rehiyon ng Mindanao"@tl .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?tlLabel ;
                  skos:altLabel ?tlaltLabel ;
                  skos:prefLabel ?fiLabel ;
                  skos:altLabel ?fialtLabel ;
                  skos:prefLabel ?msLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:altLabel ?rualtLabel ;
                  skos:prefLabel ?viLabel ;
                  skos:altLabel ?vialtLabel ;
                  skos:prefLabel ?cebLabel ;
                  skos:altLabel ?cebaltLabel ;
                  skos:prefLabel ?pamLabel ;
                  skos:altLabel ?pamaltLabel ;
                  skos:prefLabel ?warLabel ;
                  skos:altLabel ?waraltLabel ;
                  skos:prefLabel ?bclLabel ;
                  skos:altLabel ?bclaltLabel ;
                  skos:prefLabel ?iloLabel ;
                  skos:altLabel ?iloaltLabel ;
                  skos:prefLabel ?arLabel ;
                  skos:prefLabel ?zhLabel ;
                  skos:prefLabel ?jaLabel ;
                  skos:inScheme emergelModules:PHRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionNSOPHCodeResource ;
                  emergel:hasCode ?regionNSOPHfullCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?tlCountryLabel ;
               skos:prefLabel ?fiCountryLabel ;
               skos:prefLabel ?iloCountryLabel ;
               skos:prefLabel ?bclCountryLabel ;
               skos:prefLabel ?cebCountryLabel ;
               skos:prefLabel ?pamCountryLabel ;
               skos:prefLabel ?warCountryLabel ;
               skos:prefLabel ?viCountryLabel ;
               skos:prefLabel ?idCountryLabel ;
               skos:prefLabel ?arCountryLabel ;
               skos:prefLabel ?jaCountryLabel ;
               skos:prefLabel ?ruCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:PHRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionNSOPHCodeResource a skos:Concept;
                  rdfs:label ?NSOPHCode ;
                  skos:prefLabel ?NSOPHCode ; 
                  skos:inScheme emergelModules:PHRegionNSOPHCodeList;
                  emergel:codeOf ?regionResource .

  ?regionNSOPHfullCodeResource a skos:Concept;
                  rdfs:label ?NSOPHfullCode ;
                  skos:prefLabel ?NSOPHfullCode ; 
                  skos:inScheme emergelModules:PHRegionNSOPHfullCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?arLabel .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?araltLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?ibgLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?pagLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?btoLabel .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?hilLabel .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?enalt1Label .
}

CONSTRUCT{
  ?regionResource skos:note ?noteenLabel .
}

CONSTRUCT {
  
  ?luzonSchema skos:inScheme emergelModules:PHLuzonIslandList .
}

CONSTRUCT {
  
  ?visayasSchema skos:inScheme emergelModules:PHVisayasIslandList .
}

CONSTRUCT {
  
  ?mindanaoSchema skos:inScheme emergelModules:PHMindanaoIslandList .
}