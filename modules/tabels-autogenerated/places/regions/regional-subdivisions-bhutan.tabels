PREFIX project: <http://idi.fundacionctic.org/tabels/project/BTregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/BTregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?region,?enaltlabel,?dzlabel,?dzlatlabel,?dzlataltlabel,?nelabel,?gulabel,?hilabel,?aslabel,?newlabel,?bolabel,?rulabel,?zhlabel,?urlabel,?pop2015,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("BT_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("BT_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("BT_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("BT_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
      /*name of the country in local languages*/
      LET ?dzCountryLabel = setLangTag("འབྲུག་རྒྱལ་ཁབ་", "dz")
      LET ?dzlatCountryLabel = setLangTag("druk gyal khap", "dz-Latn")
      LET ?hiCountryLabel = setLangTag("भूटान", "hi")
      LET ?guCountryLabel = setLangTag("ભૂતાન", "gu")
      LET ?asCountryLabel = setLangTag("ভূটান", "as")
      LET ?neCountryLabel = setLangTag("भूटान", "ne")
      LET ?newCountryLabel = setLangTag("भुटान", "new")
      LET ?boCountryLabel = setLangTag("འབྲུག་ཡུལ།", "bo")
      LET ?bnCountryLabel = setLangTag("ভূটান", "bn")

  LET ?enLabel = setLangTag(?region, "en")
  LET ?dzLabel = setLangTag(?dzlabel, "dz")
  LET ?dzlatLabel = setLangTag(?dzlatlabel, "dz-Latn")
  LET ?dzlataltLabel = setLangTag(?dzlataltlabel, "dz-Latn")
  LET ?ruLabel = setLangTag(?rulabel, "ru")
  LET ?zhLabel = setLangTag(?zhlabel, "zh")
  LET ?boLabel = setLangTag(?bolabel, "bo")
  LET ?urLabel = setLangTag(?urlabel, "ur")
  LET ?hiLabel = setLangTag(?hilabel, "hi")
  LET ?guLabel = setLangTag(?gulabel, "gu")
  LET ?neLabel = setLangTag(?nelabel, "ne")
  LET ?asLabel = setLangTag(?aslabel, "as")
  LET ?newLabel = setLangTag(?newlabel, "new")
  LET ?enaltLabel = setLangTag(?enaltlabel, "en")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)

CONSTRUCT {
  
  emergelModules:BTProvinceList a emergelModules:Country;
                              rdfs:label "Departments of Buthan" ;
                              skos:prefLabel "Departments of Buthan" ;
                              rdfs:label "Departments of Buthan"@en ;
                              skos:prefLabel "Departments of Buthan"@en ;
                              rdfs:label "不丹行政区划"@zh ;
                              skos:prefLabel "不丹行政区划"@zh ;
                              rdfs:label "Дзонгдэй Бутана"@ru ;
                              skos:prefLabel "Дзонгдэй Бутана"@ru ;
                              rdfs:label "প্ৰশাসনিক অঞ্চলসমূহ"@as ;
                              skos:prefLabel "প্ৰশাসনিক অঞ্চলসমূহ"@as ;
                              rdfs:label "भूटान के जिले"@hi ;
                              skos:prefLabel "भूटान के जिले"@hi ;
                              rdfs:label "بھوٹان کے اضلاع"@ur ;
                              skos:prefLabel "بھوٹان کے اضلاع"@ur ;
                              rdfs:label "རྫོང་ཁག"@dz ;
                              skos:prefLabel "རྫོང་ཁག"@dz ;
                              rdfs:label "भूटान को जिल्ला"@ne ;
                              skos:prefLabel "भूटान को जिल्ला"@ne ;
                              rdfs:label "ભુતાન જિલ્લાઓ"@gu ;
                              skos:prefLabel "ભુતાન જિલ્લાઓ"@gu ;
                              rdfs:label "ས་ཁོངས།"@bo ;
                              skos:prefLabel "ས་ཁོངས།"@bo .
  
  emergelModules:BTProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the departments of Buthan"@en ;
                                   skos:prefLabel "ISO codes for the departments of Buthan"@en ;
                                   rdfs:label "ISO codes for the departments of Buthan" ;
                                   skos:prefLabel "ISO codes for the departments of Buthan" ;
                                   rdfs:label "Codes ISO des départements du Buthan"@fr ;
                                   skos:prefLabel "Codes ISO des départements du Buthan"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:BTProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the departments of Buthan"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Buthan"@en ;
                                   rdfs:label "FIPS codes for the departments of Buthan" ;
                                   skos:prefLabel "FIPS codes for the departments of Buthan" ;
                                   rdfs:label "Codes FIPS des départements du Buthan"@fr ;
                                   skos:prefLabel "Codes FIPS des départements du Buthan"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:BTProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Buthan" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Buthan" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Buthan"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Buthan"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des départements du Buthan"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des départements du Buthan"@fr ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?zhLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:prefLabel ?urLabel ;
                  skos:prefLabel ?boLabel ;
                  skos:prefLabel ?dzLabel ;
                  skos:prefLabel ?hiLabel ;
                  skos:prefLabel ?guLabel ;
                  skos:prefLabel ?neLabel ;
                  skos:prefLabel ?dzlatLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?asLabel ;
                  skos:prefLabel ?neLabel ;
                  skos:prefLabel ?newLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:altLabel ?dzlataltLabel ;
                  skos:inScheme emergelModules:BTProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?dzCountryLabel ;
               skos:prefLabel ?dzlatCountryLabel ;
               skos:prefLabel ?hiCountryLabel ;
               skos:prefLabel ?guCountryLabel ;
               skos:prefLabel ?neCountryLabel ;
               skos:prefLabel ?boCountryLabel ;
               skos:prefLabel ?bnCountryLabel ;
               skos:prefLabel ?newCountryLabel ;
               skos:prefLabel ?asCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:BTProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BTProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:BTProvinceHASCCodeList;
                  emergel:codeOf ?regionResource .
}