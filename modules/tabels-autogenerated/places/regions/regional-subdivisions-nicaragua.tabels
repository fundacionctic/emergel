PREFIX project: <http://idi.fundacionctic.org/tabels/project/NIDepartments/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/NIDepartments/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?HASCCode,?region,?esaltlabel,?esaltlabel1,?esaltlabel2,?esaltlabel3,?enlabel,?enaltlabel,?enaltlabel1,?esnote,?ennote] IN horizontal 
    
  LET ?regionResource = resource(concat("NI_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("NI_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?FIPSCodeResource = resource(concat("NI_DIVISION_LEVEL_1_FIPS_CODE_",?FIPSCode),emergelModules)
  LET ?HASCCodeResource = resource(concat("NI_DIVISION_LEVEL_1_HASC_CODE_",?HASCCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?esLabel = setLangTag(?region, "es")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?esaltLabel = setLangTag(?esaltlabel, "es")
  LET ?enaltLabel = setLangTag(?enaltlabel, "en")
  LET ?NoteLabel = setLangTag(?ennote,"en")
  LET ?esnoteLabel = setLangTag(?esnote,"es")
  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

{
  WHEN not matches(?esaltlabel1, "") do
  LET ?esalt1Label = setLangTag(?esaltlabel1, "es")
  ;
  WHEN not matches(?esaltlabel2, "") do
  LET ?esalt2Label = setLangTag(?esaltlabel2, "es")
  ;
  WHEN not matches(?esaltlabel3, "") do
  LET ?esalt3Label = setLangTag(?esaltlabel3, "es")
  ;
  WHEN not matches(?enaltlabel1, "") do
  LET ?enalt1Label = setLangTag(?enaltlabel1, "en")
}

CONSTRUCT {
  
  emergelModules:NIDepartmentsList a emergelModules:Country;
                              rdfs:label "Departments of Nicaragua"@en ;
                              skos:prefLabel "Departments of Nicaragua"@en ;
                              rdfs:label "Departamentos de Nicaragua"@es ;
                              skos:prefLabel "Departamentos de Nicaragua"@es ;
                              rdfs:label "Departamentos de Nicarágua"@pt ;
                              skos:prefLabel "Departamentos de Nicarágua"@pt ;
                              rdfs:label "Nicaragua īxeliuhca"@nah ;
                              skos:prefLabel "Nicaragua īxeliuhca"@nah .
  
  emergelModules:NIDepartmentsISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the departments of Nicaragua"@en ;
                                   skos:prefLabel "ISO codes for the departments of Nicaragua"@en ;
                                   rdfs:label "Códigos ISO de los departamentos de Nicaragua"@es ;
                                   skos:prefLabel "Códigos ISO de los departamentos de Nicaragua"@es ;
                                   rdfs:label "Códigos ISO dos departamentos do Nicarágua"@pt ;
                                   skos:prefLabel "Códigos ISO dos departamentos do Nicarágua"@pt ;
                                   rdfs:label "Tetãvore Nicarágua - ISO 3166-2:NI"@gn ;
                                   skos:prefLabel "Tetãvore Nicarágua - ISO 3166-2:NI"@gn ;
                                   rdfs:label "Nicaragua īxeliuhca - ISO 3166-2:GT"@nah ;
                                   skos:prefLabel "Nicaragua īxeliuhca - ISO 3166-2:GT"@nah ;
                                   dct:publisher ?ISOPublisher .
  
  emergelModules:NIDepartmentsFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the departments of Nicaragua"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Nicaragua"@en ;
                                   rdfs:label "Códigos FIPS de los departamentos de Nicaragua"@es ;
                                   skos:prefLabel "Códigos FIPS de los departamentos de Nicaragua"@es ;
                                   rdfs:label "Códigos FIPS dos departamentos do Nicarágua"@pt ;
                                   skos:prefLabel "Códigos FIPS dos departamentos do Nicarágua"@pt ;
                                   rdfs:label "Tetãvore Nicarágua - FIPS"@gn ;
                                   skos:prefLabel "Tetãvore Nicarágua - FIPS"@gn ;
                                   rdfs:label "Nicaragua īxeliuhca - FIPS"@nah ;
                                   skos:prefLabel "Nicaragua īxeliuhca - FIPS"@nah ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:NIDepartmentsHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Nicaragua" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Nicaragua" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the departments of Nicaragua"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the departments of Nicaragua"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des départements de Nicaragua"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des départements de Nicaragua"@fr ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos departamentos de Nicaragua"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos departamentos de Nicaragua"@pt ;
                                   dct:publisher ?HASCPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?enLabel ;
                  skos:altLabel ?esaltLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:note ?NoteLabel ;
                  skos:note ?esnoteLabel ;
                  skos:inScheme emergelModules:NIDepartmentsList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?FIPSCodeResource ;
                  emergel:hasCode ?HASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:NIDepartmentsISOCodeList;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:NIDepartmentsFIPSCodeList;
                  emergel:codeOf ?regionResource .
  
  ?HASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:NIDepartmentsHASCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?esalt1Label .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?esalt2Label .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?esalt3Label .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?enalt1Label .
}