PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsBM/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsBM/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ISOCode,?region,?HASCCode,?es,?esalt,?fr,?fralt,?pt,?ptalt,?noteen,?notees,?notept,?notefr] IN horizontal 
    
  LET ?regionResource = resource(concat("BM_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("BM_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("BM_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("BM_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

  LET ?enLabel = setLangTag(?region, "en")
  LET ?esLabel = setLangTag(?es, "es")
  LET ?esaltLabel = setLangTag(?esalt, "es")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?fraltLabel = setLangTag(?fralt, "fr")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?ptaltLabel = setLangTag(?ptalt, "pt")

  LET ?noteenLabel = setLangTag(?noteen, "en")
  LET ?noteesLabel = setLangTag(?notees, "es")
  LET ?notefrLabel = setLangTag(?notefr, "fr")
  LET ?noteptLabel = setLangTag(?notept, "pt")
    

CONSTRUCT {
  
  emergelModules:BMParishList a emergelModules:Country;
                              rdfs:label "Parishes of Bermuda" ;
                              skos:prefLabel "Parishes of Bermuda" ;
                              rdfs:label "Parishes of Bermuda"@en ;
                              skos:prefLabel "Parishes of Bermuda"@en ;
                              rdfs:label "Parroquias de las Bermudas"@es ;
                              skos:prefLabel "Parroquias de las Bermudas"@es ;
                              rdfs:label "Paróquias das Bermudas"@pt ;
                              skos:prefLabel "Paróquias das Bermudas"@pt ;
                              rdfs:label "Paroisses des Bermudes"@fr ;
                              skos:prefLabel "Paroisses des Bermudes"@fr .
  
  emergelModules:BMParishISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the parishes of Bermuda" ;
                                   skos:prefLabel "ISO codes for the parishes of Bermuda" ;
                                   rdfs:label "ISO codes for the parishes of Bermuda"@en ;
                                   skos:prefLabel "ISO codes for the parishes of Bermuda"@en ;
                                   rdfs:label "Códigos ISO de las parroquias de las Bermudas"@es ;
                                   skos:prefLabel "Códigos ISO de las parroquias de las Bermudas"@es ;
                                   rdfs:label "Códigos ISO das paróquias das Bermudas"@pt ;
                                   skos:prefLabel "Códigos ISO das paróquias das Bermudas"@pt ;
                                   rdfs:label "Codes ISO des paroisses des Bermudes"@fr ;
                                   skos:prefLabel "Codes ISO des paroisses des Bermudes"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:BMRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Bermuda" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Bermuda" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Bermuda"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Bermuda"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de las Bermudas"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de las parroquias de las Bermudas"@es ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) das paróquias das Bermudas"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) das paróquias das Bermudas"@pt ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des paroisses des Bermudes"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des paroisses des Bermudes"@fr ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:BMRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the parishes of Bermuda" ;
                                   skos:prefLabel "FIPS codes for the parishes of Bermuda" ;
                                   rdfs:label "FIPS codes for the parishes of Bermuda"@en ;
                                   skos:prefLabel "FIPS codes for the parishes of Bermuda"@en ;
                                   rdfs:label "Códigos FIPS de las parroquias de las Bermudas"@es ;
                                   skos:prefLabel "Códigos FIPS de las parroquias de las Bermudas"@es ;
                                   rdfs:label "Códigos FIPS das paróquias das Bermudas"@pt ;
                                   skos:prefLabel "Códigos FIPS das paróquias das Bermudas"@pt ;
                                   rdfs:label "Codes FIPS des paroisses des Bermudes"@fr ;
                                   skos:prefLabel "Codes FIPS des paroisses des Bermudes"@fr ;
                                   dct:publisher ?FIPSPublisher .
}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?esaltLabel ; 
                  skos:prefLabel ?frLabel ;
                  skos:altLabel ?fraltLabel ; 
                  skos:prefLabel ?ptLabel ;
                  skos:altLabel ?ptaltLabel ;
                  skos:note ?noteenLabel ;
                  skos:note ?noteesLabel ;
                  skos:note ?notefrLabel ;
                  skos:note ?noteptLabel ;
                  skos:inScheme emergelModules:BMParishList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:broader ?ISOResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BMRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .


  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:BMRegionHASCCodeList;
                  emergel:codeOf ?regionResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:BMParishISOCodeList;
                  emergel:codeOf ?regionResource .
}
