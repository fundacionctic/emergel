PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsBH/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsBH/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?enlabel,?altlabel1,?ISOCode,?FIPSCode] IN horizontal 
    
  LET ?regionResource = resource(concat("BH_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("BH_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("BH_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "ar")
  LET ?altLanglabel1 = setLangTag(?altlabel1, "ar-Latn")
  LET ?enLanglabel = setLangTag(?enlabel, "en")

CONSTRUCT {
  
  emergelModules:BHGovernorateList a emergelModules:Country;
                              rdfs:label "Governorates of Bahrain" ;
                              skos:prefLabel "Governorates of Bahrain" ;
                              rdfs:label "Governorates of Bahrain"@en ;
                              skos:prefLabel "Governorates of Bahrain"@en ;
                              rdfs:label "محافظات البحرين"@ar ;
                              skos:prefLabel "محافظات البحرين"@ar ;
                              rdfs:label "mhafzat albhryn"@ar-Latn ;
                              skos:prefLabel "mhafzat albhryn"@ar-Latn .
  
  emergelModules:BHGovernorateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the governorates of Bahrain" ;
                                   skos:prefLabel "ISO codes for the governorates of Bahrain" ;
                                   rdfs:label "ISO codes for the governorates of Bahrain"@en ;
                                   skos:prefLabel "ISO codes for the governorates of Bahrain"@en ;
                                   rdfs:label "ISO 3166-2:BH - محافظات البحرين"@ar ;
                                   skos:prefLabel "ISO 3166-2:BH - محافظات البحرين"@ar ;
                                   rdfs:label "ISO 3166-2:BH - mhafzat albhryn"@ar-Latn ;
                                   skos:prefLabel "ISO 3166-2:BH - mhafzat albhryn"@ar-Latn .

  emergelModules:BHGovernorateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the governorates of Bahrain" ;
                                   skos:prefLabel "FIPS codes for the governorates of Bahrain" ;
                                   rdfs:label "FIPS codes for the governorates of Bahrain"@en ;
                                   skos:prefLabel "FIPS codes for the governorates of Bahrain"@en ;
                                   rdfs:label "FIPS - محافظات البحرين"@ar ;
                                   skos:prefLabel "FIPS - محافظات البحرين"@ar ;
                                   rdfs:label "FIPS - mhafzat albhryn"@ar-Latn ;
                                   skos:prefLabel "FIPS - mhafzat albhryn"@ar-Latn .

}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?altLanglabel1 ; 
                  skos:inScheme emergelModules:BHGovernorateList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:BHGovernorateISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:BHGovernorateFIPSCodeList;
                  emergel:codeOf ?regionResource .
}