PREFIX project: <http://idi.fundacionctic.org/tabels/project/YEregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/YEregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?MOPHPCode,?region,?arlat1label,?arlabel,?enlabel,?noteen,?pop2004,?areakm2,?areami2,?capital,?proposed] IN horizontal 
    
  LET ?regionResource = resource(concat("YE_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("YE_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("YE_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("YE_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionMOPHPCodeResource = resource(concat("YE_DIVISION_LEVEL_1_MOPHP_CODE_",replace(?MOPHPCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
    LET ?amCountryLabel = setLangTag("የመን", "am")
    LET ?soCountryLabel = setLangTag("Yemen", "so")
    LET ?omCountryLabel = setLangTag("Yaman", "om")
    LET ?kuCountryLabel = setLangTag("Yemen", "ku")
    LET ?urCountryLabel = setLangTag("یمن", "ur")
    LET ?faCountryLabel = setLangTag("یمن", "fa")

  LET ?arlatLabel = setLangTag(?region, "ar-Latn")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?arLabel = setLangTag(?arlabel, "ar")
  LET ?arlat1Label = setLangTag(?arlat1label, "ar-Latn")

  LET ?noteenLabel = setLangTag(?noteen,"en")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)

CONSTRUCT {
  
  emergelModules:YERegionList a emergelModules:Country;
                              rdfs:label "Governorates of Yemen" ;
                              skos:prefLabel "Governorates of Yemen" ;
                              rdfs:label "Governorates of Yemen"@en ;
                              skos:prefLabel "Governorates of Yemen"@en ;
                              rdfs:label "محافظات اليمن"@ar ;
                              skos:prefLabel "محافظات اليمن"@ar ;
                              rdfs:label "محافظات یمن"@ur ;
                              skos:prefLabel "محافظات یمن"@ur ;
                              rdfs:label "استان‌های یمن"@fa ;
                              skos:prefLabel "استان‌های یمن"@fa .
  
  emergelModules:YERegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the governorates of Yemen"@en ;
                                   skos:prefLabel "ISO codes for the governorates of Yemen"@en ;
                                   rdfs:label "ISO codes for the governorates of Yemen" ;
                                   skos:prefLabel "ISO codes for the governorates of Yemen" ;
                                   rdfs:label "محافظات اليمن- ISO 3166-2:YE"@ar ;
                                   skos:prefLabel "محافظات اليمن - ISO 3166-2:YE"@ar ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:YERegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the governorates of Yemen"@en ;
                                   skos:prefLabel "FIPS codes for the governorates of Yemen"@en ;
                                   rdfs:label "FIPS codes for the governorates of Yemen" ;
                                   skos:prefLabel "FIPS codes for the governorates of Yemen" ;
                                   rdfs:label "محافظات اليمن - FIPS"@ar ;
                                   skos:prefLabel "محافظات اليمن - FIPS"@ar ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:YERegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the governorates of Yemen" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the governorates of Yemen" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the governorates of Yemen"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the governorates of Yemen"@en ;
                                   rdfs:label "محافظات اليمن - HASC (Hierarchical administrative subdivision codes)"@ar ;
                                   skos:prefLabel "محافظات اليمن - HASC (Hierarchical administrative subdivision codes)"@ar ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:YERegionMOPHPCodeList a emergelModules:Country;
                                   rdfs:label "MOPHP (Yemen Ministry of Public Health and Population) codes for the governorates of Yemen"@en ;
                                   skos:prefLabel "MOPHP (Yemen Ministry of Public Health and Population) codes for the governorates of Yemen"@en ;
                                   rdfs:label "MOPHP (Yemen Ministry of Public Health and Population) codes for the governorates of Yemen" ;
                                   skos:prefLabel "MOPHP (Yemen Ministry of Public Health and Population) codes for the governorates of Yemen" .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?arlatLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?arLabel ;
                  skos:altLabel ?arlat1Label ;
                  skos:note ?noteenLabel ;
                  skos:inScheme emergelModules:YERegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionMOPHPCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?amCountryLabel ;
               skos:prefLabel ?soCountryLabel ;
               skos:prefLabel ?omCountryLabel ;
               skos:prefLabel ?kuCountryLabel ;
               skos:prefLabel ?urCountryLabel ;
               skos:prefLabel ?faCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:YERegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:YERegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:YERegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionMOPHPCodeResource a skos:Concept;
                  rdfs:label ?MOPHPCode ;
                  skos:prefLabel ?MOPHPCode ; 
                  skos:inScheme emergelModules:YERegionMOPHPCodeList;
                  emergel:codeOf ?regionResource .
}