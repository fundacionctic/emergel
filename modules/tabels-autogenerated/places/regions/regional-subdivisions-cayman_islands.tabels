PREFIX project: <http://idi.fundacionctic.org/tabels/project/districtsKY/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/districtsKY/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?region,?HASCCode,?pop2010,?ISOCode,?frlabel,?eslabel,?ptlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("KY_DIVISION_LEVEL_1_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("KY_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "en")
  LET ?esLanglabel = setLangTag(?eslabel, "es")
  LET ?frLanglabel = setLangTag(?frlabel, "fr")
  LET ?ptLanglabel = setLangTag(?ptlabel, "pt")

  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

CONSTRUCT {
  
  emergelModules:KYDistrictList a emergelModules:Country;
                              rdfs:label "Districts of the Cayman Islands" ;
                              skos:prefLabel "Districts of the Cayman Islands" ;
                              rdfs:label "Districts of the Cayman Islands"@en ;
                              skos:prefLabel "Districts of the Cayman Islands"@en ;
                              rdfs:label "Distritos de las Islas Caimán"@es ;
                              skos:prefLabel "Distritos de las Islas Caimán"@es ;
                              rdfs:label "Distritos das Ilhas Cayman"@pt ;
                              skos:prefLabel "Distritos das Ilhas Cayman"@pt ;
                              rdfs:label "Districts des Îles Caïman"@fr ;
                              skos:prefLabel "Districts des Îles Caïman"@fr .

  emergelModules:KYDistrictHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of the Cayman Islands" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of the Cayman Islands" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the districts of the Cayman Islands"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the districts of the Cayman Islands"@en ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) de los distritos de las Islas Caimán"@es ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) de los distritos de las Islas Caimán"@es ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des districts des Îles Caïman"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des districts des Îles Caïman"@fr ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos das Ilhas Cayman"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) dos distritos das Ilhas Cayman"@pt ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?esLanglabel ;
                  skos:prefLabel ?frLanglabel ;
                  skos:prefLabel ?ptLanglabel ;
                  skos:inScheme emergelModules:KYDistrictList;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?HASCCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:KYDistrictHASCCodeList;
                  emergel:codeOf ?regionResource .
}