PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsPK/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsPK/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
MATCH [?region,?ISOCode,?regionCode,?FIPSCode,?HASCCode,?urlabel,?enaltlabel,?falabel,?pnblabel,?sdlabel,?ennote,?fanote] IN horizontal 
    
  LET ?regionResource = resource(concat("PK_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("PK_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("PK_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        LET ?ISOResource = resource(?ISOCode,euCountry)
      /*name of the country in local languages*/
      LET ?lrcCountryLabel = setLangTag("پاکئستان", "lrc")
      LET ?mznCountryLabel = setLangTag("پاکستون", "mzn")
      LET ?psCountryLabel = setLangTag("پاکستان", "ps")
      LET ?sdCountryLabel = setLangTag("پاڪستان", "sd")
      LET ?kaaCountryLabel = setLangTag("Pakistan", "kaa")

  LET ?enLabel = setLangTag(?region, "en")
  LET ?faLabel = setLangTag(?falabel, "fa")
  LET ?urLabel = setLangTag(?urlabel, "ur")
  LET ?pnbLabel = setLangTag(?pnblabel, "pnb")
  LET ?enaltLabel = setLangTag(?enaltlabel, "en")

  LET ?enNoteLabel = setLangTag(?ennote, "en")

{
  WHEN not matches(?fanote,"") DO
  LET ?faNoteLabel = setLangTag(?fanote, "fa")
;
  WHEN not matches(?sdlabel,"") DO
  LET ?sdLabel = setLangTag(?sdlabel, "sd")
}   

CONSTRUCT {
  
  emergelModules:PKRegionList a emergelModules:Country;
                              rdfs:label "Provinces and administrative units of Pakistan" ;
                              skos:prefLabel "Provinces and administrative units of Pakistan" ;
                              rdfs:label "Provinces and administrative units of Pakistan"@en ;
                              skos:prefLabel "Provinces and administrative units of Pakistan"@en ;
                              rdfs:label "تقسیمات کشوری پاکستان"@fa ;
                              skos:prefLabel "تقسیمات کشوری پاکستان"@fa ;
                              rdfs:label "پاکستان دی ونڈ"@pnb ;
                              skos:prefLabel "پاکستان دی ونڈ"@pnb ;
                              rdfs:label "巴基斯坦行政区划"@zh ;
                              skos:prefLabel "巴基斯坦行政区划"@zh ;
                              rdfs:label "پاکستان کی انتظامی تقسیم"@ur ;
                              skos:prefLabel "پاکستان کی انتظامی تقسیم"@ur ;
                              rdfs:label "ولايات أفغانستان"@ar ;
                              skos:prefLabel "ولايات أفغانستان"@ar ;
                              rdfs:label "Административное деление Пакистана - провинции"@ru ;
                              skos:prefLabel "Административное деление Пакистана - провинции"@ru .
  
  emergelModules:PKRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces and administrative units of Pakistan" ;
                                   skos:prefLabel "ISO codes for the provinces and administrative units of Pakistan" ;
                                   rdfs:label "ISO codes for the provinces and administrative units of Pakistan"@en ;
                                   skos:prefLabel "ISO codes for the provinces and administrative units of Pakistan"@en ;
                                   rdfs:label "Геокоды Пакистана ISO 3166-2:PK"@ru ;
                                   skos:prefLabel "Геокоды Пакистана ISO 3166-2:PK"@ru ;
                                   rdfs:label "ISO 3166-2:PK کدها"@fa ;
                                   skos:prefLabel "ISO 3166-2:PK کدها"@fa ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:PKRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces and administrative units of Pakistan" ;
                                   skos:prefLabel "FIPS codes for the provinces and administrative units of Pakistan" ;
                                   rdfs:label "FIPS codes for the provinces and administrative units of Pakistan"@en ;
                                   skos:prefLabel "FIPS codes for the provinces and administrative units of Pakistan"@en ;
                                   rdfs:label "Геокоды Пакистана ФИПС (FIPS)"@ru ;
                                   skos:prefLabel "Геокоды Пакистана ФИПС (FIPS)"@ru ;
                                   rdfs:label "FIPS کدها"@fa ;
                                   skos:prefLabel "FIPS کدها"@fa ;
                                   dct:publisher ?FIPSPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?faLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?urLabel ;
                  skos:prefLabel ?pnbLabel ;
                  skos:altLabel ?enaltLabel ;
                  skos:note ?enNoteLabel ;
                  skos:inScheme emergelModules:PKRegionList ;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .

  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?lrcCountryLabel ;
               skos:prefLabel ?mznCountryLabel ;
               skos:prefLabel ?kaaCountryLabel ;
               skos:prefLabel ?sdCountryLabel ;
               skos:prefLabel ?psCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:PKRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:PKRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?sdLabel .
}

CONSTRUCT {
  ?regionResource skos:note ?faNoteLabel .
}
