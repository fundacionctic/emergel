PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsDZ/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsDZ/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?ONSCode,?region,?arlabel,?araltlabel,?ISOCode,?enlabel,?frlabel,?fraltlabel,?fraltlabel1,?kabtflabel,?kablatlabel,?kablataltlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("DZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("DZ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("DZ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionONSCodeResource = resource(concat("DZ_DIVISION_LEVEL_1_ONS_CODE_",replace(?ONSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "ar-Latn")
  LET ?enLanglabel = setLangTag(?enlabel, "en")
  LET ?frLanglabel = setLangTag(?frlabel, "fr")
  LET ?fraltLanglabel = setLangTag(?fraltlabel, "fr")
  LET ?kablatLanglabel = setLangTag(?kablatlabel, "kab")
  LET ?fraltLanglabel1 = setLangTag(?fraltlabel1, "fr")
  LET ?arLanglabel = setLangTag(?arlabel, "ar")
  LET ?araltLanglabel = setLangTag(?araltlabel, "ar")

{
WHEN not matches (?kablataltlabel, "") DO
LET ?kablataltLanglabel = setLangTag(?kablataltlabel, "kab")
;
WHEN not matches (?kabtflabel, "") DO
LET ?kabtfLanglabel = setLangTag(?kabtflabel, "kab-Tfng")
}


CONSTRUCT {
  
  emergelModules:DZProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Algeria" ;
                              skos:prefLabel "Provinces of Algeria" ;
                              rdfs:label "Provinces of Algeria"@en ;
                              skos:prefLabel "Provinces of Algeria"@en ;
                              rdfs:label "Wilayas d'Algérie"@fr ;
                              skos:prefLabel "Wilayas d'Algérie"@fr ;
                              rdfs:label "ولايات الجزائر"@ar ;
                              skos:prefLabel "ولايات الجزائر"@ar ;
                              rdfs:label "`ulaiat algzaj'r`"@ar-Latn ;
                              skos:prefLabel "`ulaiat algzaj'r`"@ar-Latn .
  
  emergelModules:DZProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Algeria" ;
                                   skos:prefLabel "ISO codes for the provinces of Algeria" ;
                                   rdfs:label "ISO codes for the provinces of Algeria"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Algeria"@en ;
                                   rdfs:label "Codes ISO 3166-2:DZ des wilayas d'Algérie"@fr ;
                                   skos:prefLabel "Codes ISO 3166-2:DZ des wilayas d'Algérie"@fr ;
                                   rdfs:label "ISO 3166-2:DZ - ولايات الجزائر"@ar ;
                                   skos:prefLabel "ISO 3166-2:DZ - ولايات الجزائر"@ar ;
                                   rdfs:label "ISO 3166-2:DZ - `ulaiat algzaj'r`"@ar-Latn ;
                                   skos:prefLabel "ISO 3166-2:DZ - `ulaiat algzaj'r`"@ar-Latn .

  emergelModules:DZProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Algeria" ;
                                   skos:prefLabel "FIPS codes for the provinces of Algeria" ;
                                   rdfs:label "FIPS codes for the provinces of Algeria"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Algeria"@en ;
                                   rdfs:label "Codes FIPS des wilayas d'Algérie"@fr ;
                                   skos:prefLabel "Codes FIPS des wilayas d'Algérie"@fr ;
                                   rdfs:label "FIPS - ولايات الجزائر"@ar ;
                                   skos:prefLabel "FIPS - ولايات الجزائر"@ar ;
                                   rdfs:label "FIPS - `ulaiat algzaj'r`"@ar-Latn ;
                                   skos:prefLabel "FIPS - `ulaiat algzaj'r`"@ar-Latn .

  emergelModules:DZProvinceONSCodeList a emergelModules:Country;
                                   rdfs:label "ONS codes for the provinces of Algeria" ;
                                   skos:prefLabel "ONS codes for the provinces of Algeria" ;
                                   rdfs:label "ONS codes for the provinces of Algeria"@en ;
                                   skos:prefLabel "ONS codes for the provinces of Algeria"@en ;
                                   rdfs:label "Codes ONS des wilayas d'Algérie"@fr ;
                                   skos:prefLabel "Codes ONS des wilayas d'Algérie"@fr ;
                                   rdfs:label "ONS - ولايات الجزائر"@ar ;
                                   skos:prefLabel "ONS - ولايات الجزائر"@ar ;
                                   rdfs:label "ONS - `ulaiat algzaj'r`"@ar-Latn ;
                                   skos:prefLabel "ONS - `ulaiat algzaj'r`"@ar-Latn .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?arLanglabel ;
                  skos:altLabel ?araltLanglabel ;
                  skos:prefLabel ?frLanglabel ;
                  skos:altLabel ?fraltLanglabel1 ;
                  skos:altLabel ?fraltLanglabel ;
                  skos:prefLabel ?enLanglabel ;
                  skos:inScheme emergelModules:DZProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionONSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:DZProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:DZProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionONSCodeResource a skos:Concept;
                  rdfs:label ?ONSCode ;
                  skos:prefLabel ?ONSCode ; 
                  skos:inScheme emergelModules:DZProvinceONSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?kabtfLanglabel .
}

CONSTRUCT {
  ?regionResource skos:altLabel ?kablataltLanglabel .
}