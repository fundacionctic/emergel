PREFIX project: <http://idi.fundacionctic.org/tabels/project/LTregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/LTregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?HASCCode,?region,?ISOCode,?lvlabel,?pllabel,?rulabel,?etlabel,?enlabel,?svlabel,?belabel,?nuts1,?nuts2,?nuts3,?FIPSCode] IN horizontal 
    
  LET ?regionResource = resource(concat("LT_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("LT_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("LT_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("LT_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?nuts1Res = resource(?nuts1, nuts)
  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)
  LET ?ltLabel = setLangTag(?region, "lt")
  LET ?lvLabel = setLangTag(?lvlabel, "lv")
  LET ?plLabel = setLangTag(?pllabel, "pl")
  LET ?ruLabel = setLangTag(?rulabel, "ru")
  LET ?etLabel = setLangTag(?etlabel, "et")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?svLabel = setLangTag(?svlabel, "sv")
  LET ?beLabel = setLangTag(?belabel, "be")


LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
    

CONSTRUCT {
  
  emergelModules:LTCountyList a emergelModules:Country;
                              rdfs:label "Counties of Lithuania" ;
                              skos:prefLabel "Counties of Lithuania" ;
                              rdfs:label "Counties of Lithuania"@en ;
                              skos:prefLabel "Counties of Lithuania"@en ;
                              rdfs:label "Lietuvos apskritys"@lt ;
                              skos:prefLabel "Lietuvos apskritys"@lt ;
                              rdfs:label "Уезды Литвы"@ru ;
                              skos:prefLabel "Уезды Литвы"@ru ;
                              rdfs:label "Lietuvas apriņķi"@lv ;
                              skos:prefLabel "Lietuvas apriņķi"@lv ;
                              rdfs:label "Podział administracyjny Litwy"@pl ;
                              skos:prefLabel "Podział administracyjny Litwy"@pl ;
                              rdfs:label "Leedu maakonnad"@et ;
                              skos:prefLabel "Leedu maakonnad"@et ;
                              rdfs:label "Паветы Літвы"@be ;
                              skos:prefLabel "Паветы Літвы"@be ;
                              rdfs:label "Administrativ indelning: Län av Litauen"@sv ;
                              skos:prefLabel "Administrativ indelning: Län av Litauen"@sv .
  
  emergelModules:LTCountyISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the counties of Lithuania" ;
                                   skos:prefLabel "ISO codes for the counties of Lithuania" ;
                                   rdfs:label "ISO codes for the counties of Lithuania"@en ;
                                   skos:prefLabel "ISO codes for the counties of Lithuania"@en ;
                                   rdfs:label "ISO 3166-2:LT - Apskritys"@lt ;
                                   skos:prefLabel "ISO 3166-2:LT - Apskritys"@lt ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:LTCountyFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the counties of Lithuania" ;
                                   skos:prefLabel "FIPS codes for the counties of Lithuania" ;
                                   rdfs:label "FIPS codes for the counties of Lithuania"@en ;
                                   skos:prefLabel "FIPS codes for the counties of Lithuania"@en ;
                                   rdfs:label "FIPS - Apskritys"@lt ;
                                   skos:prefLabel "FIPS - Apskritys"@lt ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:LTCountyHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the counties of Lithuania" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the cantons of Lithuania" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the counties of Lithuania"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the counties of Lithuania"@en ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   rdfs:label "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 3"@lt ;
                                   skos:prefLabel "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 3"@lt ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   rdfs:label "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 2"@lt ;
                                   skos:prefLabel "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 2"@lt ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 1"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 1"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 1"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 1"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 1"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 1"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 1"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 1"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 1"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 1"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 1"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 1"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 1"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 1"@sk ;
                                   rdfs:label "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 1"@lt ;
                                   skos:prefLabel "Teritorinių statistinių vienetų nomenklatūra (NUTS) - lygmuo 1"@lt ;
                                   dct:publisher ?EurostatPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?ltLabel ;
                  skos:prefLabel ?lvLabel ;
                  skos:prefLabel ?plLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:prefLabel ?etLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?beLabel ;
                  skos:prefLabel ?svLabel ;
                  skos:inScheme emergelModules:LTCountyList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?nuts3Res ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:LTCountyISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:LTCountyFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:LTCountyHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?nuts3Res a skos:Concept;
           emergel:codeOf ?regionResource ;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nuts2Res .
  
  ?nuts2Res a skos:Concept;
           skos:narrower ?nuts3Res ;
           skos:inScheme emergelModules:NUTS2CodeList .

  ?nuts1Res a skos:Concept;
           skos:narrower ?nuts2Res ;
           skos:inScheme emergelModules:NUTS1CodeList .
}
