PREFIX project: <http://idi.fundacionctic.org/tabels/project/GAprovinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/GAprovinces/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?region,?enlabel,?pop2003,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("GA_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("GA_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("GA_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("GA_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?frLabel = setLangTag(?region, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)


CONSTRUCT {
  
  emergelModules:GAProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Gabon" ;
                              skos:prefLabel "Provinces of Gabon" ;
                              rdfs:label "Provinces of Gabon"@en ;
                              skos:prefLabel "Provinces of Gabon"@en ;
                              rdfs:label "Provinces du Gabon"@fr ;
                              skos:prefLabel "Provinces du Gabon"@fr .
  
  emergelModules:GAProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Gabon"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Gabon"@en ;
                                   rdfs:label "ISO codes for the provinces of Gabon" ;
                                   skos:prefLabel "ISO codes for the provinces of Gabon" ;
                                   rdfs:label "Codes ISO des provinces du Gabon"@fr ;
                                   skos:prefLabel "Codes ISO des provinces du Gabon"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:GAProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Gabon"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Gabon"@en ;
                                   rdfs:label "FIPS codes for the provinces of Gabon" ;
                                   skos:prefLabel "FIPS codes for the provinces of Gabon" ;
                                   rdfs:label "Codes FIPS des provinces du Gabon"@fr ;
                                   skos:prefLabel "Codes FIPS des provinces du Gabon"@fr ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:GAProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Gabon" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Gabon" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Gabon"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Gabon"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des provinces du Gabon"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des provinces du Gabon"@fr ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:inScheme emergelModules:GAProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:GAProvinceISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GAProvinceFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:GAProvinceHASCCodeList;
                  emergel:codeOf ?regionResource .
}