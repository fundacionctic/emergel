PREFIX project: <http://idi.fundacionctic.org/tabels/project/shiresAU/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/shiresAU/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?provinceCode,?province,?area,?pop2003,?pop2011,?pop2013] IN horizontal 
    
  LET ?provinceResource = resource(concat("AU_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("AU_DIVISION_LEVEL_2_ABS_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("AU_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?enLabel = setLangTag(?province, "en")


CONSTRUCT {
  
  emergelModules:AUDistrictList a emergelModules:Country;
                              rdfs:label "Shires, councils and cities of Australia" ;
                              skos:prefLabel "Shires, councils and cities of Australia" ;
                              rdfs:label "Shires, councils and cities of Australia"@en ;
                              skos:prefLabel "Shires, councils and cities of Australia"@en .
  
  emergelModules:AUDistrictABSCodeList a emergelModules:Country;
                                   rdfs:label "ABS (Australian Bureau of Statistics) codes for the shires, councils and cities of Australia" ;
                                   skos:prefLabel "ABS (Australian Bureau of Statistics) codes for the shires, councils and cities of Australia" ;
                                   rdfs:label "ABS (Australian Bureau of Statistics) codes for the shires, councils and cities of Australia"@en ;
                                   skos:prefLabel "ABS (Australian Bureau of Statistics) codes for the shires, councils and cities of Australia"@en .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ;
                  skos:inScheme emergelModules:AUDistrictList;
                  emergel:hasCode ?provinceCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
                 // skos:broader ?ISOResource .
  
  // ?ISOResource skos:narrower ?provinceResource .

  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:AUDistrictABSCodeList;
                  emergel:codeOf ?provinceResource .
}