PREFIX project: <http://idi.fundacionctic.org/tabels/project/GGparishes/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/GGparishes/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode, ?regionCode, ?en, ?fr, ?region, ?nrfalt, ?noteen, ?notefr, ?pop2001, ?aresqkm, ?aresqmi] IN horizontal 
    
  LET ?regionResource = resource(concat("GG_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("GG_DIVISION_LEVEL_1_HASC_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
    /*name of the country in local languages*/
  LET ?nrfCountryLabel = setLangTag("Guernésy", "nrf")
  LET ?nrmCountryLabel = setLangTag("Guernési", "nrm")

  LET ?nrfLangLabel = setLangTag(?region, "nrf")
  LET ?enLangLabel = setLangTag(?en, "en")
  LET ?frLangLabel = setLangTag(?fr, "fr")
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

WHEN not matches(?nrfalt,"") do
LET ?nrfaltLangLabel = setLangTag(?nrfalt, "nrf")

CONSTRUCT {
  
  emergelModules:GGParishList a emergelModules:Country;
                              rdfs:label "Parishes of Guernsey" ;
                              skos:prefLabel "Parishes of Guernsey" ;
                              rdfs:label "Parishes of Guernsey"@en ;
                              skos:prefLabel "Parishes of Guernsey"@en ;
                              rdfs:label "Paroisses de Guernesey"@fr ;
                              skos:prefLabel "Paroisses de Guernesey"@fr ;
                              rdfs:label "Pâraisses dé Guernésy"@nrf ;
                              skos:prefLabel "Pâraisses dé Guernésy"@nrf .
  
  emergelModules:GGParishHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Guernsey" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Guernsey" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the parishes of Guernsey"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the parishes of Guernsey"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des paroisses de Guernesey"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des paroisses de Guernesey"@fr ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des pâraisses dé Guernésy"@nrf ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des pâraisses dé Guernésy"@nrf ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?enLangLabel ; 
                  skos:prefLabel ?frLangLabel ;
                  skos:prefLabel ?nrfLangLabel ;
                  skos:inScheme emergelModules:GGParishList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?nrfCountryLabel ;
               skos:prefLabel ?nrmCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:GGParishHASCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:altLabel ?nrfaltLangLabel .
}
