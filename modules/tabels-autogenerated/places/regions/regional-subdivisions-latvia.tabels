PREFIX project: <http://idi.fundacionctic.org/tabels/project/LVregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/LVregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?LAU1Code,?HASCCode,?region,?en,?ISOCode,?noteen,?notelv,?noteru,?ru,?et,?FIPSCode,?LOCode,?nuts1,?nuts2,?nuts3] IN horizontal 
    
  LET ?regionResource = resource(concat("LV_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("LV_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("LV_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionLAU1CodeResource = resource(concat("LV_DIVISION_LEVEL_1_LAU1_CODE_",replace(?LAU1Code,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("LV_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
          /*name of the country in local languages*/
      LET ?ltgCountryLabel = setLangTag("Latveja", "ltg")
      LET ?szlCountryLabel = setLangTag("Łotwa", "szl")
      LET ?vroCountryLabel = setLangTag("Läti", "vro")
      LET ?livCountryLabel = setLangTag("Lețmō", "liv")

  LET ?lvLabel = setLangTag(?region, "lv")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?ruLabel = setLangTag(?ru, "ru")
  LET ?etLabel = setLangTag(?et, "et")
  LET ?enLabelNote = setLangTag(?noteen, "en")
  LET ?ruLabelNote = setLangTag(?noteru, "ru")
  LET ?lvLabelNote = setLangTag(?notelv, "lv")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?UNPublisher = resource("STANDARDIZATION_ORGANIZATION_UN",emergelModules)
  LET ?EurostatPublisher = resource("STANDARDIZATION_ORGANIZATION_Eurostat",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)


WHEN not matches(?LOCode,"") DO
LET ?regionLOCodeResource = resource(concat("LV_PLACE_LOCODE_",replace(?LOCode," ","_")),emergelModules)

    

CONSTRUCT {
  
  emergelModules:LVMunicipalityList a emergelModules:Country;
                              rdfs:label "Municipalities of Latvia" ;
                              skos:prefLabel "Municipalities of Latvia" ;
                              rdfs:label "Municipalities of Latvia"@en ;
                              skos:prefLabel "Municipalities of Latvia"@en ;
                              rdfs:label "Latvijas administratīvais iedalījums"@lv ;
                              skos:prefLabel "Latvijas administratīvais iedalījums"@lv ;
                              rdfs:label "Läti haldusjaotus"@et ;
                              skos:prefLabel "Läti haldusjaotus"@et ;
                              rdfs:label "Края и республиканские города Латвии"@ru ;
                              skos:prefLabel "Края и республиканские города Латвии"@ru .
  
  emergelModules:LVMunicipalityISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO 3166-2:LV kodi"@lv ;
                                   skos:prefLabel "ISO 3166-2:LV kodi"@lv ;
                                   rdfs:label "ISO codes for the municipalities of Latvia"@en ;
                                   skos:prefLabel "ISO codes for the municipalities of Latvia"@en ;
                                   rdfs:label "ISO codes for the municipalities of Latvia" ;
                                   skos:prefLabel "ISO codes for the municipalities of Latvia" ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:LVMunicipalityFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS kodi - Latvijas administratīvais iedalījums"@lv ;
                                   skos:prefLabel "FIPS kodi - Latvijas administratīvais iedalījums"@lv ;
                                   rdfs:label "FIPS codes for the municipalities of Latvia"@en ;
                                   skos:prefLabel "FIPS codes for the municipalities of Latvia"@en ;
                                   rdfs:label "FIPS codes for the municipalities of Latvia" ;
                                   skos:prefLabel "FIPS codes for the municipalities of Latvia" ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:LVMunicipalityLAU1CodeList a emergelModules:Country;
                                   rdfs:label "LAU-1 kodi - Latvijas administratīvais iedalījums"@lv ;
                                   skos:prefLabel "LAU-1 kodi - Latvijas administratīvais iedalījums"@lv ;
                                   rdfs:label "LAU-1 codes for the municipalities of Latvia"@en ;
                                   skos:prefLabel "LAU-1 codes for the municipalities of Latvia"@en ;
                                   rdfs:label "LAU-1 codes for the municipalities of Latvia" ;
                                   skos:prefLabel "LAU-1 codes for the municipalities of Latvia" ;
                                   dct:publisher ?EurostatPublisher .

  emergelModules:LVMunicipalityHASCCodeList a emergelModules:Country;
                                   rdfs:label "HASC kodi - Latvijas administratīvais iedalījums"@lv ;
                                   skos:prefLabel "HASC kodi - Latvijas administratīvais iedalījums"@lv ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Latvia" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Latvia" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the municipalities of Latvia"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the municipalities of Latvia"@en ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:LVMunicipalityLOCodeList a emergelModules:Country;
                                   rdfs:label "HASC kodi - Latvijas administratīvais iedalījums"@lv ;
                                   skos:prefLabel "HASC kodi - Latvijas administratīvais iedalījums"@lv ;
                                   rdfs:label "UN/LOCODEs for the municipalities of Latvia"@en ;
                                   skos:prefLabel "UN/LOCODEs for the municipalities of Latvia"@en ;
                                   rdfs:label "UN/LOCODEs for the municipalities of Latvia" ;
                                   skos:prefLabel "UN/LOCODEs for the municipalities of Latvia" ;
                                   dct:publisher ?UNPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?lvLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?etLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:note ?lvLabelNote ;
                  skos:note ?enLabelNote ;
                  skos:note ?ruLabelNote ;
                  skos:inScheme emergelModules:LVMunicipalityList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionLAU1CodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?ltgCountryLabel ;
               skos:prefLabel ?vroCountryLabel ;
               skos:prefLabel ?livCountryLabel ;
               skos:prefLabel ?szlCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:LVMunicipalityISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:LVMunicipalityFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionLAU1CodeResource a skos:Concept;
                  rdfs:label ?LAU1Code ;
                  skos:prefLabel ?LAU1Code ; 
                  skos:inScheme emergelModules:LVMunicipalityLAU1CodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:LVMunicipalityHASCCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionLOCodeResource a skos:Concept;
                  rdfs:label ?LOCode ;
                  skos:prefLabel ?LOCode ; 
                  skos:inScheme emergelModules:LVMunicipalityLOCodeList;
                  emergel:codeOf ?regionResource .

?regionResource emergel:hasCode ?regionLOCodeResource .
}
