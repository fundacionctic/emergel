PREFIX project: <http://idi.fundacionctic.org/tabels/project/departmentsGR/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/departmentsGR/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?provinceCode,?province,?elaltlabel,?noteen,?noteel,?regionCode,?ISOCode,?nuts2,?nuts3,?it,?en,?tr,?FIPSCode,?bg] IN horizontal 
    
  LET ?provinceResource = resource(concat("GR_DIVISION_LEVEL_2_",replace(?provinceCode,"[^a-zA-Z0-9]","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("GR_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("GR_DIVISION_LEVEL_2_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionResource = resource(concat("GR_DIVISION_LEVEL_1_GR_",?regionCode),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?elLangLabel = setLangTag(?province, "el")
  LET ?elaltLangLabel = setLangTag(?elaltlabel, "el-Latn")
  LET ?itLangLabel = setLangTag(?it, "it")
  LET ?enLangLabel = setLangTag(?en, "en")
  LET ?trLangLabel = setLangTag(?tr, "tr")
  LET ?bgLangLabel = setLangTag(?bg, "bg")
  LET ?noteEnLang = setLangTag(?noteen, "en")
  LET ?noteElLang = setLangTag(?noteel, "el")

  LET ?nuts2Res = resource(?nuts2, nuts)
  LET ?nuts3Res = resource(?nuts3, nuts)


CONSTRUCT {
  
  emergelModules:GRDepartmentList a emergelModules:Country;
                              rdfs:label "Departments of Greece" ;
                              skos:prefLabel "Departments of Greece" ;
                              rdfs:label "Departments of Greece"@en ;
                              skos:prefLabel "Departments of Greece"@en ;
                              rdfs:label "Νομοί της Ελλάδας"@el ;
                              skos:prefLabel "Νομοί της Ελλάδας"@el ;
                              rdfs:label "Nomí tis Elládas"@el-Latn ;
                              skos:prefLabel " Nomí tis Elládas"@el-Latn ;
                              rdfs:label "Yunanistan'ın illeri"@tr ;
                              skos:prefLabel "Yunanistan'ın illeri"@tr ;
                              rdfs:label "Prefetture della Grecia"@it ;
                              skos:prefLabel "Prefetture della Grecia"@it ;
                              rdfs:label "Областни единици в Гърция"@bg ;
                              skos:prefLabel "Областни единици в Гърция"@bg .
  
  emergelModules:GRDepartmentISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the departments of Greece" ;
                                   skos:prefLabel "ISO codes for the departments of Greece" ;
                                   rdfs:label "ISO codes for the departments of Greece"@en ;
                                   skos:prefLabel "ISO codes for the departments of Greece"@en ;
                                   rdfs:label "Κωδικοί ISO 3166-2:GR - Νομοί της Ελλάδας"@el ;
                                   skos:prefLabel "Κωδικοί ISO 3166-2:GR - Νομοί της Ελλάδας"@el .

  emergelModules:GRDepartmentFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the departments of Greece" ;
                                   skos:prefLabel "FIPS codes for the departments of Greece" ;
                                   rdfs:label "FIPS codes for the departments of Greece"@en ;
                                   skos:prefLabel "FIPS codes for the departments of Greece"@en ;
                                   rdfs:label "Κωδικοί FIPS - Νομοί της Ελλάδας"@el ;
                                   skos:prefLabel "Κωδικοί FIPS - Νομοί της Ελλάδας"@el .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 2"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 2"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 2"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 2"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 2"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 2"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 2"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 2"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 2"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 2"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 2"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 2"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 2"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 2"@sk .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes" ;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   rdfs:label "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   skos:prefLabel "Nomenclature des unités territoriales statistiques (NUTS) - codes niveau 3"@fr ;
                                   rdfs:label "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   skos:prefLabel "Nomenclatuur van territoriale eenheden voor de statistiek (NUTS) niveau 3"@nl ;
                                   rdfs:label "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   skos:prefLabel "Nomenclatura de les unitats territorials estadístiques (NUTS) - codis nivell 3"@ca ;
                                   rdfs:label "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   skos:prefLabel "Nomenclatura delle unità territoriali statistiche (NUTS) - codici livello 3"@it ;
                                   rdfs:label "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   skos:prefLabel "Nomenklaturo de Unuiĝoj Teritoriaj Statistikaj (NUTS) - 3"@eo ;
                                   rdfs:label "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   skos:prefLabel "Statistiknomenklatur for geografiske enheder (NUTS) - standardkoder niveau 3"@da ;
                                   rdfs:label "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   skos:prefLabel "Nomenklatura územních statistických jednotek (NUTS) - 3"@cs ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@bs ;
                                   rdfs:label "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   skos:prefLabel "Nomenclatura Comum das Unidades Territoriais Estatísticas (NUTS) - nível 3"@pt ;
                                   rdfs:label "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   skos:prefLabel "Systematik vau de Gebietseinheitn fiad Statistik (NUTS) - 3"@bar ;
                                   rdfs:label "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   skos:prefLabel "Номенклатура за статистически териториални единици (NUTS) - ниво 3"@bg ;
                                   rdfs:label "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   skos:prefLabel "Κοινή ονοματολογία των εδαφικών στατιστικών μονάδων (NUTS) - 3"@el ;
                                   rdfs:label "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   skos:prefLabel "Номенклатура статистичких територијалних јединица (НСТЈ) - ниво 3"@sr ;
                                   rdfs:label "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   skos:prefLabel "Nomenklatura prostornih jedinica za statistiku (NUTS) - nivo 3"@sh ;
                                   rdfs:label "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk ;
                                   skos:prefLabel "Spoločná nomenklatúra územných jednotiek pre štatistické účely (NUTS) - úroveň 3"@sk .
}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?elLangLabel ;
                  skos:prefLabel ?itLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?elaltLangLabel ;
                  skos:prefLabel ?trLangLabel ;
                  skos:prefLabel ?bgLangLabel ;
                  skos:note ?noteEnLang ;
                  skos:note ?noteElLang ;
                  skos:inScheme emergelModules:GRDepartmentList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  emergel:hasCode ?nuts3Res ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .
                  //skos:broader ?ISOResource .

  
  //?ISOResource skos:narrower ?provinceResource .
  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:GRDepartmentISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GRDepartmentFIPSCodeList;
                  emergel:codeOf ?provinceResource .

   ?nuts2Res a skos:Concept;
               skos:inScheme emergelModules:NUTS2CodeList ;
               skos:narrower ?nuts3Res .

   ?nuts3Res a skos:Concept;
               skos:inScheme emergelModules:NUTS3CodeList ;
               emergel:codeOf ?provinceResource ;
               skos:broader ?nuts2Res .
}