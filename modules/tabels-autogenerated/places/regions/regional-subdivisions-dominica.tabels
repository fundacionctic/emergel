PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsDM/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsDM/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?hasc,?ISOCode,?region,?fr,?fralt,?es,?esalt,?pt,?ptalt] IN horizontal 
    
  LET ?regionResource = resource(concat("DM_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("DM_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("DM_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)

  LET ?enLabel = setLangTag(?region, "en")
  LET ?esLabel = setLangTag(?es, "es")
  LET ?esaltLabel = setLangTag(?esalt, "es")
  LET ?frLabel = setLangTag(?fr, "fr")
  LET ?fraltLabel = setLangTag(?fralt, "fr")
  LET ?ptLabel = setLangTag(?pt, "pt")
  LET ?ptaltLabel = setLangTag(?ptalt, "pt")
    

CONSTRUCT {
  
  emergelModules:DMParishList a emergelModules:Country;
                              rdfs:label "Parishes of Dominica" ;
                              skos:prefLabel "Parishes of Dominica" ;
                              rdfs:label "Parishes of Dominica"@en ;
                              skos:prefLabel "Parishes of Dominica"@en ;
                              rdfs:label "Parroquias de Dominica"@es ;
                              skos:prefLabel "Parroquias de Dominica"@es ;
                              rdfs:label "Paróquias de Dominica"@pt ;
                              skos:prefLabel "Paróquias de Dominica"@pt ;
                              rdfs:label "Paroisses de Dominica"@fr ;
                              skos:prefLabel "Paroisses de Dominica"@fr .
  
  emergelModules:DMParishISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the parishes of Dominica" ;
                                   skos:prefLabel "ISO codes for the parishes of Dominica" ;
                                   rdfs:label "ISO codes for the parishes of Dominica"@en ;
                                   skos:prefLabel "ISO codes for the parishes of Dominica"@en ;
                                   rdfs:label "Códigos ISO de las parroquias de Dominica"@es ;
                                   skos:prefLabel "Códigos ISO de las parroquias de Dominica"@es ;
                                   rdfs:label "Códigos ISO das paróquias de Dominica"@pt ;
                                   skos:prefLabel "Códigos ISO das paróquias de Dominica"@pt ;
                                   rdfs:label "Codes ISO des paroisses de Dominica"@fr ;
                                   skos:prefLabel "Codes ISO des paroisses de Dominica"@fr ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:DMRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the parishes of Dominica" ;
                                   skos:prefLabel "FIPS codes for the parishes of Dominica" ;
                                   rdfs:label "FIPS codes for the parishes of Dominica"@en ;
                                   skos:prefLabel "FIPS codes for the parishes of Dominica"@en ;
                                   rdfs:label "Códigos FIPS de las parroquias de Dominica"@es ;
                                   skos:prefLabel "Códigos FIPS de las parroquias de Dominica"@es ;
                                   rdfs:label "Códigos FIPS das paróquias de Dominica"@pt ;
                                   skos:prefLabel "Códigos FIPS das paróquias de Dominica"@pt ;
                                   rdfs:label "Codes FIPS des paroisses de Dominica"@fr ;
                                   skos:prefLabel "Codes FIPS des paroisses de Dominica"@fr ;
                                   dct:publisher ?FIPSPublisher .
}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?esaltLabel ; 
                  skos:prefLabel ?ptLabel ;
                  skos:altLabel ?ptaltLabel ; 
                  skos:prefLabel ?frLabel ;
                  skos:altLabel ?fraltLabel ;
                  skos:inScheme emergelModules:DMParishList;
                  emergel:hasCode ?regionCodeResource ;
                  skos:notation ?regionCode ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:DMRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:DMParishISOCodeList;
                  emergel:codeOf ?regionResource .
}
