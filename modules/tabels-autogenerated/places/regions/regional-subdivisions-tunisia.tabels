PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsTN/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsTN/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?fraltlabel,?arlabel,?araltlabel,?arlatlabel,?arlatlabel1,?ISOCode,?FIPSCode,?enlabel] IN horizontal 
    
  LET ?regionResource = resource(concat("TN_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("TN_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("TN_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "fr")
  LET ?fraltLanglabel = setLangTag(?fraltlabel, "fr")
  LET ?arLanglabel = setLangTag(?arlabel, "ar")
  LET ?araltLanglabel = setLangTag(?araltlabel, "ar")
  LET ?arlatLanglabel = setLangTag(?arlatlabel, "ar-Latn")
  LET ?arlatLanglabel1 = setLangTag(?arlatlabel1, "ar-Latn")
  LET ?enLanglabel = setLangTag(?enlabel, "en")

CONSTRUCT {
  
  emergelModules:TNGovernorateList a emergelModules:Country;
                              rdfs:label "Governorates of Tunisia" ;
                              skos:prefLabel "Governorates of Tunisia" ;
                              rdfs:label "Governorates of Tunisia"@en ;
                              skos:prefLabel "Governorates of Tunisia"@en ;
                              rdfs:label "Gouvernorats de Tunisie"@fr ;
                              skos:prefLabel "Gouvernorats de Tunisie"@fr ;
                              rdfs:label "ولايات تونس"@ar ;
                              skos:prefLabel "ولايات تونس"@ar ;
                              rdfs:label "wlayat twns"@ar-Latn ;
                              skos:prefLabel "wlayat twns"@ar-Latn .
  
  emergelModules:TNGovernorateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the governorates of Tunisia" ;
                                   skos:prefLabel "ISO codes for the governorates of Tunisia" ;
                                   rdfs:label "ISO codes for the governorates of Tunisia"@en ;
                                   skos:prefLabel "ISO codes for the governorates of Tunisia"@en ;
                                   rdfs:label "Codes ISO 3166-2:TN pour les gouvernorats de Tunisie"@fr ;
                                   skos:prefLabel "Codes ISO 3166-2:TN pour les gouvernorats de Tunisie"@fr ;
                                   rdfs:label "ISO 3166-2:TN - ولايات تونس"@ar ;
                                   skos:prefLabel "ISO 3166-2:TN - ولايات تونس"@ar ;
                                   rdfs:label "ISO 3166-2:TN - wlayat twns"@ar-Latn ;
                                   skos:prefLabel "ISO 3166-2:TN - wlayat twns"@ar-Latn .

  emergelModules:TNGovernorateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the governorates of Tunisia" ;
                                   skos:prefLabel "FIPS codes for the governorates of Tunisia" ;
                                   rdfs:label "FIPS codes for the governorates of Tunisia"@en ;
                                   skos:prefLabel "FIPS codes for the governorates of Tunisia"@en ;
                                   rdfs:label "Codes FIPS pour les gouvernorats de Tunisie"@fr ;
                                   skos:prefLabel "Codes FIPS pour les gouvernorats de Tunisie"@fr ;
                                   rdfs:label "FIPS - مولايات تونس"@ar ;
                                   skos:prefLabel "FIPS - ولايات تونس"@ar ;
                                   rdfs:label "FIPS - wlayat twns"@ar-Latn ;
                                   skos:prefLabel "FIPS - wlayat twns"@ar-Latn . 

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?arLanglabel ;
                  skos:prefLabel ?arlatLanglabel ;
                  skos:altLabel ?fraltLanglabel ; 
                  skos:altLabel ?araltLanglabel ; 
                  skos:altLabel ?araltLanglabel ; 
                  skos:altLabel ?arlatLanglabel1 ; 
                  skos:inScheme emergelModules:TNGovernorateList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:TNGovernorateISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:TNGovernorateFIPSCodeList;
                  emergel:codeOf ?regionResource .
}