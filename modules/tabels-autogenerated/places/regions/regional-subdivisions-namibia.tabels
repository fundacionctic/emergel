PREFIX project: <http://idi.fundacionctic.org/tabels/project/NAregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/NAregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
WHEN not matches(?rowId,"") DO
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?region,?delabel,?aflabel,?swlabel,?pop2011,?pop2001,?pop1991,?areakm2,?areami2,?capital] IN horizontal 
    
  LET ?regionResource = resource(concat("NA_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("NA_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("NA_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("NA_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
    LET ?swCountryLabel = setLangTag("Namibia", "sw")
    LET ?kgCountryLabel = setLangTag("Namibia", "kg")
    LET ?amCountryLabel = setLangTag("ናሚቢያ", "am")
    LET ?lnCountryLabel = setLangTag("Namibia", "ln")
    LET ?yoCountryLabel = setLangTag("Nàmíbíà", "yo")
    LET ?omCountryLabel = setLangTag("Namiibiyaa", "om")
    LET ?woCountryLabel = setLangTag("Namibi", "wo")
    LET ?naqCountryLabel = setLangTag("Namibiab Republiki dib", "naq")
    LET ?hzCountryLabel = setLangTag("Republika yaNamibia", "hz")
    LET ?kjCountryLabel = setLangTag("Orepublika yaNamibia", "kj")
    LET ?kwnCountryLabel = setLangTag("Republika zaNamibia", "kwn")
    LET ?tnCountryLabel = setLangTag("Repaboleki ya Namibia", "tn")
    LET ?lozCountryLabel = setLangTag("Namibia ye Lukuluhile", "loz")

  LET ?enLabel = setLangTag(?region, "en")
  LET ?deLabel = setLangTag(?delabel, "de")
  LET ?afLabel = setLangTag(?aflabel, "af")
  LET ?swLabel = setLangTag(?swlabel, "sw")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)


CONSTRUCT {
  
  emergelModules:NARegionList a emergelModules:Country;
                              rdfs:label "Regions of Namibia" ;
                              skos:prefLabel "Regions of Namibia" ;
                              rdfs:label "Regions of Namibia"@en ;
                              skos:prefLabel "Regions of Namibia"@en ;
                              rdfs:label "Regionen von Namibia"@de ;
                              skos:prefLabel "Regionen von Namibia"@de ;
                              rdfs:label "Mikoa ya Namibia"@sw ;
                              skos:prefLabel "Mikoa ya Namibia"@sw ;
                              rdfs:label "Streke van Namibië"@af ;
                              skos:prefLabel "Streke van Namibië"@af .
  
  emergelModules:NARegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the regions of Namibia"@en ;
                                   skos:prefLabel "ISO codes for the regions of Namibia"@en ;
                                   rdfs:label "ISO codes for the regions of Namibia" ;
                                   skos:prefLabel "ISO codes for the regions of Namibia" ;
                                   rdfs:label "Liste der ISO-3166-2-Codes für Namibia"@de ;
                                   skos:prefLabel "Liste der ISO-3166-2-Codes für Namibia"@de ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:NARegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the regions of Namibia"@en ;
                                   skos:prefLabel "FIPS codes for the regions of Namibia"@en ;
                                   rdfs:label "FIPS codes for the regions of Namibia" ;
                                   skos:prefLabel "FIPS codes for the regions of Namibia" ;
                                   rdfs:label "Liste der FIPS-Codes für Namibia"@de ;
                                   skos:prefLabel "Liste der FIPS-Codes für Namibia"@de ;
                                   dct:publisher ?FIPSPublisher .


  emergelModules:NARegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Namibia"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Namibia"@en ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the regions of Namibia" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the regions of Namibia" ;
                                   rdfs:label "Liste der HASC-Codes für Namibia"@de ;
                                   skos:prefLabel "Liste der HASC-Codes für Namibia"@de ;
                                   dct:publisher ?HASCPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?deLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?afLabel ;
                  skos:prefLabel ?swLabel ;
                  skos:inScheme emergelModules:NARegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?swCountryLabel ;
               skos:prefLabel ?amCountryLabel ;
               skos:prefLabel ?yoCountryLabel ;
               skos:prefLabel ?woCountryLabel ;
               skos:prefLabel ?omCountryLabel ;
               skos:prefLabel ?lnCountryLabel ;
               skos:prefLabel ?naqCountryLabel ;
               skos:prefLabel ?kwnCountryLabel ;
               skos:prefLabel ?lozCountryLabel ;
               skos:prefLabel ?kgCountryLabel ;
               skos:prefLabel ?hzCountryLabel ;
               skos:prefLabel ?tnCountryLabel ;
               skos:prefLabel ?kjCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:NARegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:NARegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:NARegionHASCCodeList;
                  emergel:codeOf ?regionResource .
}