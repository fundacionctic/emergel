PREFIX project: <http://idi.fundacionctic.org/tabels/project/RWprovinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/RWprovinces/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?ISOCode,?regionCode,?HASCCode,?FIPSCode,?CDCode,?region,?enlabel,?rwlabel,?rwaltlabel,?ptlabel,?zhlabel,?rulabel,?arlabel,?pop2006,?areakm2,?areami2,?capital,?formerProvince] IN horizontal 
    
  LET ?regionResource = resource(concat("RW_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("RW_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("RW_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionHASCCodeResource = resource(concat("RW_DIVISION_LEVEL_1_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)
  LET ?regionCDCodeResource = resource(concat("RW_DIVISION_LEVEL_1_LOCALGOVMIN_CODE_",replace(?CDCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
        /*name of the country in local languages*/
    LET ?amCountryLabel = setLangTag("ሩዋንዳ", "am")
    LET ?swCountryLabel = setLangTag("Rwanda", "sw")
    LET ?kgCountryLabel = setLangTag("Rwanda", "kg")
    LET ?igCountryLabel = setLangTag("Rwanda", "ig")
    LET ?sgCountryLabel = setLangTag("Repubiliki ya Rwanda", "sg")
    LET ?bmCountryLabel = setLangTag("Rwanda", "bm")
    LET ?bmaltCountryLabel = setLangTag("Rwanda ka Fasojamana", "bm")
    LET ?lnCountryLabel = setLangTag("Rwanda", "ln")
    LET ?lnaltCountryLabel = setLangTag("Republíki ya Rwanda", "ln")
    LET ?rnCountryLabel = setLangTag("Urwanda", "rn")
    LET ?rwCountryLabel = setLangTag("Rwanda", "rw")
    LET ?rwaltCountryLabel = setLangTag("Repubulika y'u Rwanda", "rw")
    LET ?omCountryLabel = setLangTag("Ruwaandaa", "om")
    LET ?zuCountryLabel = setLangTag("IRuwanda", "zu")
    LET ?soCountryLabel = setLangTag("Ruwanda", "so")
    LET ?woCountryLabel = setLangTag("Ruwandaa", "wo")

  LET ?frLabel = setLangTag(?region, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?ptLabel = setLangTag(?ptlabel, "pt")
  LET ?ruLabel = setLangTag(?rulabel, "ru")
  LET ?arLabel = setLangTag(?arlabel, "ar")
  LET ?zhLabel = setLangTag(?zhlabel, "zh")
  LET ?rwLabel = setLangTag(?rwlabel, "rw")
  LET ?rwaltLabel = setLangTag(?rwaltlabel, "rw")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)


CONSTRUCT {
  
  emergelModules:RWRegionList a emergelModules:Country;
                              rdfs:label "Provinces of Rwanda" ;
                              skos:prefLabel "Provinces of Rwanda" ;
                              rdfs:label "Provinces of Rwanda"@en ;
                              skos:prefLabel "Provinces of Rwanda"@en ;
                              rdfs:label "Províncias de Ruanda"@pt ;
                              skos:prefLabel "Províncias de Ruanda"@pt ;
                              rdfs:label "Províncies de Ruanda"@ca ;
                              skos:prefLabel "Províncies de Ruanda"@ca ;
                              rdfs:label "محافظات رواندا"@ar ;
                              skos:prefLabel "محافظات رواندا"@ar ;
                              rdfs:label "Intara y’u Rwanda"@rw ;
                              skos:prefLabel "Intara y’u Rwanda"@rw ;
                              rdfs:label "Административное деление Руанды по провинциям"@ru ;
                              skos:prefLabel "Административное деление Руанды по провинциям"@ru ;
                              rdfs:label "卢旺达行政区划"@zh ;
                              skos:prefLabel "卢旺达行政区划"@zh ;
                              rdfs:label "Provinces du Rwanda"@fr ;
                              skos:prefLabel "Provinces du Rwanda"@fr .
  
  emergelModules:RWRegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Rwanda"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Rwanda"@en ;
                                   rdfs:label "ISO codes for the provinces of Rwanda" ;
                                   skos:prefLabel "ISO codes for the provinces of Rwanda" ;
                                   rdfs:label "Codes ISO des provinces du Rwanda"@fr ;
                                   skos:prefLabel "Codes ISO des provinces du Rwanda"@fr ;
                                   rdfs:label "Códigos ISO das províncias de Rwanda"@pt ;
                                   skos:prefLabel "Códigos ISO das províncias de Rwanda"@pt ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:RWRegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Rwanda"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Rwanda"@en ;
                                   rdfs:label "FIPS codes for the provinces of Rwanda" ;
                                   skos:prefLabel "FIPS codes for the provinces of Rwanda" ;
                                   rdfs:label "Codes FIPS des provinces du Rwanda"@fr ;
                                   skos:prefLabel "Codes FIPS des provinces du Rwanda"@fr ;
                                   rdfs:label "Códigos FIPS das províncias de Rwanda"@pt ;
                                   skos:prefLabel "Códigos FIPS das províncias de Rwanda"@pt ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:RWRegionHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Rwanda" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Rwanda" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the provinces of Rwanda"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the provinces of Rwanda"@en ;
                                   dct:publisher ?HASCPublisher .

  emergelModules:RWRegionCDCodeList a emergelModules:Country;
                                   rdfs:label "Ministry of Local Government codes for the provinces of Rwanda"@en ;
                                   skos:prefLabel "Ministry of Local Government codess for the provinces of Rwanda"@en ;
                                   rdfs:label "Ministry of Local Government codes for the provinces of Rwanda" ;
                                   skos:prefLabel "Ministry of Local Government codes for the provinces of Rwanda" ;
                                   rdfs:label "Códigos do Ministério do Governo Local para as províncias de Rwanda"@pt ;
                                   skos:prefLabel "Códigos do Ministério do Governo Local para as províncias de Rwanda"@pt ;
                                   rdfs:label "Codes du Ministère du Gouvernement Local des provinces du Rwanda"@fr ;
                                   skos:prefLabel "Codes du Ministère du Gouvernement Local des provinces du Rwanda"@fr .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?rwLabel ;
                  skos:altLabel ?rwaltLabel ;
                  skos:prefLabel ?frLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?ruLabel ;
                  skos:prefLabel ?zhLabel ;
                  skos:prefLabel ?arLabel ;
                  skos:inScheme emergelModules:RWRegionList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  emergel:hasCode ?regionHASCCodeResource ;
                  emergel:hasCode ?regionCDCodeResource ;
                  skos:notation ?regionCode ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:prefLabel ?amCountryLabel ;
               skos:prefLabel ?swCountryLabel ;
               skos:prefLabel ?sgCountryLabel ;
               skos:prefLabel ?igCountryLabel ;
               skos:prefLabel ?bmCountryLabel ;
               skos:altLabel ?bmaltCountryLabel ;
               skos:prefLabel ?lnCountryLabel ;
               skos:altLabel ?lnaltCountryLabel ;
               skos:prefLabel ?omCountryLabel ;
               skos:prefLabel ?rnCountryLabel ;
               skos:prefLabel ?soCountryLabel ;
               skos:prefLabel ?woCountryLabel ;
               skos:prefLabel ?rwCountryLabel ;
               skos:altLabel ?rwaltCountryLabel ;
               skos:prefLabel ?zuCountryLabel ;
               skos:prefLabel ?kgCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:RWRegionISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:RWRegionFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:RWRegionHASCCodeList;
                  emergel:codeOf ?regionResource .

  ?regionCDCodeResource a skos:Concept;
                  rdfs:label ?CDCode ;
                  skos:prefLabel ?CDCode ; 
                  skos:inScheme emergelModules:RWRegionCDCodeList;
                  emergel:codeOf ?regionResource .
}