PREFIX project: <http://idi.fundacionctic.org/tabels/project/GNprefectures/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/GNprefectures/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX provo: <http://www.w3.org/ns/prov#>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?provinceCode,?HASCCode,?FIPSCode,?province,?enlabel,?ptlabel,?pop2014,?areakm2,?areami2] IN horizontal 
    
  LET ?regionResource = resource(concat("GN_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?provinceResource = resource(concat("GN_DIVISION_LEVEL_2_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceCodeResource = resource(concat("GN_DIVISION_LEVEL_2_ISO_CODE_",replace(?provinceCode,"-","_")),emergelModules)
  LET ?provinceFIPSCodeResource = resource(concat("GN_DIVISION_LEVEL_2_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?provinceHASCCodeResource = resource(concat("GN_DIVISION_LEVEL_2_HASC_CODE_",replace(?HASCCode,"-","_")),emergelModules)

  LET ?frLabel = setLangTag(?province, "fr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?ptLabel = setLangTag(?ptlabel, "pt")

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?HASCPublisher = resource("STANDARDIZATION_ORGANIZATION_HASC",emergelModules)

CONSTRUCT {
  
  emergelModules:GNProvinceList a emergelModules:Country;
                              rdfs:label "Prefectures of Guinea" ;
                              skos:prefLabel "Prefectures of Guinea" ;
                              rdfs:label "Prefectures of Guinea"@en ;
                              skos:prefLabel "Prefectures of Guinea"@en ;
                              rdfs:label "Prefeituras da Guiné"@pt ;
                              skos:prefLabel "Prefeituras da Guiné"@pt ;
                              rdfs:label "Préfectures du Guinée"@fr ;
                              skos:prefLabel "Préfectures du Guinée"@fr .
  
  emergelModules:GNProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the prefectures of Guinea"@en ;
                                   skos:prefLabel "ISO codes for the prefectures of Guinea"@en ;
                                   rdfs:label "ISO codes for the prefectures of Guinea" ;
                                   skos:prefLabel "ISO codes for the prefectures of Guinea" ;
                                   rdfs:label "Codes ISO des préfectures du Guinée"@fr ;
                                   skos:prefLabel "Codes ISO des préfectures du Guinée"@fr ;
                                   rdfs:label "Códigos ISO das prefeituras da Guiné"@pt ;
                                   skos:prefLabel "Códigos ISO das prefeituras da Guiné"@pt ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:GNProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the prefectures of Guinea"@en ;
                                   skos:prefLabel "FIPS codes for the prefectures of Guinea"@en ;
                                   rdfs:label "FIPS codes for the prefectures of Guinea" ;
                                   skos:prefLabel "FIPS codes for the prefectures of Guinea" ;
                                   rdfs:label "Codes FIPS des préfectures du Guinée"@fr ;
                                   skos:prefLabel "Codes FIPS des préfectures du Guinée"@fr ;
                                   rdfs:label "Códigos FIPS das prefeituras da Guiné"@pt ;
                                   skos:prefLabel "Códigos FIPS das prefeituras da Guiné"@pt ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:GNProvinceHASCCodeList a emergelModules:Country;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the prefectures of Guinea" ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the prefectures of Guinea" ;
                                   rdfs:label "Hierarchical administrative subdivision codes (HASC) for the prefectures of Guinea"@en ;
                                   skos:prefLabel "Hierarchical administrative subdivision codes (HASC) for the prefectures of Guinea"@en ;
                                   rdfs:label "Codes HASC (Hierarchical administrative subdivision codes) des préfectures de la Guinée"@fr ;
                                   skos:prefLabel "Codes HASC (Hierarchical administrative subdivision codes) des préfectures de la Guinée"@fr ;
                                   rdfs:label "Códigos HASC (Hierarchical administrative subdivision codes) das prefeituras da Guiné"@pt ;
                                   skos:prefLabel "Códigos HASC (Hierarchical administrative subdivision codes) das prefeituras da Guiné"@pt ;
                                   dct:publisher ?HASCPublisher .

}

CONSTRUCT {
  ?provinceResource a skos:Concept;
                  rdfs:label ?province ;
                  skos:prefLabel ?province ; 
                  skos:prefLabel ?frLabel ; 
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?ptLabel ;
                  skos:inScheme emergelModules:GNProvinceList;
                  emergel:hasCode ?provinceCodeResource ;
                  emergel:hasCode ?provinceFIPSCodeResource ;
                  emergel:hasCode ?provinceHASCCodeResource ;
                  skos:notation ?provinceCode ;
                  skos:broader ?regionResource .

  ?regionResource skos:narrower ?provinceResource .
  
  ?provinceCodeResource a skos:Concept;
                  rdfs:label ?provinceCode ;
                  skos:prefLabel ?provinceCode ; 
                  skos:inScheme emergelModules:GNProvinceISOCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:GNProvinceFIPSCodeList;
                  emergel:codeOf ?provinceResource .

  ?provinceHASCCodeResource a skos:Concept;
                  rdfs:label ?HASCCode ;
                  skos:prefLabel ?HASCCode ; 
                  skos:inScheme emergelModules:GNProvinceHASCCodeList;
                  emergel:codeOf ?provinceResource .
}