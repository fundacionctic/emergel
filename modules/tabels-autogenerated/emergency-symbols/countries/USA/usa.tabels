PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#> 
PREFIX symbol: <http://idi.fundacionctic.org/map-styles/symbols/>

FOR ?rowId IN rows
    MATCH [?ConceptName,?ConceptID,?Description,?URI,?LabelDE,?LabelNL,?LabelUK,?ConceptNameFR,?LabelFR,?Label,?Class1,?Class2,?Class3,?Class4,?Class5,?Class6] IN horizontal 
  LET ?symbolRes = resource(?URI,symbol)
  LET ?conceptRes = resource(concat("US_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code1Res = resource(concat("NL_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code2Res = resource(concat("DK_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code3Res = resource(concat("UK_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code4Res = resource(concat("DE_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code5Res = resource(concat("FR_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code6Res = resource(concat("ES_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code7Res = resource(concat("HR_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code8Res = resource(concat("AT_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code9Res = resource(concat("SE_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code10Res = resource(concat("CH_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code11Res = resource(concat("FI_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code12Res = resource(concat("SI_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code13Res = resource(concat("PL_FIREBRIGADE_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code14Res = resource(concat("PL_MILITARY_POLICE_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code15Res = resource(concat("BE_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code16Res = resource(concat("CA_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?code17Res = resource(concat("AU_EMERGENCY_CONCEPT_",lower-case(replace(?LabelUK,"[^a-zA-Z0-9]",""))), emergelModules)
  LET ?symbolLabel = setLangTag(concat("US symbol for ", ?ConceptName), "en")
  LET ?conceptLabel = setLangTag(?ConceptName, "en")
  LET ?engLabel = setLangTag(?LabelUK, "en")
    WHEN not matches(?Description,"") DO
    LET ?descLabel = ?Description


CONSTRUCT{
  emergelModules:USEmergencySymbol a emergelModules:EmergencySymbol;
                                    rdfs:label "United States emergency symbols" .
  
  emergelModules:USEmergencyConcept a emergelModules:EmergencySymbol;
                               rdfs:label "United States emergency concepts" .
                                      
}

CONSTRUCT{
  ?conceptRes a skos:Concept ;
             rdfs:label ?engLabel ;
             emergel:prefStyle ?symbolRes ;
             skos:inScheme emergelModules:USEmergencyConcept ;
             skos:exactMatch ?code1Res ;
             skos:exactMatch ?code2Res ;
             skos:exactMatch ?code3Res ;
             skos:exactMatch ?code4Res ;
             skos:exactMatch ?code5Res ;
             skos:exactMatch ?code6Res ;
             skos:exactMatch ?code7Res ;
             skos:exactMatch ?code8Res ;
             skos:exactMatch ?code9Res ;
             skos:exactMatch ?code10Res ;
             skos:exactMatch ?code11Res ;
             skos:exactMatch ?code12Res ;
             skos:exactMatch ?code13Res ;
             skos:exactMatch ?code14Res ;
             skos:exactMatch ?code15Res ;
             skos:exactMatch ?code16Res ;
             skos:exactMatch ?code17Res ;
             skos:prefLabel ?conceptLabel .
             
             
}

CONSTRUCT{
  ?conceptRes skos:description ?descLabel .
}

CONSTRUCT{
  ?symbolRes a emergel:Symbol ;
             skos:inScheme emergelModules:USEmergencySymbol ;
             rdfs:label ?symbolLabel ;
             skos:prefLabel ?symbolLabel .
}