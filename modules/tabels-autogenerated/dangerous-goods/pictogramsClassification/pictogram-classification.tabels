PREFIX project: <http://idi.fundacionctic.org/tabels/project/pictogramClassification/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/pictogramClassification/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX symbol: <http://idi.fundacionctic.org/map-styles/symbols/pictograms/>

SET sheets "Hoja1"
    FOR ?rowId IN rows
        MATCH [?pictogram,?label,?classification,?pictureFolder,?rawCode,?status,?deLabel,?frLabel] IN horizontal
        LET ?resourceConcept = resource(replace(concat(?classification,"_CONCEPT_CLASSIFICATION_",?label),"[ .,&/]",""),emergelModules)
        LET ?resourceConceptClassification = resource(replace(concat(?classification,"ConceptClassification"),"[ .,&/]",""), emergelModules)
        LET ?listConceptLabel = setLangTag(concat(?classification," hazards"),"en")
        LET ?resourcePictogram = resource(concat(lower-case(?pictureFolder),"/",?pictogram),symbol) 
        LET ?resourceClassification = resource(replace(concat(?classification,"PictogramClassification"),"[ .,&/]",""), emergelModules)
        LET ?listLabel = setLangTag(concat(?classification," pictograms"),"en")
        LET ?resourceCodeClassification = resource(replace(concat(?classification,"PictogramCodeClassification"),"[ .,&/]",""), emergelModules)
        LET ?listCodeLabel = setLangTag(concat(?classification," pictogram codes"),"en")
        LET ?pictConceptLabel = setLangTag(concat(?classification," pictogram"),"en")
        LET ?pictCodeRes = resource(concat("PICTOGRAM_CODE_",?rawCode), emergelModules)
        LET ?code = replace(?rawCode,"","")
        {
        WHEN not matches(?status, "") DO
          LET ?statusfilled = ?status
        ;
        WHEN not matches(?deLabel, "") DO
          LET ?deLangLabel = setLangTag(?deLabel,"de")
        ;
        WHEN not matches(?frLabel, "") DO
          LET ?frLangLabel = setLangTag(?frLabel,"fr")
        }
  
CONSTRUCT {
        
    ?resourceClassification a emergelModules:DangerousGood ;
                  skos:prefLabel ?listLabel ;
                  rdfs:label ?listLabel . 
}

CONSTRUCT {
        
    ?resourceConceptClassification a emergelModules:DangerousGood ;
                                   skos:prefLabel ?listConceptLabel ;
                                   rdfs:label ?listConceptLabel . 
}

CONSTRUCT {
        
    ?resourceConcept skos:prefLabel ?deLangLabel . 
}

CONSTRUCT {
        
    ?resourceConcept skos:prefLabel ?frLangLabel . 
}

CONSTRUCT {
        
    ?resourceCodeClassification a emergelModules:DangerousGood ;
                  skos:prefLabel ?listCodeLabel ;
                  rdfs:label ?listCodeLabel . 
}

CONSTRUCT{
    ?resourceConcept a skos:Concept ;
                     emergel:prefStyle ?resourcePictogram ;
                     skos:inScheme ?resourceConceptClassification ;
                     emergel:hasCode ?pictCodeRes ;
                     skos:prefLabel ?label ;
                     rdfs:label ?label.
  
}

CONSTRUCT{
    ?resourceConcept emergel:status ?statusfilled.
  
}

CONSTRUCT{
    ?resourcePictogram a emergel:Style ;
                         skos:inScheme ?resourceClassification ;
                         emergel:hasCode ?pictCodeRes ;
                         skos:prefLabel ?label ;
                         rdfs:label ?label.
  
    ?pictCodeRes a skos:Concept ;
                 skos:prefLabel ?code ;
                 rdfs:label ?code ;
                 skos:inScheme ?resourceCodeClassification ;
                 emergel:codeOf ?resourceConcept ;
                 emergel:codeOf ?resourcePictogram .
}

CONSTRUCT{
  
  ?resourcePictogram emergel:status ?statusfilled .
}