#!/bin/bash

# this script uses rdfcat
# see https://jena.apache.org/documentation/javadoc/jena/jena/rdfcat.html

# Esta función tiene problemas con los espacios en blanco
function abspath() { 
	pushd . > /dev/null; if [ -d "$1" ]; then cd "$1"; dirs -l +0; else cd "`dirname \"$1\"`"; cur_dir=`dirs -l +0`; if [ "$cur_dir" == "/" ]; then echo "$cur_dir`basename \"$1\"`"; else echo "$cur_dir/`basename \"$1\"`"; fi; fi; popd > /dev/null; 
}

function exit_error() {
    echo "ATENCION: Script finalizado con error"
    exit 1
}

# Extracts the line error from lines like this:
#   MALFORMED DATA: IRI included an unencoded space: '32' [line 491872]
# 
# Usage: getErrorLine <logFile>
# 
function getErrorLine() {
	logFile=$1
	# echo "calculating error line for file $logFile"
	local result=`grep line $logFile | egrep -o '\[line [^]]*\]' | sed -e 's/\[line\ //' | sed -e 's/\]//'`
	# echo "calculating error line: $result"
	echo "$result"
} 

# Prints a <context size> number of lines around <line number> in <rdf gz file>
# 
# Usage: printErrorContext <rdf gz file> <line number> <context size>
# 
function printErrorContext() {
	rdfZipFile=$1
	lineNumber=$2
	contextSize=$3
	# echo "before calculating start, line number is $lineNumber, contextSize is $contextSize"
	start=`expr $lineNumber - $contextSize / 2`
	end=`expr $lineNumber + $contextSize / 2`
	# echo "start = $start end = $end"
	# gzip -cd rdfZipFile | sed = | sed 'N;s/\n/\t/' | sed -n '9200,9210p'
	gzip -cd $rdfZipFile | sed = | sed 'N;s/\n/\t/' | sed -n ''"$start"','"$end"'p'
}

# Default context value: 20 lines
CONTEXT_SIZE=20

# Obtains the error line from <log file> and prints the corrupt lines from <rdf gz file>
# 
# Usage: printError <log file> <rdf gz file>
# 
function printError() {
	logFile=$1
	rdfZipFile=$2
	echo "printing error for log file $logFile and rdf file $rdfZipFile"
	lineNumber=`getErrorLine $logFile`
	echo "line number found is $lineNumber"
	printErrorContext $rdfZipFile $lineNumber $CONTEXT_SIZE
}

RDFCAT_OUTPUT_FILE="salida.ttl"
RDFCAT_RDF_OUTPUT_FORMAT="TTL"

OUTPUT_FILE_ISSUED="issued.rdf"

TABELS_RDF_OUTPUT_FORMAT="TTL"
TABELS_RDF_OUTPUT_FORMAT_EXTENSION="ttl"
ERROR_FLAG=false