#!/bin/bash

source ./utils.sh

############## BEGIN TABELS GENERATION ###################### 
# read all file name into an array
fileArray_tabels=($(find ../modules/tabels-autogenerated -type f  -name "*.tabels"))

# get length of an array
tLen_tabels=${#fileArray_tabels[@]}

# use for loop read all filenames
for (( i=0; i<${tLen_tabels}; i++ ));
do
  File_path_tabels=$(abspath "${fileArray_tabels[$i]}") 

  File_path_rdf=$(abspath "${File_path_tabels/.tabels/.${TABELS_RDF_OUTPUT_FORMAT_EXTENSION}}")

  File_path_tabular_data=$(abspath "${File_path_tabels/.tabels/.xls}")
  
  if [ ! -f $File_path_tabular_data ]; then
	  File_path_tabular_data=$(abspath "${File_path_tabels/.tabels/.csv}")
  fi

  if [ ! -f $File_path_tabular_data ]; then
	  File_path_tabular_data=$(abspath "${File_path_tabels/.tabels/.ods}")
  fi
  
  if [ -f $File_path_tabular_data ]; then
	  java -Xmx1024m -Dlog4j.configuration=file:"$PWD"/tabels-log4j.properties -jar tabels-cli_2.9.1-0.6-SNAPSHOT-assembly.jar -t ${File_path_tabels} -o ${File_path_rdf} -of ${TABELS_RDF_OUTPUT_FORMAT} ${File_path_tabular_data}
	  return_code=$?
	  if [ $return_code -ne 0 ]; then
	    ERROR_FLAG=true
	  fi
  else
	  echo "Problem obtaining the source tabular data of the tabels program ${File_path_tabels}"
  fi
done

# Check previous problems
if ($ERROR_FLAG); then
	exit_error #send non-zero value
fi


############## END TABELS GENERATION ###################### 

